<?php

get_header();
if (have_posts()):the_post();
    ?>
    <div class="banner-page-list"
         style="background-image: url('<?php echo get_field("background", '11') ?>');">
        <div class="title-page">
            <div class="ac">
            </div>

        </div>
    </div>
    <div class="container">
        <div class="block">


            <div class="block-detail">
                <h2 class="module-title center">Tin tức</h2>
                <hr>
                <h1 class="news-title"><?php the_title(); ?></h1>
                <div class="block-content center">
                    <?php
                    the_content();
                    ?>
                </div>
                <div class="news-others clearfix">
                    <h3>Tin tức khác</h3>
                    <hr>
                    <div class="news-list-other">
                        <?php
                        $args = array(
                            'post_status' => 'publish',
                            'posts_per_page' => 4


                        );
                        wp_reset_query();
                        wp_reset_postdata();

                        $the_query = new WP_Query($args);
                        if ($the_query->have_posts()):
                            while ($the_query->have_posts()):$the_query->the_post();
                                ?>
                                <div class="news-item col-md-6">
                                    <div class="thumb"><a href="<?php echo get_permalink()?>"><?php $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'thumbnail' ); ?>
                                            <img src="<?php echo aq_resize($url,160,120,false,true,true) ?>" /></a></div>
                                    <div class="info">
                                        <div class="title"><a href="<?php echo get_permalink()?>"><?php the_title()?></a></div>
                                        <div class="desc"><?php echo wp_trim_words(get_field('desc'),20);?></div>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
endif;
get_footer();
?>

