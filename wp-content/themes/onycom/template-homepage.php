<?php
/*
 * Template Name: Trang chủ
 * */
get_header();
?>
<div class="home-content">
    <div class="hero-slider">
        <div class="slider-1">
            <div class="slider-1-text"><img src="<?php echo get_template_directory_uri() ?>/images/slider-text.png"
                                            alt=""></div>

        </div>
    </div>
    <div class="block block-chungtoilaai">
        <div class="container">
            <div class="col-md-6">
                <br>
                <br>
                <br>
                <img src="<?php echo get_field('whoweaare_image'); ?>" alt="">
            </div>
            <div class="col-md-6 bg-gray">
                <div class="row">
                    <div class="block-detail">
                        <h2 class="module-title"><?php echo get_field('whoweaare_title'); ?></h2>
                        <div class="module-desc">
                            <?php echo get_field('whoweaare_desc'); ?>
                            <a href="<?php echo get_field('whoweaare_link'); ?>" class="block-more">Tìm hiểu
                                thêm</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block block-giaiphapchodoanhnghiep">
        <div class="container">

            <div class="col-md-12">
                <div class="block-detail">
                    <h2 class="module-title center"><?php echo get_field('doanhnghiep_title'); ?></h2>
                    <div class="module-desc w80 center">
                        <p style="font-family: Roboto"><?php echo get_field('doanhnghiep_desc'); ?></p>
                        <div class="center clearfix"><img
                                    src="<?php echo get_field('doanhnghiep_image'); ?>" alt="">
                        </div>
                        <br>
                        <br>

                        <div class="clearfix">
                            <a href="<?php echo get_field('doanhnghiep_link'); ?>" class="block-more">Xem tất cả</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <hr>
    <div class="block">
        <div class="container">
            <div class="block-detail pd-bot-zero">
                <div class="sub-module-title"><?php echo get_field('giadinh_sub_title'); ?></div>
                <h2 class="module-title center"><?php echo get_field('giadinh_title'); ?></h2>
                <div class="module-tabs">
                    <ul class="family-solution clearfix">
                        <?php
                        $giaiphap = get_field('giadinh_giaphap');
                        $i = 0;
                        $gp_name = array('security', 'kks', 'smarthome');
                        foreach ($giaiphap as $item) {
                            $i++;

                            ?>
                            <li data-name="<?php echo $gp_name[$i - 1] ?>" data-num="<?php echo $i; ?>"
                                class="solution-tab <?php echo ($i == 1) ? 'active' : ''; ?>">
                                <a href="#tab<?php echo $i ?>" data-toggle="tab"
                                   style="    background: #f6f6f6;border-color: transparent;">
                                    <img class="solution-ico img_<?php echo $gp_name[$i - 1]; ?>"
                                         src="<?php echo get_template_directory_uri() ?>/images/<?php echo $gp_name[$i - 1] ?><?php echo ($i == 1) ? '_active' : ''; ?>.png"
                                         alt=""><?php echo $item['title']; ?>
                                </a>
                            </li>
                        <?php } ?>

                    </ul>

                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.solution-tab').click(function () {
                    <?php
                    foreach ($gp_name as $item){
                    ?>
                    $('.img_<?php echo $item;?>').attr('src', '<?php echo get_template_directory_uri() ?>/images/<?php echo $item;?>.png');

                    <?php
                    }
                    ?>
                    $('.img_' + $(this).attr('data-name')).attr('src', '<?php echo get_template_directory_uri() ?>/images/' + $(this).attr('data-name') + '_active.png');
                });
            });
        </script>
        <div class="tab-content">
            <?php

            $j = 0;
            foreach ($giaiphap as $item) {
                $j++;
                ?>
                <div id="tab<?php echo $j ?>"
                     class="tab-pane fade in <?php echo ($j == 1) ? 'active' : ''; ?> ">
                    <div class="f-slider-1"
                         style="height: 580px;background: url('<?php echo get_template_directory_uri() ?>/images/black-gradian-bg.png') left bottom no-repeat, url('<?php echo $item['background'] ?>') no-repeat center center;    -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
                        <div class="f-slider-content">
                            <div class="col-md-2 center bd-white-right"><span><?php echo $item['tag']; ?></span>
                            </div>
                            <div class="col-md-8 center bd-white-right"><p><?php echo $item['desc']; ?></p></div>
                            <div class="col-md-2 center"><a href="<?php echo $item['link']; ?>"
                                                            class="block-more white">Xem thêm</a></div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
    <div class="block home-sanphamnoibat">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Sản phẩm nổi bật</h2>
                <div class="list-products">

                    <?php
                    $args = array(
                        'post_status' => 'publish',
                        'post_type' => 'product'

                    );
                    wp_reset_query();
                    wp_reset_postdata();
                    $arrProductHome = array();
                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()):
                        while ($the_query->have_posts()):$the_query->the_post();
                            wc_get_template_part('content', 'product');
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="block block-duan bg-gray">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Dự án đã thực hiện</h2>
                <div class="list-projects slider2">
                    <?php
                    for ($i = 0; $i < 6; $i++) {
                        ?>
                        <div class="project-item col-md-6">
                            <div class="thumb"><a href=""><img
                                            src="<?php echo get_template_directory_uri() ?>/images/gpdn-home.jpg"
                                            alt=""></a></div>
                            <div class="title"><a href="">Dự án A bla bla bla</a></div>
                            <div class="desc">LẮP ĐĂT KHÓA CHO KHÁCH SẠN LIBERTY QUÊ HƯƠNG KHÓA THẺ KHÁCH SẠN</div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="block block-tintuc">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Tin tức</h2>
                <div class="list-projects slider2">
                    <?php

                    global $post;
                    $args = array('posts_per_page' => 10);

                    $myposts = get_posts($args);
                    foreach ($myposts as $post) : setup_postdata($post);

                        $news_img_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'thumbnail' );?>
                        <div class="project-item col-md-6">
                            <div class="thumb"><a href="<?php echo get_permalink()?>"><img
                                            src="<?php echo aq_resize($news_img_url,490,275) ?>"
                                            alt=""></a></div>
                            <div class="title"><a href="<?php echo get_permalink()?>"><?php the_title()?></a></div>
                            <div class="desc"><?php echo get_field('desc')?>                            </div>
                        </div>
                    <?php endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="block block-video">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Video</h2>
                <div class="slidervideo">

                    <div class="video-item">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="video-item-inner">
                                <a href=""><img src="<?php echo get_template_directory_uri() ?>/images/video-thumb.png"
                                                alt=""></a>
                                <div class="overlay"><a data-fancybox
                                                        href="https://www.youtube.com/watch?v=g2wxJWgqhes">
                                        <img
                                                src="<?php echo get_template_directory_uri() ?>/images/video-play.png"
                                                alt=""></a></div>
                            </div>
                        </div>

                    </div>

                    <div class="video-item">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="video-item-inner">
                                <img src="<?php echo get_template_directory_uri() ?>/images/video-thumb.png"
                                     alt="">
                                <div class="overlay"><a data-fancybox
                                                        href="https://www.youtube.com/watch?v=liT3s1hrc0M"><img
                                                src="<?php echo get_template_directory_uri() ?>/images/video-play.png"
                                                alt=""></a></div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container">

        <div class="block block-video">
            <div class="container">
                <div class="block-detail">

                    <h2 class="module-title center">Đối tác</h2>
                    <?php
                    get_template_part('parts/slider','brands');
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.slidervideo').slick({});
</script>
<?php
get_footer();
?>


