<?php
/*
 * Template Name: Khóa khách sạn
 * */
get_header();
if (have_posts()):the_post();
    ?>
    <div class="onycom-page">
        <div class="onycom-page-header" style="background: url('<?php echo get_field("background") ?>')">
            <div class="container">
                <div class="col-md-8">
                    <div class="text">
                        <h1><?php echo the_title(); ?></h1>

                        <div class="page-desc">
                            <?php echo get_field('desc') ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img right">
                        <img src="<?php echo get_field('image_desc'); ?>" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="breadcrumb">
            <ul>
                <li><a href="">Trang chủ</a> <i class="fa fa-chevron-right"></i></li>
                <li><a href="">Khóa khách sạn</a></li>
            </ul>
        </div>
    </div>
    <?php
    $kks_block = get_field('kks_block');
    foreach ($kks_block as $item) {
        ?>
        <div class="kks-block"
             style="background: url('<?php echo $item['background'] ?>') <?php echo $item['background_color']; ?> no-repeat  <?php echo ($item['align_content'] == 'left') ? 'right' : 'left'; ?>">
            <div class="container">
                <div class="kks-block-content <?php echo $item['align_content']; ?>">
                    <h2><?php echo $item['title'] ?></h2>
                    <?php
                    if ($item['align_content'] == 'right') {
                        ?>
                        <div class="col-md-5"></div>
                    <?php } ?>
                    <div class="desc col-md-7  <?php echo $item['align_content']; ?>">
                        <div class="row">
                            <?php echo $item['desc'] ?>
                            <br>
                            <br>
                            <br>
                            <br>

                            <div>
                                <a href="<?php echo $item['link_xem_them'] ?>" class="block-more">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($item['align_content'] == 'left') {
                        ?>
                        <div class="col-md-5"></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="block home-sanphamnoibat">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Sản phẩm nổi bật</h2>

                <div class="list-products">

                    <?php
                    $args = array(
                        'post_status' => 'publish',
                        'post_type' => 'product'

                    );
                    wp_reset_query();
                    wp_reset_postdata();
                    $arrProductHome = array();
                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()):
                        while ($the_query->have_posts()):$the_query->the_post();
                            wc_get_template_part('content', 'product');
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="block home-slide-duan bg-gray">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Các dự án mới lắp đặt</h2>
                <br>

                <div class="kss-project-item">
                    <div class="col-md-6">
                        <div class="thumb"><a href=""><img
                                    src="<?php echo get_template_directory_uri() ?>/images/kss-project-demo.png"
                                    alt="br"></a></div>
                    </div>
                    <div class="col-md-6">
                        <div class="title"><a href="">Các dòng khóa khách sạn tốt nhất hiện nay</a></div>
                        <div class="desc"> Các dòng khóa khách sạn tốt nhất hiện nay Các dòng khóa khách sạn tốt nhất
                            hiện nay Các dòng khóa khách sạn tốt nhất hiện nay Các dòng khóa khách sạn tốt nhất hiện nay
                        </div>
                        <br>
                        <br>
                        <br>

                        <div><a href="" class="block-more">Xem thêm</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block customer-feedback">
        <div class="container">
            <div class="row">
                <div class="block-detail">

                    <h2 class="module-title center">Nhận xét của khách hàng</h2>
                    <br>

                    <div class="list-customer-feedback">
                        <?php
                        for ($i = 0; $i < 5; $i++) {
                            ?>
                            <div class="col-md-4">
                                <div class="fb-item">
                                    <div class="icon">
                                        <i class="fa fa-quote-left"></i>
                                    </div>
                                    <div class="desc clearfix">
                                        Các dòng khóa khách sạn tốt nhất hiện nay Các dòng khóa khách sạn tốt nhất hiện
                                        nay Các dòng khóa khách sạn tốt nhất hiện nay Các dòng khóa khách sạn tốt nhất
                                        hiện nay

                                    </div>
                                    <div class="user-info clearfix">
                                        <div class="info">
                                            <div class="name">Nguyễn văn A</div>
                                            <div class="sub">Managing Partner, Angelson Group</div>
                                        </div>
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri() ?>/images/avatar.png"
                                                 alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.list-customer-feedback').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            centerMode: true,
            focusOnSelect: true
        });

    </script>
    <?php
endif;
get_footer();
?>

