/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100126
 Source Host           : localhost:3306
 Source Schema         : db_onycom

 Target Server Type    : MySQL
 Target Server Version : 100126
 File Encoding         : 65001

 Date: 17/03/2019 14:38:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wp_commentmeta
-- ----------------------------
DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE `wp_commentmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `comment_id`(`comment_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_comments
-- ----------------------------
DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE `wp_comments`  (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime(0) NOT NULL,
  `comment_date_gmt` datetime(0) NOT NULL,
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`comment_ID`) USING BTREE,
  INDEX `comment_post_ID`(`comment_post_ID`) USING BTREE,
  INDEX `comment_approved_date_gmt`(`comment_approved`, `comment_date_gmt`) USING BTREE,
  INDEX `comment_date_gmt`(`comment_date_gmt`) USING BTREE,
  INDEX `comment_parent`(`comment_parent`) USING BTREE,
  INDEX `comment_author_email`(`comment_author_email`(10)) USING BTREE,
  INDEX `woo_idx_comment_type`(`comment_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_comments
-- ----------------------------
INSERT INTO `wp_comments` VALUES (1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-03-02 15:53:17', '2019-03-02 15:53:17', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- ----------------------------
-- Table structure for wp_links
-- ----------------------------
DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE `wp_links`  (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime(0) NOT NULL,
  `link_rel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`) USING BTREE,
  INDEX `link_visible`(`link_visible`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_options
-- ----------------------------
DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE `wp_options`  (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`) USING BTREE,
  UNIQUE INDEX `option_name`(`option_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1127 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_options
-- ----------------------------
INSERT INTO `wp_options` VALUES (1, 'siteurl', 'http://localhost:8080/onycom', 'yes');
INSERT INTO `wp_options` VALUES (2, 'home', 'http://localhost:8080/onycom', 'yes');
INSERT INTO `wp_options` VALUES (3, 'blogname', 'Onycom Việt Nam', 'yes');
INSERT INTO `wp_options` VALUES (4, 'blogdescription', 'Just another WordPress site', 'yes');
INSERT INTO `wp_options` VALUES (5, 'users_can_register', '0', 'yes');
INSERT INTO `wp_options` VALUES (6, 'admin_email', 'luonghx@gmail.com', 'yes');
INSERT INTO `wp_options` VALUES (7, 'start_of_week', '1', 'yes');
INSERT INTO `wp_options` VALUES (8, 'use_balanceTags', '0', 'yes');
INSERT INTO `wp_options` VALUES (9, 'use_smilies', '1', 'yes');
INSERT INTO `wp_options` VALUES (10, 'require_name_email', '1', 'yes');
INSERT INTO `wp_options` VALUES (11, 'comments_notify', '1', 'yes');
INSERT INTO `wp_options` VALUES (12, 'posts_per_rss', '10', 'yes');
INSERT INTO `wp_options` VALUES (13, 'rss_use_excerpt', '0', 'yes');
INSERT INTO `wp_options` VALUES (14, 'mailserver_url', 'mail.example.com', 'yes');
INSERT INTO `wp_options` VALUES (15, 'mailserver_login', 'login@example.com', 'yes');
INSERT INTO `wp_options` VALUES (16, 'mailserver_pass', 'password', 'yes');
INSERT INTO `wp_options` VALUES (17, 'mailserver_port', '110', 'yes');
INSERT INTO `wp_options` VALUES (18, 'default_category', '3', 'yes');
INSERT INTO `wp_options` VALUES (19, 'default_comment_status', 'open', 'yes');
INSERT INTO `wp_options` VALUES (20, 'default_ping_status', 'open', 'yes');
INSERT INTO `wp_options` VALUES (21, 'default_pingback_flag', '1', 'yes');
INSERT INTO `wp_options` VALUES (22, 'posts_per_page', '10', 'yes');
INSERT INTO `wp_options` VALUES (23, 'date_format', 'F j, Y', 'yes');
INSERT INTO `wp_options` VALUES (24, 'time_format', 'g:i a', 'yes');
INSERT INTO `wp_options` VALUES (25, 'links_updated_date_format', 'F j, Y g:i a', 'yes');
INSERT INTO `wp_options` VALUES (26, 'comment_moderation', '0', 'yes');
INSERT INTO `wp_options` VALUES (27, 'moderation_notify', '1', 'yes');
INSERT INTO `wp_options` VALUES (28, 'permalink_structure', '/%postname%/', 'yes');
INSERT INTO `wp_options` VALUES (29, 'rewrite_rules', 'a:165:{s:28:\"author/([^/]+)/([0-9]{4})/?$\";s:50:\"index.php?author_name=$matches[1]&year=$matches[2]\";s:46:\"author/([^/]+)/([0-9]{4})/page/?([0-9]{1,})/?$\";s:68:\"index.php?author_name=$matches[1]&year=$matches[2]&paged=$matches[3]\";s:39:\"author/([^/]+)/([0-9]{4})/([0-9]{2})/?$\";s:71:\"index.php?author_name=$matches[1]&year=$matches[2]&monthnum=$matches[3]\";s:57:\"author/([^/]+)/([0-9]{4})/([0-9]{2})/page/?([0-9]{1,})/?$\";s:89:\"index.php?author_name=$matches[1]&year=$matches[2]&monthnum=$matches[3]&paged=$matches[4]\";s:50:\"author/([^/]+)/([0-9]{4})/([0-9]{2})/([0-9]{2})/?$\";s:87:\"index.php?author_name=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]\";s:68:\"author/([^/]+)/([0-9]{4})/([0-9]{2})/([0-9]{2})/page/?([0-9]{1,})/?$\";s:105:\"index.php?author_name=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&paged=$matches[5]\";s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:36:\"shop/.+?/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"shop/.+?/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"shop/.+?/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"shop/.+?/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"shop/.+?/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"shop/.+?/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"shop/(.+?)/([^/]+)/embed/?$\";s:64:\"index.php?product_cat=$matches[1]&product=$matches[2]&embed=true\";s:31:\"shop/(.+?)/([^/]+)/trackback/?$\";s:58:\"index.php?product_cat=$matches[1]&product=$matches[2]&tb=1\";s:51:\"shop/(.+?)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:70:\"index.php?product_cat=$matches[1]&product=$matches[2]&feed=$matches[3]\";s:46:\"shop/(.+?)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:70:\"index.php?product_cat=$matches[1]&product=$matches[2]&feed=$matches[3]\";s:39:\"shop/(.+?)/([^/]+)/page/?([0-9]{1,})/?$\";s:71:\"index.php?product_cat=$matches[1]&product=$matches[2]&paged=$matches[3]\";s:46:\"shop/(.+?)/([^/]+)/comment-page-([0-9]{1,})/?$\";s:71:\"index.php?product_cat=$matches[1]&product=$matches[2]&cpage=$matches[3]\";s:36:\"shop/(.+?)/([^/]+)/wc-api(/(.*))?/?$\";s:72:\"index.php?product_cat=$matches[1]&product=$matches[2]&wc-api=$matches[4]\";s:40:\"shop/.+?/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:51:\"shop/.+?/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:35:\"shop/(.+?)/([^/]+)(?:/([0-9]+))?/?$\";s:70:\"index.php?product_cat=$matches[1]&product=$matches[2]&page=$matches[3]\";s:25:\"shop/.+?/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"shop/.+?/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"shop/.+?/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"shop/.+?/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"shop/.+?/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"shop/.+?/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:47:\"brands/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?brands=$matches[1]&feed=$matches[2]\";s:42:\"brands/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?brands=$matches[1]&feed=$matches[2]\";s:23:\"brands/([^/]+)/embed/?$\";s:39:\"index.php?brands=$matches[1]&embed=true\";s:35:\"brands/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?brands=$matches[1]&paged=$matches[2]\";s:17:\"brands/([^/]+)/?$\";s:28:\"index.php?brands=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=26&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes');
INSERT INTO `wp_options` VALUES (30, 'hack_file', '0', 'yes');
INSERT INTO `wp_options` VALUES (31, 'blog_charset', 'UTF-8', 'yes');
INSERT INTO `wp_options` VALUES (32, 'moderation_keys', '', 'no');
INSERT INTO `wp_options` VALUES (33, 'active_plugins', 'a:5:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:2;s:33:\"duplicate-post/duplicate-post.php\";i:3;s:27:\"woocommerce/woocommerce.php\";i:4;s:43:\"wp-htaccess-control/wp-htaccess-control.php\";}', 'yes');
INSERT INTO `wp_options` VALUES (34, 'category_base', '', 'yes');
INSERT INTO `wp_options` VALUES (35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes');
INSERT INTO `wp_options` VALUES (36, 'comment_max_links', '2', 'yes');
INSERT INTO `wp_options` VALUES (37, 'gmt_offset', '0', 'yes');
INSERT INTO `wp_options` VALUES (38, 'default_email_category', '1', 'yes');
INSERT INTO `wp_options` VALUES (39, 'recently_edited', '', 'no');
INSERT INTO `wp_options` VALUES (40, 'template', 'onycom', 'yes');
INSERT INTO `wp_options` VALUES (41, 'stylesheet', 'onycom', 'yes');
INSERT INTO `wp_options` VALUES (42, 'comment_whitelist', '1', 'yes');
INSERT INTO `wp_options` VALUES (43, 'blacklist_keys', '', 'no');
INSERT INTO `wp_options` VALUES (44, 'comment_registration', '0', 'yes');
INSERT INTO `wp_options` VALUES (45, 'html_type', 'text/html', 'yes');
INSERT INTO `wp_options` VALUES (46, 'use_trackback', '0', 'yes');
INSERT INTO `wp_options` VALUES (47, 'default_role', 'subscriber', 'yes');
INSERT INTO `wp_options` VALUES (48, 'db_version', '44719', 'yes');
INSERT INTO `wp_options` VALUES (49, 'uploads_use_yearmonth_folders', '1', 'yes');
INSERT INTO `wp_options` VALUES (50, 'upload_path', '', 'yes');
INSERT INTO `wp_options` VALUES (51, 'blog_public', '1', 'yes');
INSERT INTO `wp_options` VALUES (52, 'default_link_category', '0', 'yes');
INSERT INTO `wp_options` VALUES (53, 'show_on_front', 'page', 'yes');
INSERT INTO `wp_options` VALUES (54, 'tag_base', '', 'yes');
INSERT INTO `wp_options` VALUES (55, 'show_avatars', '1', 'yes');
INSERT INTO `wp_options` VALUES (56, 'avatar_rating', 'G', 'yes');
INSERT INTO `wp_options` VALUES (57, 'upload_url_path', '', 'yes');
INSERT INTO `wp_options` VALUES (58, 'thumbnail_size_w', '150', 'yes');
INSERT INTO `wp_options` VALUES (59, 'thumbnail_size_h', '150', 'yes');
INSERT INTO `wp_options` VALUES (60, 'thumbnail_crop', '1', 'yes');
INSERT INTO `wp_options` VALUES (61, 'medium_size_w', '300', 'yes');
INSERT INTO `wp_options` VALUES (62, 'medium_size_h', '300', 'yes');
INSERT INTO `wp_options` VALUES (63, 'avatar_default', 'mystery', 'yes');
INSERT INTO `wp_options` VALUES (64, 'large_size_w', '1024', 'yes');
INSERT INTO `wp_options` VALUES (65, 'large_size_h', '1024', 'yes');
INSERT INTO `wp_options` VALUES (66, 'image_default_link_type', 'none', 'yes');
INSERT INTO `wp_options` VALUES (67, 'image_default_size', '', 'yes');
INSERT INTO `wp_options` VALUES (68, 'image_default_align', '', 'yes');
INSERT INTO `wp_options` VALUES (69, 'close_comments_for_old_posts', '0', 'yes');
INSERT INTO `wp_options` VALUES (70, 'close_comments_days_old', '14', 'yes');
INSERT INTO `wp_options` VALUES (71, 'thread_comments', '1', 'yes');
INSERT INTO `wp_options` VALUES (72, 'thread_comments_depth', '5', 'yes');
INSERT INTO `wp_options` VALUES (73, 'page_comments', '0', 'yes');
INSERT INTO `wp_options` VALUES (74, 'comments_per_page', '50', 'yes');
INSERT INTO `wp_options` VALUES (75, 'default_comments_page', 'newest', 'yes');
INSERT INTO `wp_options` VALUES (76, 'comment_order', 'asc', 'yes');
INSERT INTO `wp_options` VALUES (77, 'sticky_posts', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (79, 'widget_text', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (80, 'widget_rss', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (81, 'uninstall_plugins', 'a:0:{}', 'no');
INSERT INTO `wp_options` VALUES (82, 'timezone_string', '', 'yes');
INSERT INTO `wp_options` VALUES (83, 'page_for_posts', '0', 'yes');
INSERT INTO `wp_options` VALUES (84, 'page_on_front', '26', 'yes');
INSERT INTO `wp_options` VALUES (85, 'default_post_format', '0', 'yes');
INSERT INTO `wp_options` VALUES (86, 'link_manager_enabled', '0', 'yes');
INSERT INTO `wp_options` VALUES (87, 'finished_splitting_shared_terms', '1', 'yes');
INSERT INTO `wp_options` VALUES (88, 'site_icon', '0', 'yes');
INSERT INTO `wp_options` VALUES (89, 'medium_large_size_w', '768', 'yes');
INSERT INTO `wp_options` VALUES (90, 'medium_large_size_h', '0', 'yes');
INSERT INTO `wp_options` VALUES (91, 'wp_page_for_privacy_policy', '3', 'yes');
INSERT INTO `wp_options` VALUES (92, 'show_comments_cookies_opt_in', '1', 'yes');
INSERT INTO `wp_options` VALUES (93, 'initial_db_version', '44719', 'yes');
INSERT INTO `wp_options` VALUES (94, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:115:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes');
INSERT INTO `wp_options` VALUES (95, 'fresh_site', '0', 'yes');
INSERT INTO `wp_options` VALUES (96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes');
INSERT INTO `wp_options` VALUES (102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (111, 'cron', 'a:14:{i:1552769463;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:0:{}s:8:\"interval\";i:60;}}}i:1552769599;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1552770725;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1552780800;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1552790640;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1552794799;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1552812240;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1552812251;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1552823040;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1552838010;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1552838011;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1552838013;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1554163200;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes');
INSERT INTO `wp_options` VALUES (112, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1551542104;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes');
INSERT INTO `wp_options` VALUES (119, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1552767131;s:7:\"checked\";a:4:{s:6:\"onycom\";s:3:\"1.9\";s:14:\"twentynineteen\";s:3:\"1.3\";s:15:\"twentyseventeen\";s:3:\"2.1\";s:13:\"twentysixteen\";s:3:\"1.9\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `wp_options` VALUES (127, 'can_compress_scripts', '1', 'no');
INSERT INTO `wp_options` VALUES (138, 'current_theme', 'OnyCom', 'yes');
INSERT INTO `wp_options` VALUES (139, 'theme_mods_onycom', 'a:2:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}}', 'yes');
INSERT INTO `wp_options` VALUES (140, 'theme_switched', '', 'yes');
INSERT INTO `wp_options` VALUES (164, 'recently_activated', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (167, 'cptui_new_install', 'false', 'yes');
INSERT INTO `wp_options` VALUES (168, 'acf_version', '5.7.12', 'yes');
INSERT INTO `wp_options` VALUES (169, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes');
INSERT INTO `wp_options` VALUES (173, 'category_children', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (190, 'woocommerce_store_address', 'Số 8 - Lô C11, Ngõ 44/3/2 Nguyễn Cơ Thạch, Mỹ Đình 1, Nam Từ Liêm, Hà Nội', 'yes');
INSERT INTO `wp_options` VALUES (191, 'woocommerce_store_address_2', '', 'yes');
INSERT INTO `wp_options` VALUES (192, 'woocommerce_store_city', 'Hà Nội', 'yes');
INSERT INTO `wp_options` VALUES (193, 'woocommerce_default_country', 'VN:*', 'yes');
INSERT INTO `wp_options` VALUES (194, 'woocommerce_store_postcode', '10000', 'yes');
INSERT INTO `wp_options` VALUES (195, 'woocommerce_allowed_countries', 'all', 'yes');
INSERT INTO `wp_options` VALUES (196, 'woocommerce_all_except_countries', '', 'yes');
INSERT INTO `wp_options` VALUES (197, 'woocommerce_specific_allowed_countries', '', 'yes');
INSERT INTO `wp_options` VALUES (198, 'woocommerce_ship_to_countries', '', 'yes');
INSERT INTO `wp_options` VALUES (199, 'woocommerce_specific_ship_to_countries', '', 'yes');
INSERT INTO `wp_options` VALUES (200, 'woocommerce_default_customer_address', 'geolocation', 'yes');
INSERT INTO `wp_options` VALUES (201, 'woocommerce_calc_taxes', 'no', 'yes');
INSERT INTO `wp_options` VALUES (202, 'woocommerce_enable_coupons', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (203, 'woocommerce_calc_discounts_sequentially', 'no', 'no');
INSERT INTO `wp_options` VALUES (204, 'woocommerce_currency', 'VND', 'yes');
INSERT INTO `wp_options` VALUES (205, 'woocommerce_currency_pos', 'left', 'yes');
INSERT INTO `wp_options` VALUES (206, 'woocommerce_price_thousand_sep', ',', 'yes');
INSERT INTO `wp_options` VALUES (207, 'woocommerce_price_decimal_sep', '.', 'yes');
INSERT INTO `wp_options` VALUES (208, 'woocommerce_price_num_decimals', '2', 'yes');
INSERT INTO `wp_options` VALUES (209, 'woocommerce_shop_page_id', '57', 'yes');
INSERT INTO `wp_options` VALUES (210, 'woocommerce_cart_redirect_after_add', 'no', 'yes');
INSERT INTO `wp_options` VALUES (211, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (212, 'woocommerce_placeholder_image', '', 'yes');
INSERT INTO `wp_options` VALUES (213, 'woocommerce_weight_unit', 'kg', 'yes');
INSERT INTO `wp_options` VALUES (214, 'woocommerce_dimension_unit', 'cm', 'yes');
INSERT INTO `wp_options` VALUES (215, 'woocommerce_enable_reviews', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (216, 'woocommerce_review_rating_verification_label', 'yes', 'no');
INSERT INTO `wp_options` VALUES (217, 'woocommerce_review_rating_verification_required', 'no', 'no');
INSERT INTO `wp_options` VALUES (218, 'woocommerce_enable_review_rating', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (219, 'woocommerce_review_rating_required', 'yes', 'no');
INSERT INTO `wp_options` VALUES (220, 'woocommerce_manage_stock', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (221, 'woocommerce_hold_stock_minutes', '60', 'no');
INSERT INTO `wp_options` VALUES (222, 'woocommerce_notify_low_stock', 'yes', 'no');
INSERT INTO `wp_options` VALUES (223, 'woocommerce_notify_no_stock', 'yes', 'no');
INSERT INTO `wp_options` VALUES (224, 'woocommerce_stock_email_recipient', 'luonghx@gmail.com', 'no');
INSERT INTO `wp_options` VALUES (225, 'woocommerce_notify_low_stock_amount', '2', 'no');
INSERT INTO `wp_options` VALUES (226, 'woocommerce_notify_no_stock_amount', '0', 'yes');
INSERT INTO `wp_options` VALUES (227, 'woocommerce_hide_out_of_stock_items', 'no', 'yes');
INSERT INTO `wp_options` VALUES (228, 'woocommerce_stock_format', '', 'yes');
INSERT INTO `wp_options` VALUES (229, 'woocommerce_file_download_method', 'force', 'no');
INSERT INTO `wp_options` VALUES (230, 'woocommerce_downloads_require_login', 'no', 'no');
INSERT INTO `wp_options` VALUES (231, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no');
INSERT INTO `wp_options` VALUES (232, 'woocommerce_prices_include_tax', 'no', 'yes');
INSERT INTO `wp_options` VALUES (233, 'woocommerce_tax_based_on', 'shipping', 'yes');
INSERT INTO `wp_options` VALUES (234, 'woocommerce_shipping_tax_class', 'inherit', 'yes');
INSERT INTO `wp_options` VALUES (235, 'woocommerce_tax_round_at_subtotal', 'no', 'yes');
INSERT INTO `wp_options` VALUES (236, 'woocommerce_tax_classes', 'Reduced rate\r\nZero rate', 'yes');
INSERT INTO `wp_options` VALUES (237, 'woocommerce_tax_display_shop', 'excl', 'yes');
INSERT INTO `wp_options` VALUES (238, 'woocommerce_tax_display_cart', 'excl', 'yes');
INSERT INTO `wp_options` VALUES (239, 'woocommerce_price_display_suffix', '', 'yes');
INSERT INTO `wp_options` VALUES (240, 'woocommerce_tax_total_display', 'itemized', 'no');
INSERT INTO `wp_options` VALUES (241, 'woocommerce_enable_shipping_calc', 'yes', 'no');
INSERT INTO `wp_options` VALUES (242, 'woocommerce_shipping_cost_requires_address', 'no', 'yes');
INSERT INTO `wp_options` VALUES (243, 'woocommerce_ship_to_destination', 'billing', 'no');
INSERT INTO `wp_options` VALUES (244, 'woocommerce_shipping_debug_mode', 'no', 'yes');
INSERT INTO `wp_options` VALUES (245, 'woocommerce_enable_guest_checkout', 'yes', 'no');
INSERT INTO `wp_options` VALUES (246, 'woocommerce_enable_checkout_login_reminder', 'no', 'no');
INSERT INTO `wp_options` VALUES (247, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no');
INSERT INTO `wp_options` VALUES (248, 'woocommerce_enable_myaccount_registration', 'no', 'no');
INSERT INTO `wp_options` VALUES (249, 'woocommerce_registration_generate_username', 'yes', 'no');
INSERT INTO `wp_options` VALUES (250, 'woocommerce_registration_generate_password', 'yes', 'no');
INSERT INTO `wp_options` VALUES (251, 'woocommerce_erasure_request_removes_order_data', 'no', 'no');
INSERT INTO `wp_options` VALUES (252, 'woocommerce_erasure_request_removes_download_data', 'no', 'no');
INSERT INTO `wp_options` VALUES (253, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes');
INSERT INTO `wp_options` VALUES (254, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes');
INSERT INTO `wp_options` VALUES (255, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no');
INSERT INTO `wp_options` VALUES (256, 'woocommerce_trash_pending_orders', '', 'no');
INSERT INTO `wp_options` VALUES (257, 'woocommerce_trash_failed_orders', '', 'no');
INSERT INTO `wp_options` VALUES (258, 'woocommerce_trash_cancelled_orders', '', 'no');
INSERT INTO `wp_options` VALUES (259, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no');
INSERT INTO `wp_options` VALUES (260, 'woocommerce_email_from_name', 'Onycom Việt Nam', 'no');
INSERT INTO `wp_options` VALUES (261, 'woocommerce_email_from_address', 'luonghx@gmail.com', 'no');
INSERT INTO `wp_options` VALUES (262, 'woocommerce_email_header_image', '', 'no');
INSERT INTO `wp_options` VALUES (263, 'woocommerce_email_footer_text', '{site_title}<br/>Built with <a href=\"https://woocommerce.com/\">WooCommerce</a>', 'no');
INSERT INTO `wp_options` VALUES (264, 'woocommerce_email_base_color', '#96588a', 'no');
INSERT INTO `wp_options` VALUES (265, 'woocommerce_email_background_color', '#f7f7f7', 'no');
INSERT INTO `wp_options` VALUES (266, 'woocommerce_email_body_background_color', '#ffffff', 'no');
INSERT INTO `wp_options` VALUES (267, 'woocommerce_email_text_color', '#3c3c3c', 'no');
INSERT INTO `wp_options` VALUES (268, 'woocommerce_cart_page_id', '58', 'yes');
INSERT INTO `wp_options` VALUES (269, 'woocommerce_checkout_page_id', '59', 'yes');
INSERT INTO `wp_options` VALUES (270, 'woocommerce_myaccount_page_id', '60', 'yes');
INSERT INTO `wp_options` VALUES (271, 'woocommerce_terms_page_id', '', 'no');
INSERT INTO `wp_options` VALUES (272, 'woocommerce_force_ssl_checkout', 'no', 'yes');
INSERT INTO `wp_options` VALUES (273, 'woocommerce_unforce_ssl_checkout', 'no', 'yes');
INSERT INTO `wp_options` VALUES (274, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes');
INSERT INTO `wp_options` VALUES (275, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes');
INSERT INTO `wp_options` VALUES (276, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes');
INSERT INTO `wp_options` VALUES (277, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes');
INSERT INTO `wp_options` VALUES (278, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes');
INSERT INTO `wp_options` VALUES (279, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes');
INSERT INTO `wp_options` VALUES (280, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes');
INSERT INTO `wp_options` VALUES (281, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes');
INSERT INTO `wp_options` VALUES (282, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes');
INSERT INTO `wp_options` VALUES (283, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes');
INSERT INTO `wp_options` VALUES (284, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes');
INSERT INTO `wp_options` VALUES (285, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes');
INSERT INTO `wp_options` VALUES (286, 'woocommerce_logout_endpoint', 'customer-logout', 'yes');
INSERT INTO `wp_options` VALUES (287, 'woocommerce_api_enabled', 'no', 'yes');
INSERT INTO `wp_options` VALUES (288, 'woocommerce_single_image_width', '600', 'yes');
INSERT INTO `wp_options` VALUES (289, 'woocommerce_thumbnail_image_width', '300', 'yes');
INSERT INTO `wp_options` VALUES (290, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (291, 'woocommerce_demo_store', 'no', 'no');
INSERT INTO `wp_options` VALUES (292, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:19:\"/shop/%product_cat%\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:1;}', 'yes');
INSERT INTO `wp_options` VALUES (293, 'current_theme_supports_woocommerce', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (294, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes');
INSERT INTO `wp_options` VALUES (295, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (297, 'default_product_cat', '18', 'yes');
INSERT INTO `wp_options` VALUES (300, 'woocommerce_version', '3.5.5', 'yes');
INSERT INTO `wp_options` VALUES (301, 'woocommerce_db_version', '3.5.5', 'yes');
INSERT INTO `wp_options` VALUES (302, 'woocommerce_admin_notices', 'a:2:{i:1;s:20:\"no_secure_connection\";i:2;s:10:\"wootenberg\";}', 'yes');
INSERT INTO `wp_options` VALUES (303, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (304, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (305, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (306, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (307, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (308, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (309, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (310, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (311, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (312, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (313, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (314, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (315, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` VALUES (319, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:12:\"post-trashed\";s:1:\"1\";s:14:\"total_comments\";i:0;s:3:\"all\";i:0;s:9:\"moderated\";i:0;s:8:\"approved\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;}', 'yes');
INSERT INTO `wp_options` VALUES (320, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (328, 'woocommerce_product_type', 'both', 'yes');
INSERT INTO `wp_options` VALUES (329, 'woocommerce_allow_tracking', 'yes', 'yes');
INSERT INTO `wp_options` VALUES (331, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes');
INSERT INTO `wp_options` VALUES (332, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes');
INSERT INTO `wp_options` VALUES (333, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes');
INSERT INTO `wp_options` VALUES (335, 'woocommerce_tracker_last_send', '1552407955', 'yes');
INSERT INTO `wp_options` VALUES (336, '_transient_shipping-transient-version', '1551602815', 'yes');
INSERT INTO `wp_options` VALUES (339, '_transient_product_query-transient-version', '1552463282', 'yes');
INSERT INTO `wp_options` VALUES (351, '_transient_product-transient-version', '1552463282', 'yes');
INSERT INTO `wp_options` VALUES (375, 'duplicate_post_copytitle', '1', 'yes');
INSERT INTO `wp_options` VALUES (376, 'duplicate_post_copydate', '0', 'yes');
INSERT INTO `wp_options` VALUES (377, 'duplicate_post_copystatus', '0', 'yes');
INSERT INTO `wp_options` VALUES (378, 'duplicate_post_copyslug', '0', 'yes');
INSERT INTO `wp_options` VALUES (379, 'duplicate_post_copyexcerpt', '1', 'yes');
INSERT INTO `wp_options` VALUES (380, 'duplicate_post_copycontent', '1', 'yes');
INSERT INTO `wp_options` VALUES (381, 'duplicate_post_copythumbnail', '1', 'yes');
INSERT INTO `wp_options` VALUES (382, 'duplicate_post_copytemplate', '1', 'yes');
INSERT INTO `wp_options` VALUES (383, 'duplicate_post_copyformat', '1', 'yes');
INSERT INTO `wp_options` VALUES (384, 'duplicate_post_copyauthor', '0', 'yes');
INSERT INTO `wp_options` VALUES (385, 'duplicate_post_copypassword', '0', 'yes');
INSERT INTO `wp_options` VALUES (386, 'duplicate_post_copyattachments', '0', 'yes');
INSERT INTO `wp_options` VALUES (387, 'duplicate_post_copychildren', '0', 'yes');
INSERT INTO `wp_options` VALUES (388, 'duplicate_post_copycomments', '0', 'yes');
INSERT INTO `wp_options` VALUES (389, 'duplicate_post_copymenuorder', '1', 'yes');
INSERT INTO `wp_options` VALUES (390, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (391, 'duplicate_post_blacklist', '', 'yes');
INSERT INTO `wp_options` VALUES (392, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes');
INSERT INTO `wp_options` VALUES (393, 'duplicate_post_show_row', '1', 'yes');
INSERT INTO `wp_options` VALUES (394, 'duplicate_post_show_adminbar', '1', 'yes');
INSERT INTO `wp_options` VALUES (395, 'duplicate_post_show_submitbox', '1', 'yes');
INSERT INTO `wp_options` VALUES (396, 'duplicate_post_show_bulkactions', '1', 'yes');
INSERT INTO `wp_options` VALUES (397, 'duplicate_post_version', '3.2.2', 'yes');
INSERT INTO `wp_options` VALUES (398, 'duplicate_post_show_notice', '1', 'no');
INSERT INTO `wp_options` VALUES (419, 'woocommerce_tracker_ua', 'a:3:{i:0;s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36\";i:1;s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";i:2;s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36\";}', 'yes');
INSERT INTO `wp_options` VALUES (440, '_transient_timeout_wc_shipping_method_count_1_1551602815', '1554476576', 'no');
INSERT INTO `wp_options` VALUES (441, '_transient_wc_shipping_method_count_1_1551602815', '2', 'no');
INSERT INTO `wp_options` VALUES (742, '_transient_timeout_external_ip_address_::1', '1553012741', 'no');
INSERT INTO `wp_options` VALUES (743, '_transient_external_ip_address_::1', '171.234.194.180', 'no');
INSERT INTO `wp_options` VALUES (756, '_site_transient_timeout_browser_75b3341da9e7208fc03d0909f69991aa', '1553012887', 'no');
INSERT INTO `wp_options` VALUES (757, '_site_transient_browser_75b3341da9e7208fc03d0909f69991aa', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"72.0.3626.121\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no');
INSERT INTO `wp_options` VALUES (758, '_site_transient_timeout_php_check_8d84621c0faef737b2f26332c8bab1db', '1553012888', 'no');
INSERT INTO `wp_options` VALUES (759, '_site_transient_php_check_8d84621c0faef737b2f26332c8bab1db', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:1;}', 'no');
INSERT INTO `wp_options` VALUES (805, 'cptui_taxonomies', 'a:1:{s:6:\"brands\";a:24:{s:4:\"name\";s:6:\"brands\";s:5:\"label\";s:6:\"Brands\";s:14:\"singular_label\";s:5:\"Brand\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:12:\"show_in_menu\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:1:\"1\";s:20:\"rewrite_hierarchical\";s:1:\"0\";s:17:\"show_admin_column\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:18:\"show_in_quick_edit\";s:0:\"\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:6:\"labels\";a:18:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:11:\"update_item\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:13:\"new_item_name\";s:0:\"\";s:11:\"parent_item\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:13:\"popular_items\";s:0:\"\";s:26:\"separate_items_with_commas\";s:0:\"\";s:19:\"add_or_remove_items\";s:0:\"\";s:21:\"choose_from_most_used\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:8:\"no_terms\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";}s:11:\"meta_box_cb\";s:0:\"\";s:12:\"object_types\";a:1:{i:0;s:7:\"product\";}}}', 'yes');
INSERT INTO `wp_options` VALUES (843, 'product_cat_children', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (859, 'WPhtc_data', 'a:38:{s:18:\"maintenance_active\";s:0:\"\";s:14:\"login_disabled\";s:0:\"\";s:3:\"hta\";s:0:\"\";s:23:\"disable_serversignature\";s:0:\"\";s:11:\"admin_email\";s:0:\"\";s:15:\"disable_indexes\";s:0:\"\";s:8:\"up_limit\";s:0:\"\";s:12:\"redirect_500\";s:0:\"\";s:12:\"redirect_403\";s:0:\"\";s:17:\"protect_wp_config\";s:0:\"\";s:16:\"protect_htaccess\";s:0:\"\";s:16:\"protect_comments\";s:0:\"\";s:15:\"disable_hotlink\";s:0:\"\";s:24:\"disable_file_hotlink_ext\";s:0:\"\";s:5:\"canon\";s:0:\"\";s:4:\"gzip\";s:0:\"\";s:7:\"deflate\";s:0:\"\";s:6:\"wp_hta\";s:1:\"\n\";s:3:\"cpp\";s:0:\"\";s:3:\"cap\";s:0:\"\";s:23:\"custom_search_permalink\";s:0:\"\";s:20:\"remove_taxonomy_base\";b:0;s:14:\"create_archive\";b:0;s:18:\"remove_author_base\";s:0:\"\";s:17:\"htaccess_original\";b:0;s:11:\"suffix_html\";s:0:\"\";s:20:\"donation_hidden_time\";i:1552412476;s:7:\"cur_hta\";s:322:\"# BEGIN WordPress<br/><br />\n<IfModule mod_rewrite.c><br />\nRewriteEngine On<br />\nRewriteBase /luonghx/onycom/<br />\nRewriteRule ^index\\.php$ - [L]<br />\nRewriteCond %{REQUEST_FILENAME} !-f<br />\nRewriteCond %{REQUEST_FILENAME} !-d<br />\nRewriteRule . /luonghx/onycom/index.php [L]<br />\n</IfModule><br />\n# END WordPress\";s:17:\"category_archives\";s:0:\"\";s:15:\"author_archives\";s:0:\"\";s:12:\"tag_archives\";s:0:\"\";s:26:\"disable_file_hotlink_redir\";s:0:\"\";s:23:\"maintenance_redirection\";s:0:\"\";s:15:\"login_half_mode\";s:0:\"\";s:17:\"login_redirection\";s:0:\"\";s:10:\"sm_enabled\";s:0:\"\";s:16:\"remove_hierarchy\";s:0:\"\";s:15:\"maintenance_ips\";a:0:{}}', 'yes');
INSERT INTO `wp_options` VALUES (904, '_site_transient_timeout_browser_7287feec18def593acadc342d76f271b', '1553051855', 'no');
INSERT INTO `wp_options` VALUES (905, '_site_transient_browser_7287feec18def593acadc342d76f271b', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"73.0.3683.75\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no');
INSERT INTO `wp_options` VALUES (906, '_site_transient_timeout_php_check_090cc27c804cf8206cdc25c72b37bc99', '1553051856', 'no');
INSERT INTO `wp_options` VALUES (907, '_site_transient_php_check_090cc27c804cf8206cdc25c72b37bc99', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no');
INSERT INTO `wp_options` VALUES (1051, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:0;s:3:\"all\";i:0;s:12:\"post-trashed\";s:1:\"1\";s:9:\"moderated\";i:0;s:8:\"approved\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;}', 'yes');
INSERT INTO `wp_options` VALUES (1059, '_transient_timeout_wc_term_counts', '1555055563', 'no');
INSERT INTO `wp_options` VALUES (1060, '_transient_wc_term_counts', 'a:1:{i:21;s:1:\"3\";}', 'no');
INSERT INTO `wp_options` VALUES (1076, 'brands_children', 'a:0:{}', 'yes');
INSERT INTO `wp_options` VALUES (1097, '_transient_timeout_wc_low_stock_count', '1555089422', 'no');
INSERT INTO `wp_options` VALUES (1098, '_transient_wc_low_stock_count', '0', 'no');
INSERT INTO `wp_options` VALUES (1099, '_transient_timeout_wc_outofstock_count', '1555089422', 'no');
INSERT INTO `wp_options` VALUES (1100, '_transient_wc_outofstock_count', '0', 'no');
INSERT INTO `wp_options` VALUES (1119, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.1.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1552767127;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `wp_options` VALUES (1122, '_transient_timeout_acf_plugin_updates', '1552853528', 'no');
INSERT INTO `wp_options` VALUES (1123, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.7.13\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:5:\"4.9.9\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:10:\"expiration\";i:86400;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.7.12\";}}', 'no');
INSERT INTO `wp_options` VALUES (1124, '_site_transient_timeout_theme_roots', '1552768930', 'no');
INSERT INTO `wp_options` VALUES (1125, '_site_transient_theme_roots', 'a:4:{s:6:\"onycom\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no');
INSERT INTO `wp_options` VALUES (1126, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1552767133;s:7:\"checked\";a:7:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.7.12\";s:19:\"akismet/akismet.php\";s:5:\"4.1.1\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.6.1\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.2\";s:9:\"hello.php\";s:5:\"1.7.1\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.5.5\";s:43:\"wp-htaccess-control/wp-htaccess-control.php\";s:5:\"3.5.1\";}s:8:\"response\";a:2:{s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.5.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.5.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.1.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.7.13\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:5:\"4.9.9\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:5:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.6.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.6.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"wp-htaccess-control/wp-htaccess-control.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/wp-htaccess-control\";s:4:\"slug\";s:19:\"wp-htaccess-control\";s:6:\"plugin\";s:43:\"wp-htaccess-control/wp-htaccess-control.php\";s:11:\"new_version\";s:5:\"3.5.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/wp-htaccess-control/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/wp-htaccess-control.3.5.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/wp-htaccess-control/assets/icon-256x256.jpg?rev=1266138\";s:2:\"1x\";s:72:\"https://ps.w.org/wp-htaccess-control/assets/icon-256x256.jpg?rev=1266138\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/wp-htaccess-control/assets/banner-772x250.png?rev=523903\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- ----------------------------
-- Table structure for wp_postmeta
-- ----------------------------
DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE `wp_postmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1868 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_postmeta
-- ----------------------------
INSERT INTO `wp_postmeta` VALUES (1, 2, '_wp_page_template', 'default');
INSERT INTO `wp_postmeta` VALUES (2, 3, '_wp_page_template', 'default');
INSERT INTO `wp_postmeta` VALUES (3, 5, '_edit_lock', '1552411884:1');
INSERT INTO `wp_postmeta` VALUES (4, 7, '_edit_lock', '1551894100:1');
INSERT INTO `wp_postmeta` VALUES (5, 9, '_edit_lock', '1551984393:1');
INSERT INTO `wp_postmeta` VALUES (6, 11, '_edit_lock', '1551980738:1');
INSERT INTO `wp_postmeta` VALUES (7, 13, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (8, 13, '_menu_item_menu_item_parent', '0');
INSERT INTO `wp_postmeta` VALUES (9, 13, '_menu_item_object_id', '11');
INSERT INTO `wp_postmeta` VALUES (10, 13, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (11, 13, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (12, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (13, 13, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (14, 13, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (16, 14, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (17, 14, '_menu_item_menu_item_parent', '0');
INSERT INTO `wp_postmeta` VALUES (18, 14, '_menu_item_object_id', '9');
INSERT INTO `wp_postmeta` VALUES (19, 14, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (20, 14, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (21, 14, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (22, 14, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (23, 14, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (25, 15, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (26, 15, '_menu_item_menu_item_parent', '143');
INSERT INTO `wp_postmeta` VALUES (27, 15, '_menu_item_object_id', '7');
INSERT INTO `wp_postmeta` VALUES (28, 15, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (29, 15, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (30, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (31, 15, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (32, 15, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (34, 16, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (35, 16, '_menu_item_menu_item_parent', '0');
INSERT INTO `wp_postmeta` VALUES (36, 16, '_menu_item_object_id', '5');
INSERT INTO `wp_postmeta` VALUES (37, 16, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (38, 16, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (39, 16, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (40, 16, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (41, 16, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (43, 18, '_edit_lock', '1552410336:1');
INSERT INTO `wp_postmeta` VALUES (46, 1, '_wp_trash_meta_status', 'publish');
INSERT INTO `wp_postmeta` VALUES (47, 1, '_wp_trash_meta_time', '1551598494');
INSERT INTO `wp_postmeta` VALUES (48, 1, '_wp_desired_post_slug', 'hello-world');
INSERT INTO `wp_postmeta` VALUES (49, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}');
INSERT INTO `wp_postmeta` VALUES (52, 21, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (53, 21, '_edit_lock', '1551600051:1');
INSERT INTO `wp_postmeta` VALUES (54, 26, '_edit_lock', '1552505326:1');
INSERT INTO `wp_postmeta` VALUES (55, 26, '_wp_page_template', 'template-homepage.php');
INSERT INTO `wp_postmeta` VALUES (56, 28, '_wp_attached_file', '2019/03/chungtoilaai-img.png');
INSERT INTO `wp_postmeta` VALUES (57, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:477;s:6:\"height\";i:508;s:4:\"file\";s:28:\"2019/03/chungtoilaai-img.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"chungtoilaai-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"chungtoilaai-img-282x300.png\";s:5:\"width\";i:282;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (58, 26, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (59, 26, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (60, 26, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (61, 26, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (62, 26, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (63, 26, 'whoweaare_link', 'http://localhost:8080/onycom/chung-toi-la-ai/');
INSERT INTO `wp_postmeta` VALUES (64, 26, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (65, 26, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (66, 26, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (67, 29, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (68, 29, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (69, 29, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (70, 29, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (71, 29, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (72, 29, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (73, 29, 'whoweaare_desc', 'Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.\r\n\r\nTại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.');
INSERT INTO `wp_postmeta` VALUES (74, 29, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (75, 30, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (76, 30, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (77, 30, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (78, 30, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (79, 30, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (80, 30, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (81, 30, 'whoweaare_desc', 'Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.\r\n\r\nTại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.');
INSERT INTO `wp_postmeta` VALUES (82, 30, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (83, 31, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (84, 31, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (85, 31, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (86, 31, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (87, 31, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (88, 31, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (89, 31, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (90, 31, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (91, 32, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (92, 32, '_edit_lock', '1551600187:1');
INSERT INTO `wp_postmeta` VALUES (93, 37, '_wp_attached_file', '2019/03/gpdn-home.jpg');
INSERT INTO `wp_postmeta` VALUES (94, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:730;s:6:\"height\";i:409;s:4:\"file\";s:21:\"2019/03/gpdn-home.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gpdn-home-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gpdn-home-300x168.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (95, 26, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (96, 26, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (97, 26, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (98, 26, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (99, 26, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (100, 26, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (101, 26, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (102, 26, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (103, 38, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (104, 38, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (105, 38, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (106, 38, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (107, 38, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (108, 38, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (109, 38, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (110, 38, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (111, 38, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (112, 38, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (113, 38, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (114, 38, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (115, 38, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (116, 38, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (117, 38, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (118, 38, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (119, 39, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (120, 39, '_edit_lock', '1551979012:1');
INSERT INTO `wp_postmeta` VALUES (121, 26, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (122, 26, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (123, 26, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (124, 26, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (125, 26, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (126, 26, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (127, 26, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (128, 26, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (129, 26, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (130, 26, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (131, 26, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (132, 26, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (133, 26, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (134, 26, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (135, 48, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (136, 48, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (137, 48, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (138, 48, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (139, 48, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (140, 48, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (141, 48, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (142, 48, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (143, 48, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (144, 48, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (145, 48, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (146, 48, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (147, 48, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (148, 48, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (149, 48, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (150, 48, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (151, 48, 'giadinh_sub_title', '');
INSERT INTO `wp_postmeta` VALUES (152, 48, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (153, 48, 'giadinh_title', '');
INSERT INTO `wp_postmeta` VALUES (154, 48, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (155, 48, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (156, 48, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (157, 48, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (158, 48, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (159, 48, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (160, 48, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (161, 48, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (162, 48, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (163, 48, 'giadinh_giaphap', '1');
INSERT INTO `wp_postmeta` VALUES (164, 48, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (165, 49, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (166, 49, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (167, 49, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (168, 49, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (169, 49, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (170, 49, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (171, 49, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (172, 49, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (173, 49, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (174, 49, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (175, 49, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (176, 49, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (177, 49, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (178, 49, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (179, 49, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (180, 49, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (181, 49, 'giadinh_sub_title', '');
INSERT INTO `wp_postmeta` VALUES (182, 49, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (183, 49, 'giadinh_title', '');
INSERT INTO `wp_postmeta` VALUES (184, 49, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (185, 49, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (186, 49, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (187, 49, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (188, 49, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (189, 49, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (190, 49, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (191, 49, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (192, 49, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (193, 49, 'giadinh_giaphap', '1');
INSERT INTO `wp_postmeta` VALUES (194, 49, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (195, 50, '_wp_attached_file', '2019/03/image-2.png');
INSERT INTO `wp_postmeta` VALUES (196, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1377;s:6:\"height\";i:552;s:4:\"file\";s:19:\"2019/03/image-2.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"image-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"image-2-300x120.png\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"image-2-768x308.png\";s:5:\"width\";i:768;s:6:\"height\";i:308;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"image-2-1024x410.png\";s:5:\"width\";i:1024;s:6:\"height\";i:410;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"image-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"image-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"image-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"image-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (197, 26, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (198, 26, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (199, 51, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (200, 51, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (201, 51, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (202, 51, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (203, 51, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (204, 51, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (205, 51, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (206, 51, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (207, 51, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (208, 51, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (209, 51, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (210, 51, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (211, 51, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (212, 51, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (213, 51, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (214, 51, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (215, 51, 'giadinh_sub_title', '');
INSERT INTO `wp_postmeta` VALUES (216, 51, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (217, 51, 'giadinh_title', '');
INSERT INTO `wp_postmeta` VALUES (218, 51, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (219, 51, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (220, 51, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (221, 51, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (222, 51, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (223, 51, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (224, 51, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (225, 51, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (226, 51, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (227, 51, 'giadinh_giaphap', '1');
INSERT INTO `wp_postmeta` VALUES (228, 51, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (229, 51, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (230, 51, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (231, 52, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (232, 52, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (233, 52, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (234, 52, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (235, 52, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (236, 52, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (237, 52, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (238, 52, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (239, 52, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (240, 52, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (241, 52, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (242, 52, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (243, 52, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (244, 52, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (245, 52, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (246, 52, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (247, 52, 'giadinh_sub_title', '');
INSERT INTO `wp_postmeta` VALUES (248, 52, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (249, 52, 'giadinh_title', '');
INSERT INTO `wp_postmeta` VALUES (250, 52, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (251, 52, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (252, 52, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (253, 52, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (254, 52, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (255, 52, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (256, 52, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (257, 52, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (258, 52, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (259, 52, 'giadinh_giaphap', '1');
INSERT INTO `wp_postmeta` VALUES (260, 52, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (261, 52, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (262, 52, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (263, 53, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (264, 53, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (265, 53, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (266, 53, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (267, 53, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (268, 53, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (269, 53, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (270, 53, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (271, 53, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (272, 53, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (273, 53, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (274, 53, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (275, 53, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (276, 53, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (277, 53, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (278, 53, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (279, 53, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (280, 53, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (281, 53, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (282, 53, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (283, 53, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (284, 53, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (285, 53, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (286, 53, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (287, 53, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (288, 53, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (289, 53, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (290, 53, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (291, 53, 'giadinh_giaphap', '1');
INSERT INTO `wp_postmeta` VALUES (292, 53, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (293, 53, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (294, 53, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (295, 26, 'giadinh_giaphap_1_title', '<br/>Giải pháp khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (296, 26, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (297, 26, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (298, 26, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (299, 26, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (300, 26, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (301, 26, 'giadinh_giaphap_1_desc', 'Quản lý Khách sạn quy mô từ 2 sao với giải pháp quản lý 4.0, giúp tối ưu doanh thu và tiết kiệm chi phí');
INSERT INTO `wp_postmeta` VALUES (302, 26, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (303, 26, 'giadinh_giaphap_1_background', '110');
INSERT INTO `wp_postmeta` VALUES (304, 26, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (305, 26, 'giadinh_giaphap_2_title', '<br/>Smarthome');
INSERT INTO `wp_postmeta` VALUES (306, 26, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (307, 26, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (308, 26, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (309, 26, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (310, 26, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (311, 26, 'giadinh_giaphap_2_desc', 'Kết nối các thiết bị thông minh với nhau, giúp nâng cao tiện nghi cho ngôi nhà của bạn, tự động hoá nhiệm vụ của các thiết bị trong nhà.');
INSERT INTO `wp_postmeta` VALUES (312, 26, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (313, 26, 'giadinh_giaphap_2_background', '112');
INSERT INTO `wp_postmeta` VALUES (314, 26, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (315, 54, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (316, 54, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (317, 54, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (318, 54, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (319, 54, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (320, 54, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (321, 54, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (322, 54, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (323, 54, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (324, 54, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (325, 54, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (326, 54, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (327, 54, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (328, 54, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (329, 54, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (330, 54, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (331, 54, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (332, 54, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (333, 54, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (334, 54, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (335, 54, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (336, 54, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (337, 54, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (338, 54, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (339, 54, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (340, 54, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (341, 54, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (342, 54, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (343, 54, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (344, 54, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (345, 54, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (346, 54, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (347, 54, 'giadinh_giaphap_1_title', 'Giải pháp khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (348, 54, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (349, 54, 'giadinh_giaphap_1_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (350, 54, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (351, 54, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (352, 54, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (353, 54, 'giadinh_giaphap_1_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (354, 54, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (355, 54, 'giadinh_giaphap_1_background', '50');
INSERT INTO `wp_postmeta` VALUES (356, 54, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (357, 54, 'giadinh_giaphap_2_title', 'Marthome');
INSERT INTO `wp_postmeta` VALUES (358, 54, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (359, 54, 'giadinh_giaphap_2_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (360, 54, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (361, 54, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (362, 54, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (363, 54, 'giadinh_giaphap_2_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (364, 54, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (365, 54, 'giadinh_giaphap_2_background', '50');
INSERT INTO `wp_postmeta` VALUES (366, 54, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (367, 55, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (368, 55, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (369, 55, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (370, 55, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (371, 55, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (372, 55, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (373, 55, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (374, 55, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (375, 55, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (376, 55, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (377, 55, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (378, 55, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (379, 55, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (380, 55, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (381, 55, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (382, 55, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (383, 55, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (384, 55, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (385, 55, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (386, 55, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (387, 55, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (388, 55, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (389, 55, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (390, 55, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (391, 55, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (392, 55, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (393, 55, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (394, 55, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (395, 55, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (396, 55, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (397, 55, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (398, 55, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (399, 55, 'giadinh_giaphap_1_title', 'Giải pháp khóa khách sạn<br/>');
INSERT INTO `wp_postmeta` VALUES (400, 55, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (401, 55, 'giadinh_giaphap_1_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (402, 55, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (403, 55, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (404, 55, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (405, 55, 'giadinh_giaphap_1_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (406, 55, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (407, 55, 'giadinh_giaphap_1_background', '50');
INSERT INTO `wp_postmeta` VALUES (408, 55, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (409, 55, 'giadinh_giaphap_2_title', 'Marthome<br/>');
INSERT INTO `wp_postmeta` VALUES (410, 55, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (411, 55, 'giadinh_giaphap_2_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (412, 55, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (413, 55, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (414, 55, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (415, 55, 'giadinh_giaphap_2_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (416, 55, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (417, 55, 'giadinh_giaphap_2_background', '50');
INSERT INTO `wp_postmeta` VALUES (418, 55, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (419, 56, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (420, 56, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (421, 56, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (422, 56, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (423, 56, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (424, 56, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (425, 56, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (426, 56, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (427, 56, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (428, 56, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (429, 56, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (430, 56, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (431, 56, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (432, 56, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (433, 56, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (434, 56, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (435, 56, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (436, 56, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (437, 56, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (438, 56, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (439, 56, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (440, 56, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (441, 56, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (442, 56, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (443, 56, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (444, 56, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (445, 56, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (446, 56, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (447, 56, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (448, 56, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (449, 56, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (450, 56, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (451, 56, 'giadinh_giaphap_1_title', 'Giải pháp khóa khách sạn<br/>');
INSERT INTO `wp_postmeta` VALUES (452, 56, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (453, 56, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (454, 56, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (455, 56, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (456, 56, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (457, 56, 'giadinh_giaphap_1_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (458, 56, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (459, 56, 'giadinh_giaphap_1_background', '50');
INSERT INTO `wp_postmeta` VALUES (460, 56, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (461, 56, 'giadinh_giaphap_2_title', 'Marthome<br/>');
INSERT INTO `wp_postmeta` VALUES (462, 56, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (463, 56, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (464, 56, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (465, 56, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (466, 56, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (467, 56, 'giadinh_giaphap_2_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (468, 56, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (469, 56, 'giadinh_giaphap_2_background', '50');
INSERT INTO `wp_postmeta` VALUES (470, 56, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (479, 63, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (480, 63, '_edit_lock', '1551888157:1');
INSERT INTO `wp_postmeta` VALUES (484, 66, '_wc_review_count', '0');
INSERT INTO `wp_postmeta` VALUES (485, 66, '_wc_rating_count', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (486, 66, '_wc_average_rating', '0');
INSERT INTO `wp_postmeta` VALUES (487, 66, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (488, 66, '_edit_lock', '1551985125:1');
INSERT INTO `wp_postmeta` VALUES (489, 66, '_sku', '');
INSERT INTO `wp_postmeta` VALUES (490, 66, '_regular_price', '10000000');
INSERT INTO `wp_postmeta` VALUES (491, 66, '_sale_price', '8000000');
INSERT INTO `wp_postmeta` VALUES (492, 66, '_sale_price_dates_from', '');
INSERT INTO `wp_postmeta` VALUES (493, 66, '_sale_price_dates_to', '');
INSERT INTO `wp_postmeta` VALUES (494, 66, 'total_sales', '0');
INSERT INTO `wp_postmeta` VALUES (495, 66, '_tax_status', 'taxable');
INSERT INTO `wp_postmeta` VALUES (496, 66, '_tax_class', '');
INSERT INTO `wp_postmeta` VALUES (497, 66, '_manage_stock', 'no');
INSERT INTO `wp_postmeta` VALUES (498, 66, '_backorders', 'no');
INSERT INTO `wp_postmeta` VALUES (499, 66, '_low_stock_amount', '');
INSERT INTO `wp_postmeta` VALUES (500, 66, '_sold_individually', 'no');
INSERT INTO `wp_postmeta` VALUES (501, 66, '_weight', '');
INSERT INTO `wp_postmeta` VALUES (502, 66, '_length', '');
INSERT INTO `wp_postmeta` VALUES (503, 66, '_width', '');
INSERT INTO `wp_postmeta` VALUES (504, 66, '_height', '');
INSERT INTO `wp_postmeta` VALUES (505, 66, '_upsell_ids', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (506, 66, '_crosssell_ids', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (507, 66, '_purchase_note', '');
INSERT INTO `wp_postmeta` VALUES (508, 66, '_default_attributes', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (509, 66, '_virtual', 'no');
INSERT INTO `wp_postmeta` VALUES (510, 66, '_downloadable', 'no');
INSERT INTO `wp_postmeta` VALUES (511, 66, '_product_image_gallery', '');
INSERT INTO `wp_postmeta` VALUES (512, 66, '_download_limit', '-1');
INSERT INTO `wp_postmeta` VALUES (513, 66, '_download_expiry', '-1');
INSERT INTO `wp_postmeta` VALUES (514, 66, '_stock', NULL);
INSERT INTO `wp_postmeta` VALUES (515, 66, '_stock_status', 'instock');
INSERT INTO `wp_postmeta` VALUES (516, 66, '_product_version', '3.5.5');
INSERT INTO `wp_postmeta` VALUES (517, 66, '_price', '8000000');
INSERT INTO `wp_postmeta` VALUES (518, 66, 'product_code', 'SHP DP 728');
INSERT INTO `wp_postmeta` VALUES (519, 66, '_product_code', 'field_5c7b950afd4a3');
INSERT INTO `wp_postmeta` VALUES (520, 67, '_wp_attached_file', '2019/03/smart-doorlock.png');
INSERT INTO `wp_postmeta` VALUES (521, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:210;s:6:\"height\";i:554;s:4:\"file\";s:26:\"2019/03/smart-doorlock.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"smart-doorlock-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"smart-doorlock-114x300.png\";s:5:\"width\";i:114;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"smart-doorlock-210x300.png\";s:5:\"width\";i:210;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"smart-doorlock-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"smart-doorlock-210x300.png\";s:5:\"width\";i:210;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"smart-doorlock-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (522, 66, '_thumbnail_id', '67');
INSERT INTO `wp_postmeta` VALUES (523, 68, '_sku', '');
INSERT INTO `wp_postmeta` VALUES (524, 68, '_regular_price', '7000000');
INSERT INTO `wp_postmeta` VALUES (525, 68, '_sale_price', '5000000');
INSERT INTO `wp_postmeta` VALUES (526, 68, '_sale_price_dates_from', '');
INSERT INTO `wp_postmeta` VALUES (527, 68, '_sale_price_dates_to', '');
INSERT INTO `wp_postmeta` VALUES (528, 68, 'total_sales', '0');
INSERT INTO `wp_postmeta` VALUES (529, 68, '_tax_status', 'taxable');
INSERT INTO `wp_postmeta` VALUES (530, 68, '_tax_class', '');
INSERT INTO `wp_postmeta` VALUES (531, 68, '_manage_stock', 'no');
INSERT INTO `wp_postmeta` VALUES (532, 68, '_backorders', 'no');
INSERT INTO `wp_postmeta` VALUES (533, 68, '_low_stock_amount', '');
INSERT INTO `wp_postmeta` VALUES (534, 68, '_sold_individually', 'no');
INSERT INTO `wp_postmeta` VALUES (535, 68, '_weight', '');
INSERT INTO `wp_postmeta` VALUES (536, 68, '_length', '');
INSERT INTO `wp_postmeta` VALUES (537, 68, '_width', '');
INSERT INTO `wp_postmeta` VALUES (538, 68, '_height', '');
INSERT INTO `wp_postmeta` VALUES (539, 68, '_upsell_ids', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (540, 68, '_crosssell_ids', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (541, 68, '_purchase_note', '');
INSERT INTO `wp_postmeta` VALUES (542, 68, '_default_attributes', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (543, 68, '_virtual', 'no');
INSERT INTO `wp_postmeta` VALUES (544, 68, '_downloadable', 'no');
INSERT INTO `wp_postmeta` VALUES (545, 68, '_product_image_gallery', '');
INSERT INTO `wp_postmeta` VALUES (546, 68, '_download_limit', '-1');
INSERT INTO `wp_postmeta` VALUES (547, 68, '_download_expiry', '-1');
INSERT INTO `wp_postmeta` VALUES (548, 68, '_thumbnail_id', '67');
INSERT INTO `wp_postmeta` VALUES (549, 68, '_stock', NULL);
INSERT INTO `wp_postmeta` VALUES (550, 68, '_stock_status', 'instock');
INSERT INTO `wp_postmeta` VALUES (551, 68, '_wc_average_rating', '0');
INSERT INTO `wp_postmeta` VALUES (552, 68, '_wc_rating_count', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (553, 68, '_wc_review_count', '0');
INSERT INTO `wp_postmeta` VALUES (554, 68, '_downloadable_files', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (555, 68, '_product_attributes', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (556, 68, '_product_version', '3.5.5');
INSERT INTO `wp_postmeta` VALUES (557, 68, '_price', '5000000');
INSERT INTO `wp_postmeta` VALUES (558, 68, 'product_code', 'SHP DP 728');
INSERT INTO `wp_postmeta` VALUES (559, 68, '_product_code', 'field_5c7b950afd4a3');
INSERT INTO `wp_postmeta` VALUES (560, 68, '_edit_lock', '1551985157:1');
INSERT INTO `wp_postmeta` VALUES (561, 68, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (562, 69, '_sku', '');
INSERT INTO `wp_postmeta` VALUES (563, 69, '_regular_price', '5000000');
INSERT INTO `wp_postmeta` VALUES (564, 69, '_sale_price', '3500000');
INSERT INTO `wp_postmeta` VALUES (565, 69, '_sale_price_dates_from', '');
INSERT INTO `wp_postmeta` VALUES (566, 69, '_sale_price_dates_to', '');
INSERT INTO `wp_postmeta` VALUES (567, 69, 'total_sales', '0');
INSERT INTO `wp_postmeta` VALUES (568, 69, '_tax_status', 'taxable');
INSERT INTO `wp_postmeta` VALUES (569, 69, '_tax_class', '');
INSERT INTO `wp_postmeta` VALUES (570, 69, '_manage_stock', 'no');
INSERT INTO `wp_postmeta` VALUES (571, 69, '_backorders', 'no');
INSERT INTO `wp_postmeta` VALUES (572, 69, '_low_stock_amount', '');
INSERT INTO `wp_postmeta` VALUES (573, 69, '_sold_individually', 'no');
INSERT INTO `wp_postmeta` VALUES (574, 69, '_weight', '');
INSERT INTO `wp_postmeta` VALUES (575, 69, '_length', '');
INSERT INTO `wp_postmeta` VALUES (576, 69, '_width', '');
INSERT INTO `wp_postmeta` VALUES (577, 69, '_height', '');
INSERT INTO `wp_postmeta` VALUES (578, 69, '_upsell_ids', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (579, 69, '_crosssell_ids', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (580, 69, '_purchase_note', '');
INSERT INTO `wp_postmeta` VALUES (581, 69, '_default_attributes', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (582, 69, '_virtual', 'no');
INSERT INTO `wp_postmeta` VALUES (583, 69, '_downloadable', 'no');
INSERT INTO `wp_postmeta` VALUES (584, 69, '_product_image_gallery', '');
INSERT INTO `wp_postmeta` VALUES (585, 69, '_download_limit', '-1');
INSERT INTO `wp_postmeta` VALUES (586, 69, '_download_expiry', '-1');
INSERT INTO `wp_postmeta` VALUES (587, 69, '_thumbnail_id', '67');
INSERT INTO `wp_postmeta` VALUES (588, 69, '_stock', NULL);
INSERT INTO `wp_postmeta` VALUES (589, 69, '_stock_status', 'instock');
INSERT INTO `wp_postmeta` VALUES (590, 69, '_wc_average_rating', '0');
INSERT INTO `wp_postmeta` VALUES (591, 69, '_wc_rating_count', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (592, 69, '_wc_review_count', '0');
INSERT INTO `wp_postmeta` VALUES (593, 69, '_downloadable_files', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (594, 69, '_product_attributes', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (595, 69, '_product_version', '3.5.5');
INSERT INTO `wp_postmeta` VALUES (596, 69, '_price', '3500000');
INSERT INTO `wp_postmeta` VALUES (597, 69, 'product_code', 'SHP DP 728');
INSERT INTO `wp_postmeta` VALUES (598, 69, '_product_code', 'field_5c7b950afd4a3');
INSERT INTO `wp_postmeta` VALUES (599, 69, '_edit_lock', '1552463311:1');
INSERT INTO `wp_postmeta` VALUES (600, 69, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (601, 5, '_wp_page_template', 'template-doanhnghiep.php');
INSERT INTO `wp_postmeta` VALUES (603, 71, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (604, 71, '_edit_lock', '1552450844:1');
INSERT INTO `wp_postmeta` VALUES (605, 5, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (606, 5, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (607, 5, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (608, 5, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (609, 5, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (610, 5, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (611, 5, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (612, 5, 'desc', 'Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Cac giải pháp và dịch vụ thông minh, dữ liệu lớn và ứng dụng di động\r\n');
INSERT INTO `wp_postmeta` VALUES (613, 5, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (614, 76, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (615, 76, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (616, 76, 'background', '50');
INSERT INTO `wp_postmeta` VALUES (617, 76, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (618, 76, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (619, 76, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (620, 76, 'desc', 'Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Cac giải pháp và dịch vụ thông minh, dữ liệu lớn và ứng dụng di động\r\n');
INSERT INTO `wp_postmeta` VALUES (621, 76, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (622, 77, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (623, 77, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (624, 77, 'background', '50');
INSERT INTO `wp_postmeta` VALUES (625, 77, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (626, 77, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (627, 77, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (628, 77, 'desc', 'Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Cac giải pháp và dịch vụ thông minh, dữ liệu lớn và ứng dụng di động\r\n');
INSERT INTO `wp_postmeta` VALUES (629, 77, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (630, 78, '_wp_attached_file', '2019/03/slider.png');
INSERT INTO `wp_postmeta` VALUES (631, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:832;s:4:\"file\";s:18:\"2019/03/slider.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"slider-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"slider-300x183.png\";s:5:\"width\";i:300;s:6:\"height\";i:183;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"slider-768x468.png\";s:5:\"width\";i:768;s:6:\"height\";i:468;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"slider-1024x624.png\";s:5:\"width\";i:1024;s:6:\"height\";i:624;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"slider-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"slider-600x365.png\";s:5:\"width\";i:600;s:6:\"height\";i:365;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"slider-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"slider-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:18:\"slider-600x365.png\";s:5:\"width\";i:600;s:6:\"height\";i:365;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"slider-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (632, 79, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (633, 79, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (634, 79, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (635, 79, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (636, 79, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (637, 79, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (638, 79, 'desc', 'Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Cac giải pháp và dịch vụ thông minh, dữ liệu lớn và ứng dụng di động\r\n');
INSERT INTO `wp_postmeta` VALUES (639, 79, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (640, 80, '_edit_lock', '1551980061:1');
INSERT INTO `wp_postmeta` VALUES (641, 80, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (642, 80, 'thumb', '83');
INSERT INTO `wp_postmeta` VALUES (643, 80, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (644, 80, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (645, 80, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (646, 80, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (647, 80, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (648, 80, 'desc', 'Chúng tôi cung cấp dịch vụ vượt trội dựa trên nhiều năm kinh nghiệm thực hiện các dự án lớn với chính phủ và các cơ quan công của Hàn Quốc thực hiện bởi đội ngũ kỹ thuật viên, chuyên gia đầu ngành của chúng tôi.\r\n');
INSERT INTO `wp_postmeta` VALUES (649, 80, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (650, 81, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (651, 81, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (652, 81, 'background', '');
INSERT INTO `wp_postmeta` VALUES (653, 81, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (654, 81, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (655, 81, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (656, 81, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (657, 81, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (658, 82, '_wp_attached_file', '2019/03/gpdn-1.png');
INSERT INTO `wp_postmeta` VALUES (659, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:257;s:6:\"height\";i:211;s:4:\"file\";s:18:\"2019/03/gpdn-1.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"gpdn-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"gpdn-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"gpdn-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (660, 83, '_wp_attached_file', '2019/03/gpdn-hethong.jpg');
INSERT INTO `wp_postmeta` VALUES (661, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:238;s:6:\"height\";i:198;s:4:\"file\";s:24:\"2019/03/gpdn-hethong.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"gpdn-hethong-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"gpdn-hethong-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"gpdn-hethong-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (662, 84, '_wp_attached_file', '2019/03/gpdn-hieusuat.jpg');
INSERT INTO `wp_postmeta` VALUES (663, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:237;s:6:\"height\";i:195;s:4:\"file\";s:25:\"2019/03/gpdn-hieusuat.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gpdn-hieusuat-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"gpdn-hieusuat-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"gpdn-hieusuat-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (664, 85, '_wp_attached_file', '2019/03/qgpd-chatluong.jpg');
INSERT INTO `wp_postmeta` VALUES (665, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:237;s:6:\"height\";i:198;s:4:\"file\";s:26:\"2019/03/qgpd-chatluong.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"qgpd-chatluong-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"qgpd-chatluong-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"qgpd-chatluong-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (666, 86, 'thumb', '83');
INSERT INTO `wp_postmeta` VALUES (667, 86, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (668, 86, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (669, 86, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (670, 86, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (671, 86, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (672, 86, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (673, 86, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (674, 87, '_edit_lock', '1551886060:1');
INSERT INTO `wp_postmeta` VALUES (675, 87, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (676, 87, 'thumb', '82');
INSERT INTO `wp_postmeta` VALUES (677, 87, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (678, 87, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (679, 87, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (680, 87, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (681, 87, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (682, 87, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (683, 87, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (684, 89, 'thumb', '82');
INSERT INTO `wp_postmeta` VALUES (685, 89, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (686, 89, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (687, 89, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (688, 89, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (689, 89, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (690, 89, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (691, 89, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (692, 90, '_edit_lock', '1551886107:1');
INSERT INTO `wp_postmeta` VALUES (693, 90, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (694, 90, 'thumb', '83');
INSERT INTO `wp_postmeta` VALUES (695, 90, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (696, 90, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (697, 90, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (698, 90, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (699, 90, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (700, 90, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (701, 90, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (702, 91, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (703, 91, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (704, 91, 'background', '');
INSERT INTO `wp_postmeta` VALUES (705, 91, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (706, 91, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (707, 91, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (708, 91, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (709, 91, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (710, 92, 'thumb', '83');
INSERT INTO `wp_postmeta` VALUES (711, 92, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (712, 92, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (713, 92, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (714, 92, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (715, 92, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (716, 92, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (717, 92, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (718, 93, '_edit_lock', '1551886714:1');
INSERT INTO `wp_postmeta` VALUES (719, 93, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (720, 93, 'thumb', '84');
INSERT INTO `wp_postmeta` VALUES (721, 93, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (722, 93, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (723, 93, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (724, 93, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (725, 93, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (726, 93, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (727, 93, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (728, 94, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (729, 94, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (730, 94, 'background', '');
INSERT INTO `wp_postmeta` VALUES (731, 94, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (732, 94, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (733, 94, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (734, 94, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (735, 94, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (736, 95, 'thumb', '84');
INSERT INTO `wp_postmeta` VALUES (737, 95, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (738, 95, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (739, 95, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (740, 95, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (741, 95, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (742, 95, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (743, 95, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (744, 96, 'thumb', '84');
INSERT INTO `wp_postmeta` VALUES (745, 96, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (746, 96, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (747, 96, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (748, 96, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (749, 96, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (750, 96, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (751, 96, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (752, 7, '_wp_page_template', 'template-smarthome.php');
INSERT INTO `wp_postmeta` VALUES (753, 7, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (754, 7, 'thumb', '99');
INSERT INTO `wp_postmeta` VALUES (755, 7, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (756, 7, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (757, 7, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (758, 7, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (759, 7, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (760, 7, 'desc', 'Kết nối các thiết bị thông minh trong nhà bạn bằng 1 ứng dụng. Giải pháp nhà thông minh của Onycom giúp cuộc sống của bạn thật dễ dàng và tiện nghi hơn bao giờ hết. Đã bật chế độ hỗ trợ trình đọc màn hình.\r\n');
INSERT INTO `wp_postmeta` VALUES (761, 7, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (762, 8, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (763, 8, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (764, 8, 'background', '');
INSERT INTO `wp_postmeta` VALUES (765, 8, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (766, 8, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (767, 8, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (768, 8, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (769, 8, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (770, 97, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (771, 97, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (772, 97, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (773, 97, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (774, 97, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (775, 97, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (776, 97, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (777, 97, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (778, 98, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (779, 98, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (780, 98, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (781, 98, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (782, 98, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (783, 98, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (784, 98, 'desc', 'Kết nối các thiết bị thông minh trong nhà bạn bằng 1 ứng dụng. Giải pháp nhà thông minh của Onycom giúp cuộc sống của bạn thật dễ dàng và tiện nghi hơn bao giờ hết. Đã bật chế độ hỗ trợ trình đọc màn hình.\r\n');
INSERT INTO `wp_postmeta` VALUES (785, 98, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (786, 99, '_wp_attached_file', '2019/03/smarthome-tit.png');
INSERT INTO `wp_postmeta` VALUES (787, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1234;s:6:\"height\";i:275;s:4:\"file\";s:25:\"2019/03/smarthome-tit.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"smarthome-tit-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"smarthome-tit-300x67.png\";s:5:\"width\";i:300;s:6:\"height\";i:67;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"smarthome-tit-768x171.png\";s:5:\"width\";i:768;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"smarthome-tit-1024x228.png\";s:5:\"width\";i:1024;s:6:\"height\";i:228;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"smarthome-tit-300x275.png\";s:5:\"width\";i:300;s:6:\"height\";i:275;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"smarthome-tit-600x134.png\";s:5:\"width\";i:600;s:6:\"height\";i:134;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"smarthome-tit-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"smarthome-tit-300x275.png\";s:5:\"width\";i:300;s:6:\"height\";i:275;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"smarthome-tit-600x134.png\";s:5:\"width\";i:600;s:6:\"height\";i:134;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"smarthome-tit-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (788, 100, 'thumb', '99');
INSERT INTO `wp_postmeta` VALUES (789, 100, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (790, 100, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (791, 100, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (792, 100, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (793, 100, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (794, 100, 'desc', 'Kết nối các thiết bị thông minh trong nhà bạn bằng 1 ứng dụng. Giải pháp nhà thông minh của Onycom giúp cuộc sống của bạn thật dễ dàng và tiện nghi hơn bao giờ hết. Đã bật chế độ hỗ trợ trình đọc màn hình.\r\n');
INSERT INTO `wp_postmeta` VALUES (795, 100, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (796, 101, '_wc_review_count', '0');
INSERT INTO `wp_postmeta` VALUES (797, 101, '_wc_rating_count', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (798, 101, '_wc_average_rating', '0');
INSERT INTO `wp_postmeta` VALUES (799, 101, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (800, 101, '_edit_lock', '1551891468:1');
INSERT INTO `wp_postmeta` VALUES (801, 102, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (802, 102, '_edit_lock', '1551891390:1');
INSERT INTO `wp_postmeta` VALUES (803, 101, '_wp_trash_meta_status', 'draft');
INSERT INTO `wp_postmeta` VALUES (804, 101, '_wp_trash_meta_time', '1551891538');
INSERT INTO `wp_postmeta` VALUES (805, 101, '_wp_desired_post_slug', '');
INSERT INTO `wp_postmeta` VALUES (806, 104, '_wp_attached_file', '2019/03/chungtoilaai-img-1.png');
INSERT INTO `wp_postmeta` VALUES (807, 104, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:477;s:6:\"height\";i:508;s:4:\"file\";s:30:\"2019/03/chungtoilaai-img-1.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"chungtoilaai-img-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"chungtoilaai-img-1-282x300.png\";s:5:\"width\";i:282;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"chungtoilaai-img-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"chungtoilaai-img-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"chungtoilaai-img-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"chungtoilaai-img-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (808, 105, '_wp_attached_file', '2019/03/smart-doorlock-1.png');
INSERT INTO `wp_postmeta` VALUES (809, 105, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:210;s:6:\"height\";i:554;s:4:\"file\";s:28:\"2019/03/smart-doorlock-1.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"smart-doorlock-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"smart-doorlock-1-114x300.png\";s:5:\"width\";i:114;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"smart-doorlock-1-210x300.png\";s:5:\"width\";i:210;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"smart-doorlock-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:28:\"smart-doorlock-1-210x300.png\";s:5:\"width\";i:210;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:28:\"smart-doorlock-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (810, 69, 'image_gallery', 'a:2:{i:0;s:3:\"104\";i:1;s:3:\"105\";}');
INSERT INTO `wp_postmeta` VALUES (811, 69, '_image_gallery', 'field_5c7ffc18024c7');
INSERT INTO `wp_postmeta` VALUES (812, 106, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (813, 106, '_menu_item_menu_item_parent', '16');
INSERT INTO `wp_postmeta` VALUES (814, 106, '_menu_item_object_id', '93');
INSERT INTO `wp_postmeta` VALUES (815, 106, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (816, 106, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (817, 106, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (818, 106, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (819, 106, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (821, 107, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (822, 107, '_menu_item_menu_item_parent', '16');
INSERT INTO `wp_postmeta` VALUES (823, 107, '_menu_item_object_id', '90');
INSERT INTO `wp_postmeta` VALUES (824, 107, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (825, 107, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (826, 107, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (827, 107, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (828, 107, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (830, 108, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (831, 108, '_menu_item_menu_item_parent', '16');
INSERT INTO `wp_postmeta` VALUES (832, 108, '_menu_item_object_id', '87');
INSERT INTO `wp_postmeta` VALUES (833, 108, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (834, 108, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (835, 108, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (836, 108, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (837, 108, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (839, 109, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (840, 109, '_menu_item_menu_item_parent', '16');
INSERT INTO `wp_postmeta` VALUES (841, 109, '_menu_item_object_id', '80');
INSERT INTO `wp_postmeta` VALUES (842, 109, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (843, 109, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (844, 109, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (845, 109, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (846, 109, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (847, 110, '_wp_attached_file', '2019/03/kks_slide_image.png');
INSERT INTO `wp_postmeta` VALUES (848, 110, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:910;s:4:\"file\";s:27:\"2019/03/kks_slide_image.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-768x512.png\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"kks_slide_image-1024x682.png\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"kks_slide_image-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-600x400.png\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-600x400.png\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"kks_slide_image-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (849, 26, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (850, 26, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (851, 26, 'background', '');
INSERT INTO `wp_postmeta` VALUES (852, 26, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (853, 26, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (854, 26, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (855, 26, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (856, 26, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (857, 111, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (858, 111, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (859, 111, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (860, 111, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (861, 111, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (862, 111, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (863, 111, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (864, 111, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (865, 111, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (866, 111, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (867, 111, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (868, 111, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (869, 111, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (870, 111, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (871, 111, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (872, 111, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (873, 111, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (874, 111, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (875, 111, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (876, 111, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (877, 111, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (878, 111, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (879, 111, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (880, 111, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (881, 111, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (882, 111, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (883, 111, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (884, 111, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (885, 111, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (886, 111, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (887, 111, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (888, 111, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (889, 111, 'giadinh_giaphap_1_title', 'Giải pháp khóa khách sạn<br/>');
INSERT INTO `wp_postmeta` VALUES (890, 111, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (891, 111, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (892, 111, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (893, 111, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (894, 111, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (895, 111, 'giadinh_giaphap_1_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (896, 111, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (897, 111, 'giadinh_giaphap_1_background', '110');
INSERT INTO `wp_postmeta` VALUES (898, 111, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (899, 111, 'giadinh_giaphap_2_title', 'Marthome<br/>');
INSERT INTO `wp_postmeta` VALUES (900, 111, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (901, 111, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (902, 111, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (903, 111, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (904, 111, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (905, 111, 'giadinh_giaphap_2_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (906, 111, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (907, 111, 'giadinh_giaphap_2_background', '50');
INSERT INTO `wp_postmeta` VALUES (908, 111, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (909, 111, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (910, 111, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (911, 111, 'background', '');
INSERT INTO `wp_postmeta` VALUES (912, 111, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (913, 111, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (914, 111, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (915, 111, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (916, 111, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (917, 112, '_wp_attached_file', '2019/03/smarthome_slide.png');
INSERT INTO `wp_postmeta` VALUES (918, 112, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1371;s:6:\"height\";i:822;s:4:\"file\";s:27:\"2019/03/smarthome_slide.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-768x460.png\";s:5:\"width\";i:768;s:6:\"height\";i:460;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"smarthome_slide-1024x614.png\";s:5:\"width\";i:1024;s:6:\"height\";i:614;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"smarthome_slide-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-600x360.png\";s:5:\"width\";i:600;s:6:\"height\";i:360;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-600x360.png\";s:5:\"width\";i:600;s:6:\"height\";i:360;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"smarthome_slide-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (919, 113, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (920, 113, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (921, 113, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (922, 113, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (923, 113, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (924, 113, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (925, 113, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (926, 113, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (927, 113, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (928, 113, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (929, 113, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (930, 113, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (931, 113, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (932, 113, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (933, 113, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (934, 113, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (935, 113, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (936, 113, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (937, 113, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (938, 113, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (939, 113, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (940, 113, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (941, 113, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (942, 113, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (943, 113, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (944, 113, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (945, 113, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (946, 113, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (947, 113, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (948, 113, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (949, 113, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (950, 113, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (951, 113, 'giadinh_giaphap_1_title', 'Giải pháp khóa khách sạn<br/>');
INSERT INTO `wp_postmeta` VALUES (952, 113, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (953, 113, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (954, 113, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (955, 113, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (956, 113, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (957, 113, 'giadinh_giaphap_1_desc', 'Quản lý Khách sạn quy mô từ 2 sao với giải pháp quản lý 4.0, giúp tối ưu doanh thu và tiết kiệm chi phí');
INSERT INTO `wp_postmeta` VALUES (958, 113, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (959, 113, 'giadinh_giaphap_1_background', '110');
INSERT INTO `wp_postmeta` VALUES (960, 113, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (961, 113, 'giadinh_giaphap_2_title', 'Smarthome<br/>');
INSERT INTO `wp_postmeta` VALUES (962, 113, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (963, 113, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (964, 113, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (965, 113, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (966, 113, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (967, 113, 'giadinh_giaphap_2_desc', 'Kết nối các thiết bị thông minh với nhau, giúp nâng cao tiện nghi cho ngôi nhà của bạn, tự động hoá nhiệm vụ của các thiết bị trong nhà.');
INSERT INTO `wp_postmeta` VALUES (968, 113, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (969, 113, 'giadinh_giaphap_2_background', '112');
INSERT INTO `wp_postmeta` VALUES (970, 113, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (971, 113, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (972, 113, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (973, 113, 'background', '');
INSERT INTO `wp_postmeta` VALUES (974, 113, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (975, 113, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (976, 113, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (977, 113, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (978, 113, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (979, 114, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (980, 114, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (981, 114, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (982, 114, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (983, 114, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (984, 114, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (985, 114, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (986, 114, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (987, 114, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (988, 114, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (989, 114, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (990, 114, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (991, 114, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (992, 114, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (993, 114, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (994, 114, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (995, 114, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (996, 114, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (997, 114, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (998, 114, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (999, 114, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (1000, 114, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1001, 114, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (1002, 114, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1003, 114, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (1004, 114, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1005, 114, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (1006, 114, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1007, 114, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (1008, 114, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (1009, 114, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (1010, 114, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1011, 114, 'giadinh_giaphap_1_title', 'Giải pháp khóa khách sạn<br/>');
INSERT INTO `wp_postmeta` VALUES (1012, 114, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1013, 114, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (1014, 114, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1015, 114, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (1016, 114, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1017, 114, 'giadinh_giaphap_1_desc', 'Quản lý Khách sạn quy mô từ 2 sao với giải pháp quản lý 4.0, giúp tối ưu doanh thu và tiết kiệm chi phí');
INSERT INTO `wp_postmeta` VALUES (1018, 114, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1019, 114, 'giadinh_giaphap_1_background', '110');
INSERT INTO `wp_postmeta` VALUES (1020, 114, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1021, 114, 'giadinh_giaphap_2_title', '<br/><br/>Smarthome');
INSERT INTO `wp_postmeta` VALUES (1022, 114, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1023, 114, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (1024, 114, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1025, 114, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (1026, 114, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1027, 114, 'giadinh_giaphap_2_desc', 'Kết nối các thiết bị thông minh với nhau, giúp nâng cao tiện nghi cho ngôi nhà của bạn, tự động hoá nhiệm vụ của các thiết bị trong nhà.');
INSERT INTO `wp_postmeta` VALUES (1028, 114, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1029, 114, 'giadinh_giaphap_2_background', '112');
INSERT INTO `wp_postmeta` VALUES (1030, 114, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1031, 114, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1032, 114, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1033, 114, 'background', '');
INSERT INTO `wp_postmeta` VALUES (1034, 114, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1035, 114, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1036, 114, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1037, 114, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1038, 114, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1039, 115, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (1040, 115, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (1041, 115, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (1042, 115, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (1043, 115, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (1044, 115, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (1045, 115, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (1046, 115, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (1047, 115, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (1048, 115, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (1049, 115, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (1050, 115, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (1051, 115, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (1052, 115, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (1053, 115, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (1054, 115, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (1055, 115, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (1056, 115, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (1057, 115, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (1058, 115, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (1059, 115, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (1060, 115, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1061, 115, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (1062, 115, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1063, 115, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (1064, 115, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1065, 115, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (1066, 115, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1067, 115, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (1068, 115, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (1069, 115, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (1070, 115, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1071, 115, 'giadinh_giaphap_1_title', 'Giải pháp khóa khách sạn<br/>');
INSERT INTO `wp_postmeta` VALUES (1072, 115, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1073, 115, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (1074, 115, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1075, 115, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (1076, 115, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1077, 115, 'giadinh_giaphap_1_desc', 'Quản lý Khách sạn quy mô từ 2 sao với giải pháp quản lý 4.0, giúp tối ưu doanh thu và tiết kiệm chi phí');
INSERT INTO `wp_postmeta` VALUES (1078, 115, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1079, 115, 'giadinh_giaphap_1_background', '110');
INSERT INTO `wp_postmeta` VALUES (1080, 115, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1081, 115, 'giadinh_giaphap_2_title', '<br/>Smarthome');
INSERT INTO `wp_postmeta` VALUES (1082, 115, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1083, 115, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (1084, 115, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1085, 115, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (1086, 115, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1087, 115, 'giadinh_giaphap_2_desc', 'Kết nối các thiết bị thông minh với nhau, giúp nâng cao tiện nghi cho ngôi nhà của bạn, tự động hoá nhiệm vụ của các thiết bị trong nhà.');
INSERT INTO `wp_postmeta` VALUES (1088, 115, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1089, 115, 'giadinh_giaphap_2_background', '112');
INSERT INTO `wp_postmeta` VALUES (1090, 115, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1091, 115, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1092, 115, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1093, 115, 'background', '');
INSERT INTO `wp_postmeta` VALUES (1094, 115, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1095, 115, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1096, 115, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1097, 115, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1098, 115, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1099, 116, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (1100, 116, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (1101, 116, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (1102, 116, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (1103, 116, 'whoweaare_link', '#');
INSERT INTO `wp_postmeta` VALUES (1104, 116, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (1105, 116, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (1106, 116, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (1107, 116, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (1108, 116, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (1109, 116, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (1110, 116, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (1111, 116, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (1112, 116, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (1113, 116, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (1114, 116, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (1115, 116, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (1116, 116, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (1117, 116, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (1118, 116, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (1119, 116, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (1120, 116, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1121, 116, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (1122, 116, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1123, 116, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (1124, 116, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1125, 116, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (1126, 116, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1127, 116, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (1128, 116, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (1129, 116, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (1130, 116, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1131, 116, 'giadinh_giaphap_1_title', '<br/>Giải pháp khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (1132, 116, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1133, 116, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (1134, 116, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1135, 116, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (1136, 116, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1137, 116, 'giadinh_giaphap_1_desc', 'Quản lý Khách sạn quy mô từ 2 sao với giải pháp quản lý 4.0, giúp tối ưu doanh thu và tiết kiệm chi phí');
INSERT INTO `wp_postmeta` VALUES (1138, 116, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1139, 116, 'giadinh_giaphap_1_background', '110');
INSERT INTO `wp_postmeta` VALUES (1140, 116, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1141, 116, 'giadinh_giaphap_2_title', '<br/>Smarthome');
INSERT INTO `wp_postmeta` VALUES (1142, 116, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1143, 116, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (1144, 116, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1145, 116, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (1146, 116, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1147, 116, 'giadinh_giaphap_2_desc', 'Kết nối các thiết bị thông minh với nhau, giúp nâng cao tiện nghi cho ngôi nhà của bạn, tự động hoá nhiệm vụ của các thiết bị trong nhà.');
INSERT INTO `wp_postmeta` VALUES (1148, 116, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1149, 116, 'giadinh_giaphap_2_background', '112');
INSERT INTO `wp_postmeta` VALUES (1150, 116, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1151, 116, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1152, 116, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1153, 116, 'background', '');
INSERT INTO `wp_postmeta` VALUES (1154, 116, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1155, 116, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1156, 116, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1157, 116, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1158, 116, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1159, 117, 'thumb', '83');
INSERT INTO `wp_postmeta` VALUES (1160, 117, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1161, 117, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1162, 117, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1163, 117, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1164, 117, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1165, 117, 'desc', 'Chúng tôi cung cấp dịch vụ vượt trội dựa trên nhiều năm kinh nghiệm thực hiện các dự án lớn với chính phủ và các cơ quan công của Hàn Quốc thực hiện bởi đội ngũ kỹ thuật viên, chuyên gia đầu ngành của chúng tôi.\r\n');
INSERT INTO `wp_postmeta` VALUES (1166, 117, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1167, 80, '_wp_page_template', 'template-xaydunghethong.php');
INSERT INTO `wp_postmeta` VALUES (1168, 118, 'thumb', '83');
INSERT INTO `wp_postmeta` VALUES (1169, 118, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1170, 118, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1171, 118, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1172, 118, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1173, 118, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1174, 118, 'desc', 'Chúng tôi cung cấp dịch vụ vượt trội dựa trên nhiều năm kinh nghiệm thực hiện các dự án lớn với chính phủ và các cơ quan công của Hàn Quốc thực hiện bởi đội ngũ kỹ thuật viên, chuyên gia đầu ngành của chúng tôi.\r\n');
INSERT INTO `wp_postmeta` VALUES (1175, 118, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1176, 119, 'thumb', '83');
INSERT INTO `wp_postmeta` VALUES (1177, 119, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1178, 119, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1179, 119, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1180, 119, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1181, 119, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1182, 119, 'desc', 'Chúng tôi cung cấp dịch vụ vượt trội dựa trên nhiều năm kinh nghiệm thực hiện các dự án lớn với chính phủ và các cơ quan công của Hàn Quốc thực hiện bởi đội ngũ kỹ thuật viên, chuyên gia đầu ngành của chúng tôi.\r\n');
INSERT INTO `wp_postmeta` VALUES (1183, 119, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1185, 11, '_wp_page_template', 'template-tintuc.php');
INSERT INTO `wp_postmeta` VALUES (1186, 11, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1187, 11, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1188, 11, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1189, 11, 'background', '121');
INSERT INTO `wp_postmeta` VALUES (1190, 11, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1191, 11, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1192, 11, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1193, 11, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1194, 11, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1195, 12, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1196, 12, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1197, 12, 'background', '');
INSERT INTO `wp_postmeta` VALUES (1198, 12, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1199, 12, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1200, 12, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1201, 12, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1202, 12, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1203, 121, '_wp_attached_file', '2019/03/tintuc_head_bg.jpg');
INSERT INTO `wp_postmeta` VALUES (1204, 121, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1365;s:6:\"height\";i:298;s:4:\"file\";s:26:\"2019/03/tintuc_head_bg.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"tintuc_head_bg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"tintuc_head_bg-300x65.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:65;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"tintuc_head_bg-768x168.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:168;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"tintuc_head_bg-1024x224.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"tintuc_head_bg-300x298.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:26:\"tintuc_head_bg-600x131.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:131;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"tintuc_head_bg-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"tintuc_head_bg-300x298.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:26:\"tintuc_head_bg-600x131.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:131;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"tintuc_head_bg-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1205, 122, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1206, 122, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1207, 122, 'background', '121');
INSERT INTO `wp_postmeta` VALUES (1208, 122, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1209, 122, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1210, 122, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1211, 122, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1212, 122, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1215, 18, '_thumbnail_id', '37');
INSERT INTO `wp_postmeta` VALUES (1217, 124, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1218, 124, '_edit_lock', '1552410030:1');
INSERT INTO `wp_postmeta` VALUES (1219, 126, '_edit_lock', '1551984216:1');
INSERT INTO `wp_postmeta` VALUES (1220, 127, '_wp_attached_file', '2019/03/564_chuong_cua_co_hinh_samsung_sht_7017_cn610e.jpg');
INSERT INTO `wp_postmeta` VALUES (1221, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:441;s:4:\"file\";s:58:\"2019/03/564_chuong_cua_co_hinh_samsung_sht_7017_cn610e.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-300x221.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:221;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-600x441.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:441;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-600x441.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:441;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:58:\"564_chuong_cua_co_hinh_samsung_sht_7017_cn610e-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1224, 126, '_thumbnail_id', '127');
INSERT INTO `wp_postmeta` VALUES (1227, 126, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1230, 126, 'desc', 'Tính đến thời điểm hiện tại, model Epic EF-8000L đang làm mưa làm gió trên thị trường, nhận được sự chú ý của rất nhiều người. Bài viết dưới đây của dhome.vn sẽ giúp các bạn hiểu rõ hơn về đặc điểm của thiết bị!');
INSERT INTO `wp_postmeta` VALUES (1231, 126, '_desc', 'field_5c815d073ae7d');
INSERT INTO `wp_postmeta` VALUES (1232, 130, 'desc', 'Tính đến thời điểm hiện tại, model Epic EF-8000L đang làm mưa làm gió trên thị trường, nhận được sự chú ý của rất nhiều người. Bài viết dưới đây của dhome.vn sẽ giúp các bạn hiểu rõ hơn về đặc điểm của thiết bị!');
INSERT INTO `wp_postmeta` VALUES (1233, 130, '_desc', 'field_5c815d073ae7d');
INSERT INTO `wp_postmeta` VALUES (1234, 9, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1235, 9, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1236, 9, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1237, 9, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1238, 9, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1239, 9, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1240, 9, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1241, 9, 'desc', 'Là công ty hàng đầu cung cấp sản phẩm và giải pháp kiểm soát ra vào và an ninh cho gia đình và doanh nghiệp\r\n\r\n');
INSERT INTO `wp_postmeta` VALUES (1242, 9, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1243, 131, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1244, 131, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1245, 131, 'background', '112');
INSERT INTO `wp_postmeta` VALUES (1246, 131, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1247, 131, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1248, 131, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1249, 131, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1250, 131, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1251, 132, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1252, 132, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1253, 132, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1254, 132, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1255, 132, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1256, 132, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1257, 132, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1258, 132, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1259, 133, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1260, 133, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1261, 133, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1262, 133, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1263, 133, 'image_desc', '104');
INSERT INTO `wp_postmeta` VALUES (1264, 133, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1265, 133, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1266, 133, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1267, 134, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1268, 134, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1269, 134, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1270, 134, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1271, 134, 'image_desc', '28');
INSERT INTO `wp_postmeta` VALUES (1272, 134, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1273, 134, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1274, 134, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1275, 135, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1276, 135, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1277, 135, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1278, 135, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1279, 135, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1280, 135, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1281, 135, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1282, 135, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1283, 136, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1284, 136, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1285, 136, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1286, 136, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1287, 136, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1288, 136, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1289, 136, 'desc', 'Là công ty hàng đầu cung cấp sản phẩm và giải pháp kiểm soát ra vào và an ninh cho gia đình và doanh nghiệp\r\n\r\n');
INSERT INTO `wp_postmeta` VALUES (1290, 136, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1291, 57, '_edit_lock', '1551984992:1');
INSERT INTO `wp_postmeta` VALUES (1292, 57, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1293, 57, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1294, 57, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1295, 57, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1296, 57, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1297, 57, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1298, 57, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1299, 57, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1300, 57, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1301, 138, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1302, 138, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1303, 138, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1304, 138, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1305, 138, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1306, 138, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1307, 138, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1308, 138, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1309, 66, 'image_gallery', '');
INSERT INTO `wp_postmeta` VALUES (1310, 66, '_image_gallery', 'field_5c7ffc18024c7');
INSERT INTO `wp_postmeta` VALUES (1311, 68, 'image_gallery', '');
INSERT INTO `wp_postmeta` VALUES (1312, 68, '_image_gallery', 'field_5c7ffc18024c7');
INSERT INTO `wp_postmeta` VALUES (1313, 140, '_edit_lock', '1552409946:1');
INSERT INTO `wp_postmeta` VALUES (1314, 141, '_edit_lock', '1552410036:1');
INSERT INTO `wp_postmeta` VALUES (1317, 18, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1320, 18, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1321, 18, '_desc', 'field_5c815d073ae7d');
INSERT INTO `wp_postmeta` VALUES (1322, 19, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1323, 19, '_desc', 'field_5c815d073ae7d');
INSERT INTO `wp_postmeta` VALUES (1324, 142, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1325, 142, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1326, 142, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1327, 142, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1328, 142, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1329, 142, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1330, 142, 'desc', 'Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Cac giải pháp và dịch vụ thông minh, dữ liệu lớn và ứng dụng di động\r\n');
INSERT INTO `wp_postmeta` VALUES (1331, 142, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1332, 143, '_menu_item_type', 'custom');
INSERT INTO `wp_postmeta` VALUES (1333, 143, '_menu_item_menu_item_parent', '0');
INSERT INTO `wp_postmeta` VALUES (1334, 143, '_menu_item_object_id', '143');
INSERT INTO `wp_postmeta` VALUES (1335, 143, '_menu_item_object', 'custom');
INSERT INTO `wp_postmeta` VALUES (1336, 143, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (1337, 143, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (1338, 143, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (1339, 143, '_menu_item_url', '#');
INSERT INTO `wp_postmeta` VALUES (1341, 144, '_menu_item_type', 'taxonomy');
INSERT INTO `wp_postmeta` VALUES (1342, 144, '_menu_item_menu_item_parent', '143');
INSERT INTO `wp_postmeta` VALUES (1343, 144, '_menu_item_object_id', '24');
INSERT INTO `wp_postmeta` VALUES (1344, 144, '_menu_item_object', 'product_cat');
INSERT INTO `wp_postmeta` VALUES (1345, 144, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (1346, 144, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (1347, 144, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (1348, 144, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (1359, 146, '_menu_item_type', 'taxonomy');
INSERT INTO `wp_postmeta` VALUES (1360, 146, '_menu_item_menu_item_parent', '143');
INSERT INTO `wp_postmeta` VALUES (1361, 146, '_menu_item_object_id', '23');
INSERT INTO `wp_postmeta` VALUES (1362, 146, '_menu_item_object', 'product_cat');
INSERT INTO `wp_postmeta` VALUES (1363, 146, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (1364, 146, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (1365, 146, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (1366, 146, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (1377, 148, '_menu_item_type', 'taxonomy');
INSERT INTO `wp_postmeta` VALUES (1378, 148, '_menu_item_menu_item_parent', '143');
INSERT INTO `wp_postmeta` VALUES (1379, 148, '_menu_item_object_id', '25');
INSERT INTO `wp_postmeta` VALUES (1380, 148, '_menu_item_object', 'product_cat');
INSERT INTO `wp_postmeta` VALUES (1381, 148, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (1382, 148, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (1383, 148, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (1384, 148, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (1386, 149, '_menu_item_type', 'taxonomy');
INSERT INTO `wp_postmeta` VALUES (1387, 149, '_menu_item_menu_item_parent', '143');
INSERT INTO `wp_postmeta` VALUES (1388, 149, '_menu_item_object_id', '21');
INSERT INTO `wp_postmeta` VALUES (1389, 149, '_menu_item_object', 'product_cat');
INSERT INTO `wp_postmeta` VALUES (1390, 149, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (1391, 149, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (1392, 149, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (1393, 149, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (1395, 150, '_edit_lock', '1552464844:1');
INSERT INTO `wp_postmeta` VALUES (1396, 150, '_wp_page_template', 'template-khoa-khach-san.php');
INSERT INTO `wp_postmeta` VALUES (1397, 150, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1398, 150, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1399, 150, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1400, 150, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1401, 150, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1402, 150, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1403, 150, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1404, 150, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1405, 150, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1406, 151, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1407, 151, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1408, 151, 'background', '');
INSERT INTO `wp_postmeta` VALUES (1409, 151, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1410, 151, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1411, 151, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1412, 151, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1413, 151, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1414, 152, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1415, 152, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1416, 152, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1417, 152, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1418, 152, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1419, 152, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1420, 152, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1421, 152, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1422, 153, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1423, 153, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1424, 153, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1425, 153, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1426, 153, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1427, 153, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1428, 153, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1429, 153, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1430, 154, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1431, 154, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1432, 154, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1433, 154, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1434, 154, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1435, 154, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1436, 154, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1437, 154, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1438, 155, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1439, 155, '_edit_lock', '1552462891:1');
INSERT INTO `wp_postmeta` VALUES (1440, 167, '_wp_attached_file', '2019/03/kks-trainghiemkh-bg.png');
INSERT INTO `wp_postmeta` VALUES (1441, 167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:912;s:6:\"height\";i:415;s:4:\"file\";s:31:\"2019/03/kks-trainghiemkh-bg.png\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-300x137.png\";s:5:\"width\";i:300;s:6:\"height\";i:137;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-768x349.png\";s:5:\"width\";i:768;s:6:\"height\";i:349;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-600x273.png\";s:5:\"width\";i:600;s:6:\"height\";i:273;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-600x273.png\";s:5:\"width\";i:600;s:6:\"height\";i:273;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"kks-trainghiemkh-bg-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1442, 150, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1443, 150, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1444, 150, 'kks_block_0_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1445, 150, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1446, 150, 'kks_block_0_title', 'Nâng cao trải nghiệm cho khách hàng');
INSERT INTO `wp_postmeta` VALUES (1447, 150, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1448, 150, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1449, 150, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1450, 150, 'kks_block_0_desc', 'Mở khóa chỉ bằng 1 chạm sử dụng thẻ từ, độ nhạy và chính xác cao với công nghệ an ninh mới nhất từ Hàn Quốc. Giải pháp giúp đem lại trải nghiệm dịch vụ chuyên nghiệp 5 sao cho khách hàng về trải nghiệm mở cửa.');
INSERT INTO `wp_postmeta` VALUES (1451, 150, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1452, 150, 'kks_block', '4');
INSERT INTO `wp_postmeta` VALUES (1453, 150, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1454, 168, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1455, 168, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1456, 168, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1457, 168, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1458, 168, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1459, 168, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1460, 168, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1461, 168, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1462, 168, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1463, 168, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1464, 168, 'kks_block_0_background_color', '');
INSERT INTO `wp_postmeta` VALUES (1465, 168, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1466, 168, 'kks_block_0_title', '');
INSERT INTO `wp_postmeta` VALUES (1467, 168, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1468, 168, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1469, 168, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1470, 168, 'kks_block_0_desc', '');
INSERT INTO `wp_postmeta` VALUES (1471, 168, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1472, 168, 'kks_block', '1');
INSERT INTO `wp_postmeta` VALUES (1473, 168, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1474, 150, 'kks_block_0_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1475, 150, '_kks_block_0_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1476, 170, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1477, 170, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1478, 170, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1479, 170, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1480, 170, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1481, 170, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1482, 170, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1483, 170, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1484, 170, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1485, 170, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1486, 170, 'kks_block_0_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1487, 170, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1488, 170, 'kks_block_0_title', 'Nâng cao trải nghiệm cho khách hàng');
INSERT INTO `wp_postmeta` VALUES (1489, 170, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1490, 170, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1491, 170, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1492, 170, 'kks_block_0_desc', 'Mở khóa chỉ bằng 1 chạm sử dụng thẻ từ, độ nhạy và chính xác cao với công nghệ an ninh mới nhất từ Hàn Quốc. Giải pháp giúp đem lại trải nghiệm dịch vụ chuyên nghiệp 5 sao cho khách hàng về trải nghiệm mở cửa.');
INSERT INTO `wp_postmeta` VALUES (1493, 170, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1494, 170, 'kks_block', '1');
INSERT INTO `wp_postmeta` VALUES (1495, 170, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1496, 170, 'kks_block_0_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1497, 170, '_kks_block_0_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1498, 171, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1499, 171, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1500, 171, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1501, 171, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1502, 171, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1503, 171, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1504, 171, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1505, 171, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1506, 171, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1507, 171, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1508, 171, 'kks_block_0_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1509, 171, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1510, 171, 'kks_block_0_title', 'Nâng cao trải nghiệm cho khách hàng');
INSERT INTO `wp_postmeta` VALUES (1511, 171, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1512, 171, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1513, 171, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1514, 171, 'kks_block_0_desc', 'Mở khóa chỉ bằng 1 chạm sử dụng thẻ từ, độ nhạy và chính xác cao với công nghệ an ninh mới nhất từ Hàn Quốc. Giải pháp giúp đem lại trải nghiệm dịch vụ chuyên nghiệp 5 sao cho khách hàng về trải nghiệm mở cửa.');
INSERT INTO `wp_postmeta` VALUES (1515, 171, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1516, 171, 'kks_block', '1');
INSERT INTO `wp_postmeta` VALUES (1517, 171, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1518, 171, 'kks_block_0_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1519, 171, '_kks_block_0_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1520, 172, '_wp_attached_file', '2019/03/kks-giaiphapquanly-bg.png');
INSERT INTO `wp_postmeta` VALUES (1521, 172, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:719;s:6:\"height\";i:404;s:4:\"file\";s:33:\"2019/03/kks-giaiphapquanly-bg.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-600x337.png\";s:5:\"width\";i:600;s:6:\"height\";i:337;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-600x337.png\";s:5:\"width\";i:600;s:6:\"height\";i:337;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"kks-giaiphapquanly-bg-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1522, 150, 'kks_block_1_background', '172');
INSERT INTO `wp_postmeta` VALUES (1523, 150, '_kks_block_1_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1524, 150, 'kks_block_1_background_color', '#c1b2ad');
INSERT INTO `wp_postmeta` VALUES (1525, 150, '_kks_block_1_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1526, 150, 'kks_block_1_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1527, 150, '_kks_block_1_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1528, 150, 'kks_block_1_title', 'Giải pháp quản lý phòng tiện lợi và nhanh chóng');
INSERT INTO `wp_postmeta` VALUES (1529, 150, '_kks_block_1_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1530, 150, 'kks_block_1_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1531, 150, '_kks_block_1_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1532, 150, 'kks_block_1_desc', 'Cập nhật trạng thái phòng chính xác và nhanh chóng. Hệ thống lưu lại lịch sử giờ check-in,\r\n                        check-out của khách hàng, tích hợp các dịch vụ giặt là, quầy bar, giải trí..., thuận tiện thanh\r\n                        toán cho khách hàng.');
INSERT INTO `wp_postmeta` VALUES (1533, 150, '_kks_block_1_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1534, 173, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1535, 173, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1536, 173, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1537, 173, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1538, 173, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1539, 173, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1540, 173, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1541, 173, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1542, 173, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1543, 173, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1544, 173, 'kks_block_0_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1545, 173, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1546, 173, 'kks_block_0_title', 'Nâng cao trải nghiệm cho khách hàng');
INSERT INTO `wp_postmeta` VALUES (1547, 173, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1548, 173, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1549, 173, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1550, 173, 'kks_block_0_desc', 'Mở khóa chỉ bằng 1 chạm sử dụng thẻ từ, độ nhạy và chính xác cao với công nghệ an ninh mới nhất từ Hàn Quốc. Giải pháp giúp đem lại trải nghiệm dịch vụ chuyên nghiệp 5 sao cho khách hàng về trải nghiệm mở cửa.');
INSERT INTO `wp_postmeta` VALUES (1551, 173, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1552, 173, 'kks_block', '2');
INSERT INTO `wp_postmeta` VALUES (1553, 173, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1554, 173, 'kks_block_0_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1555, 173, '_kks_block_0_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1556, 173, 'kks_block_1_background', '172');
INSERT INTO `wp_postmeta` VALUES (1557, 173, '_kks_block_1_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1558, 173, 'kks_block_1_background_color', '#c1b2ad');
INSERT INTO `wp_postmeta` VALUES (1559, 173, '_kks_block_1_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1560, 173, 'kks_block_1_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1561, 173, '_kks_block_1_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1562, 173, 'kks_block_1_title', 'Giải pháp quản lý phòng tiện lợi và nhanh chóng');
INSERT INTO `wp_postmeta` VALUES (1563, 173, '_kks_block_1_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1564, 173, 'kks_block_1_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1565, 173, '_kks_block_1_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1566, 173, 'kks_block_1_desc', 'Cập nhật trạng thái phòng chính xác và nhanh chóng. Hệ thống lưu lại lịch sử giờ check-in,\r\n                        check-out của khách hàng, tích hợp các dịch vụ giặt là, quầy bar, giải trí..., thuận tiện thanh\r\n                        toán cho khách hàng.');
INSERT INTO `wp_postmeta` VALUES (1567, 173, '_kks_block_1_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1568, 174, '_wp_attached_file', '2019/03/kks-tietkiemchiphi.png');
INSERT INTO `wp_postmeta` VALUES (1569, 174, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:719;s:6:\"height\";i:404;s:4:\"file\";s:30:\"2019/03/kks-tietkiemchiphi.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-600x337.png\";s:5:\"width\";i:600;s:6:\"height\";i:337;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-600x337.png\";s:5:\"width\";i:600;s:6:\"height\";i:337;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"kks-tietkiemchiphi-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1570, 150, 'kks_block_2_background', '174');
INSERT INTO `wp_postmeta` VALUES (1571, 150, '_kks_block_2_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1572, 150, 'kks_block_2_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1573, 150, '_kks_block_2_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1574, 150, 'kks_block_2_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1575, 150, '_kks_block_2_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1576, 150, 'kks_block_2_title', 'Tiết kiệm chi phí và nhân lực');
INSERT INTO `wp_postmeta` VALUES (1577, 150, '_kks_block_2_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1578, 150, 'kks_block_2_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1579, 150, '_kks_block_2_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1580, 150, 'kks_block_2_desc', 'Tiết kiệm nhân lực quản lý phòng và nhân lực giải quyết các sự cố gây ra bởi khóa cơ, check hiện trạng phòng nhanh chóng. Thẻ từ kết hợp với hệ thống tiết kiệm điện, giúp giảm thiểu thất thoát tiền điện.');
INSERT INTO `wp_postmeta` VALUES (1581, 150, '_kks_block_2_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1582, 175, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1583, 175, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1584, 175, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1585, 175, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1586, 175, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1587, 175, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1588, 175, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1589, 175, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1590, 175, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1591, 175, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1592, 175, 'kks_block_0_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1593, 175, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1594, 175, 'kks_block_0_title', 'Nâng cao trải nghiệm cho khách hàng');
INSERT INTO `wp_postmeta` VALUES (1595, 175, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1596, 175, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1597, 175, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1598, 175, 'kks_block_0_desc', 'Mở khóa chỉ bằng 1 chạm sử dụng thẻ từ, độ nhạy và chính xác cao với công nghệ an ninh mới nhất từ Hàn Quốc. Giải pháp giúp đem lại trải nghiệm dịch vụ chuyên nghiệp 5 sao cho khách hàng về trải nghiệm mở cửa.');
INSERT INTO `wp_postmeta` VALUES (1599, 175, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1600, 175, 'kks_block', '3');
INSERT INTO `wp_postmeta` VALUES (1601, 175, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1602, 175, 'kks_block_0_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1603, 175, '_kks_block_0_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1604, 175, 'kks_block_1_background', '172');
INSERT INTO `wp_postmeta` VALUES (1605, 175, '_kks_block_1_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1606, 175, 'kks_block_1_background_color', '#c1b2ad');
INSERT INTO `wp_postmeta` VALUES (1607, 175, '_kks_block_1_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1608, 175, 'kks_block_1_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1609, 175, '_kks_block_1_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1610, 175, 'kks_block_1_title', 'Giải pháp quản lý phòng tiện lợi và nhanh chóng');
INSERT INTO `wp_postmeta` VALUES (1611, 175, '_kks_block_1_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1612, 175, 'kks_block_1_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1613, 175, '_kks_block_1_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1614, 175, 'kks_block_1_desc', 'Cập nhật trạng thái phòng chính xác và nhanh chóng. Hệ thống lưu lại lịch sử giờ check-in,\r\n                        check-out của khách hàng, tích hợp các dịch vụ giặt là, quầy bar, giải trí..., thuận tiện thanh\r\n                        toán cho khách hàng.');
INSERT INTO `wp_postmeta` VALUES (1615, 175, '_kks_block_1_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1616, 175, 'kks_block_2_background', '174');
INSERT INTO `wp_postmeta` VALUES (1617, 175, '_kks_block_2_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1618, 175, 'kks_block_2_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1619, 175, '_kks_block_2_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1620, 175, 'kks_block_2_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1621, 175, '_kks_block_2_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1622, 175, 'kks_block_2_title', 'Tiết kiệm chi phí và nhân lực');
INSERT INTO `wp_postmeta` VALUES (1623, 175, '_kks_block_2_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1624, 175, 'kks_block_2_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1625, 175, '_kks_block_2_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1626, 175, 'kks_block_2_desc', 'Tiết kiệm nhân lực quản lý phòng và nhân lực giải quyết các sự cố gây ra bởi khóa cơ, check hiện trạng phòng nhanh chóng. Thẻ từ kết hợp với hệ thống tiết kiệm điện, giúp giảm thiểu thất thoát tiền điện.');
INSERT INTO `wp_postmeta` VALUES (1627, 175, '_kks_block_2_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1628, 176, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1629, 176, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1630, 176, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1631, 176, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1632, 176, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1633, 176, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1634, 176, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1635, 176, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1636, 176, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1637, 176, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1638, 176, 'kks_block_0_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1639, 176, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1640, 176, 'kks_block_0_title', 'Nâng cao trải nghiệm cho khách hàng');
INSERT INTO `wp_postmeta` VALUES (1641, 176, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1642, 176, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1643, 176, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1644, 176, 'kks_block_0_desc', 'Mở khóa chỉ bằng 1 chạm sử dụng thẻ từ, độ nhạy và chính xác cao với công nghệ an ninh mới nhất từ Hàn Quốc. Giải pháp giúp đem lại trải nghiệm dịch vụ chuyên nghiệp 5 sao cho khách hàng về trải nghiệm mở cửa.');
INSERT INTO `wp_postmeta` VALUES (1645, 176, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1646, 176, 'kks_block', '3');
INSERT INTO `wp_postmeta` VALUES (1647, 176, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1648, 176, 'kks_block_0_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1649, 176, '_kks_block_0_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1650, 176, 'kks_block_1_background', '172');
INSERT INTO `wp_postmeta` VALUES (1651, 176, '_kks_block_1_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1652, 176, 'kks_block_1_background_color', '#c1b2ad');
INSERT INTO `wp_postmeta` VALUES (1653, 176, '_kks_block_1_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1654, 176, 'kks_block_1_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1655, 176, '_kks_block_1_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1656, 176, 'kks_block_1_title', 'Giải pháp quản lý phòng tiện lợi và nhanh chóng');
INSERT INTO `wp_postmeta` VALUES (1657, 176, '_kks_block_1_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1658, 176, 'kks_block_1_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1659, 176, '_kks_block_1_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1660, 176, 'kks_block_1_desc', 'Cập nhật trạng thái phòng chính xác và nhanh chóng. Hệ thống lưu lại lịch sử giờ check-in,\r\n                        check-out của khách hàng, tích hợp các dịch vụ giặt là, quầy bar, giải trí..., thuận tiện thanh\r\n                        toán cho khách hàng.');
INSERT INTO `wp_postmeta` VALUES (1661, 176, '_kks_block_1_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1662, 176, 'kks_block_2_background', '174');
INSERT INTO `wp_postmeta` VALUES (1663, 176, '_kks_block_2_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1664, 176, 'kks_block_2_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1665, 176, '_kks_block_2_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1666, 176, 'kks_block_2_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1667, 176, '_kks_block_2_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1668, 176, 'kks_block_2_title', 'Tiết kiệm chi phí và nhân lực');
INSERT INTO `wp_postmeta` VALUES (1669, 176, '_kks_block_2_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1670, 176, 'kks_block_2_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1671, 176, '_kks_block_2_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1672, 176, 'kks_block_2_desc', 'Tiết kiệm nhân lực quản lý phòng và nhân lực giải quyết các sự cố gây ra bởi khóa cơ, check hiện trạng phòng nhanh chóng. Thẻ từ kết hợp với hệ thống tiết kiệm điện, giúp giảm thiểu thất thoát tiền điện.');
INSERT INTO `wp_postmeta` VALUES (1673, 176, '_kks_block_2_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1674, 177, '_wp_attached_file', '2019/03/kss_kiemsoattaichinh-bg.png');
INSERT INTO `wp_postmeta` VALUES (1675, 177, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:719;s:6:\"height\";i:404;s:4:\"file\";s:35:\"2019/03/kss_kiemsoattaichinh-bg.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-600x337.png\";s:5:\"width\";i:600;s:6:\"height\";i:337;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-600x337.png\";s:5:\"width\";i:600;s:6:\"height\";i:337;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"kss_kiemsoattaichinh-bg-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1676, 150, 'kks_block_3_background', '177');
INSERT INTO `wp_postmeta` VALUES (1677, 150, '_kks_block_3_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1678, 150, 'kks_block_3_background_color', '#f7f7ff');
INSERT INTO `wp_postmeta` VALUES (1679, 150, '_kks_block_3_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1680, 150, 'kks_block_3_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1681, 150, '_kks_block_3_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1682, 150, 'kks_block_3_title', 'Kiểm soát tài chính cho khách sạn');
INSERT INTO `wp_postmeta` VALUES (1683, 150, '_kks_block_3_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1684, 150, 'kks_block_3_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1685, 150, '_kks_block_3_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1686, 150, 'kks_block_3_desc', 'Các thẻ từ đều qua hệ thống phần mềm mới có thể được sử dụng, giúp giảm thiểu thất thoát và quản lý công suất phòng hiệu quả.');
INSERT INTO `wp_postmeta` VALUES (1687, 150, '_kks_block_3_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1688, 178, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1689, 178, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1690, 178, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1691, 178, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1692, 178, 'image_desc', '67');
INSERT INTO `wp_postmeta` VALUES (1693, 178, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1694, 178, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1695, 178, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1696, 178, 'kks_block_0_background', '167');
INSERT INTO `wp_postmeta` VALUES (1697, 178, '_kks_block_0_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1698, 178, 'kks_block_0_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1699, 178, '_kks_block_0_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1700, 178, 'kks_block_0_title', 'Nâng cao trải nghiệm cho khách hàng');
INSERT INTO `wp_postmeta` VALUES (1701, 178, '_kks_block_0_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1702, 178, 'kks_block_0_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1703, 178, '_kks_block_0_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1704, 178, 'kks_block_0_desc', 'Mở khóa chỉ bằng 1 chạm sử dụng thẻ từ, độ nhạy và chính xác cao với công nghệ an ninh mới nhất từ Hàn Quốc. Giải pháp giúp đem lại trải nghiệm dịch vụ chuyên nghiệp 5 sao cho khách hàng về trải nghiệm mở cửa.');
INSERT INTO `wp_postmeta` VALUES (1705, 178, '_kks_block_0_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1706, 178, 'kks_block', '4');
INSERT INTO `wp_postmeta` VALUES (1707, 178, '_kks_block', 'field_5c8884a6f295a');
INSERT INTO `wp_postmeta` VALUES (1708, 178, 'kks_block_0_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1709, 178, '_kks_block_0_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1710, 178, 'kks_block_1_background', '172');
INSERT INTO `wp_postmeta` VALUES (1711, 178, '_kks_block_1_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1712, 178, 'kks_block_1_background_color', '#c1b2ad');
INSERT INTO `wp_postmeta` VALUES (1713, 178, '_kks_block_1_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1714, 178, 'kks_block_1_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1715, 178, '_kks_block_1_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1716, 178, 'kks_block_1_title', 'Giải pháp quản lý phòng tiện lợi và nhanh chóng');
INSERT INTO `wp_postmeta` VALUES (1717, 178, '_kks_block_1_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1718, 178, 'kks_block_1_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1719, 178, '_kks_block_1_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1720, 178, 'kks_block_1_desc', 'Cập nhật trạng thái phòng chính xác và nhanh chóng. Hệ thống lưu lại lịch sử giờ check-in,\r\n                        check-out của khách hàng, tích hợp các dịch vụ giặt là, quầy bar, giải trí..., thuận tiện thanh\r\n                        toán cho khách hàng.');
INSERT INTO `wp_postmeta` VALUES (1721, 178, '_kks_block_1_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1722, 178, 'kks_block_2_background', '174');
INSERT INTO `wp_postmeta` VALUES (1723, 178, '_kks_block_2_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1724, 178, 'kks_block_2_background_color', '#f0ece9');
INSERT INTO `wp_postmeta` VALUES (1725, 178, '_kks_block_2_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1726, 178, 'kks_block_2_align_content', 'left');
INSERT INTO `wp_postmeta` VALUES (1727, 178, '_kks_block_2_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1728, 178, 'kks_block_2_title', 'Tiết kiệm chi phí và nhân lực');
INSERT INTO `wp_postmeta` VALUES (1729, 178, '_kks_block_2_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1730, 178, 'kks_block_2_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1731, 178, '_kks_block_2_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1732, 178, 'kks_block_2_desc', 'Tiết kiệm nhân lực quản lý phòng và nhân lực giải quyết các sự cố gây ra bởi khóa cơ, check hiện trạng phòng nhanh chóng. Thẻ từ kết hợp với hệ thống tiết kiệm điện, giúp giảm thiểu thất thoát tiền điện.');
INSERT INTO `wp_postmeta` VALUES (1733, 178, '_kks_block_2_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1734, 178, 'kks_block_3_background', '177');
INSERT INTO `wp_postmeta` VALUES (1735, 178, '_kks_block_3_background', 'field_5c8884cbf295b');
INSERT INTO `wp_postmeta` VALUES (1736, 178, 'kks_block_3_background_color', '#f7f7ff');
INSERT INTO `wp_postmeta` VALUES (1737, 178, '_kks_block_3_background_color', 'field_5c888500f295c');
INSERT INTO `wp_postmeta` VALUES (1738, 178, 'kks_block_3_align_content', 'right');
INSERT INTO `wp_postmeta` VALUES (1739, 178, '_kks_block_3_align_content', 'field_5c88860c16b9d');
INSERT INTO `wp_postmeta` VALUES (1740, 178, 'kks_block_3_title', 'Kiểm soát tài chính cho khách sạn');
INSERT INTO `wp_postmeta` VALUES (1741, 178, '_kks_block_3_title', 'field_5c88854af295d');
INSERT INTO `wp_postmeta` VALUES (1742, 178, 'kks_block_3_link_xem_them', '');
INSERT INTO `wp_postmeta` VALUES (1743, 178, '_kks_block_3_link_xem_them', 'field_5c888589f295f');
INSERT INTO `wp_postmeta` VALUES (1744, 178, 'kks_block_3_desc', 'Các thẻ từ đều qua hệ thống phần mềm mới có thể được sử dụng, giúp giảm thiểu thất thoát và quản lý công suất phòng hiệu quả.');
INSERT INTO `wp_postmeta` VALUES (1745, 178, '_kks_block_3_desc', 'field_5c88856df295e');
INSERT INTO `wp_postmeta` VALUES (1746, 179, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1747, 179, '_edit_lock', '1552462957:1');
INSERT INTO `wp_postmeta` VALUES (1748, 181, '_wc_review_count', '0');
INSERT INTO `wp_postmeta` VALUES (1749, 181, '_wc_rating_count', 'a:0:{}');
INSERT INTO `wp_postmeta` VALUES (1750, 181, '_wc_average_rating', '0');
INSERT INTO `wp_postmeta` VALUES (1751, 182, '_wp_attached_file', '2019/03/logo-samsung.png');
INSERT INTO `wp_postmeta` VALUES (1752, 182, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:80;s:4:\"file\";s:24:\"2019/03/logo-samsung.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"logo-samsung-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"logo-samsung-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1753, 183, '_wp_attached_file', '2019/03/logo-lg.png');
INSERT INTO `wp_postmeta` VALUES (1754, 183, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:80;s:4:\"file\";s:19:\"2019/03/logo-lg.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"logo-lg-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"logo-lg-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1755, 184, '_wp_attached_file', '2019/03/logo-adel.png');
INSERT INTO `wp_postmeta` VALUES (1756, 184, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:80;s:4:\"file\";s:21:\"2019/03/logo-adel.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-adel-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-adel-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1757, 185, '_wp_attached_file', '2019/03/log-ibc.png');
INSERT INTO `wp_postmeta` VALUES (1758, 185, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:80;s:4:\"file\";s:19:\"2019/03/log-ibc.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"log-ibc-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"log-ibc-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1759, 186, '_wp_attached_file', '2019/03/logo-dessman.png');
INSERT INTO `wp_postmeta` VALUES (1760, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:80;s:4:\"file\";s:24:\"2019/03/logo-dessman.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"logo-dessman-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"logo-dessman-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1761, 187, '_wp_attached_file', '2019/03/logo-nuki.png');
INSERT INTO `wp_postmeta` VALUES (1762, 187, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:80;s:4:\"file\";s:21:\"2019/03/logo-nuki.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-nuki-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-nuki-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1763, 188, '_wp_attached_file', '2019/03/logo-yale.png');
INSERT INTO `wp_postmeta` VALUES (1764, 188, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:80;s:4:\"file\";s:21:\"2019/03/logo-yale.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-yale-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-yale-100x80.png\";s:5:\"width\";i:100;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (1765, 189, '_menu_item_type', 'post_type');
INSERT INTO `wp_postmeta` VALUES (1766, 189, '_menu_item_menu_item_parent', '143');
INSERT INTO `wp_postmeta` VALUES (1767, 189, '_menu_item_object_id', '150');
INSERT INTO `wp_postmeta` VALUES (1768, 189, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` VALUES (1769, 189, '_menu_item_target', '');
INSERT INTO `wp_postmeta` VALUES (1770, 189, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (1771, 189, '_menu_item_xfn', '');
INSERT INTO `wp_postmeta` VALUES (1772, 189, '_menu_item_url', '');
INSERT INTO `wp_postmeta` VALUES (1774, 190, '_edit_lock', '1552501954:1');
INSERT INTO `wp_postmeta` VALUES (1775, 190, '_edit_last', '1');
INSERT INTO `wp_postmeta` VALUES (1776, 190, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1777, 190, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1778, 190, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1779, 190, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1780, 190, 'image_desc', '105');
INSERT INTO `wp_postmeta` VALUES (1781, 190, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1782, 190, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1783, 190, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1784, 191, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1785, 191, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1786, 191, 'background', '');
INSERT INTO `wp_postmeta` VALUES (1787, 191, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1788, 191, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1789, 191, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1790, 191, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1791, 191, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1792, 192, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1793, 192, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1794, 192, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1795, 192, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1796, 192, 'image_desc', '105');
INSERT INTO `wp_postmeta` VALUES (1797, 192, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1798, 192, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1799, 192, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1800, 193, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1801, 193, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1802, 193, 'background', '78');
INSERT INTO `wp_postmeta` VALUES (1803, 193, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1804, 193, 'image_desc', '105');
INSERT INTO `wp_postmeta` VALUES (1805, 193, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1806, 193, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1807, 193, '_desc', 'field_5c7fe2fe5c2e3');
INSERT INTO `wp_postmeta` VALUES (1808, 194, 'whoweaare_image', '28');
INSERT INTO `wp_postmeta` VALUES (1809, 194, '_whoweaare_image', 'field_5c7b8532b00c5');
INSERT INTO `wp_postmeta` VALUES (1810, 194, 'whoweaare_title', 'Chúng tôi là ai?');
INSERT INTO `wp_postmeta` VALUES (1811, 194, '_whoweaare_title', 'field_5c7b858ab00c6');
INSERT INTO `wp_postmeta` VALUES (1812, 194, 'whoweaare_link', 'http://localhost:8080/onycom/chung-toi-la-ai/');
INSERT INTO `wp_postmeta` VALUES (1813, 194, '_whoweaare_link', 'field_5c7b8623b00c8');
INSERT INTO `wp_postmeta` VALUES (1814, 194, 'whoweaare_desc', '<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\r\n\r\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>');
INSERT INTO `wp_postmeta` VALUES (1815, 194, '_whoweaare_desc', 'field_5c7b8605b00c7');
INSERT INTO `wp_postmeta` VALUES (1816, 194, 'doanhnghiep_title', 'Giải pháp cho doanh nghiệp');
INSERT INTO `wp_postmeta` VALUES (1817, 194, '_doanhnghiep_title', 'field_5c7b898f8d9db');
INSERT INTO `wp_postmeta` VALUES (1818, 194, 'doanhnghiep_image', '37');
INSERT INTO `wp_postmeta` VALUES (1819, 194, '_doanhnghiep_image', 'field_5c7b89b48d9dc');
INSERT INTO `wp_postmeta` VALUES (1820, 194, 'doanhnghiep_link', '#');
INSERT INTO `wp_postmeta` VALUES (1821, 194, '_doanhnghiep_link', 'field_5c7b89ec8d9de');
INSERT INTO `wp_postmeta` VALUES (1822, 194, 'doanhnghiep_desc', '<p>Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về khách hàng, ONYCOM cung cấp toàn bộ dịch vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng &amp; dữ liệu lớn (Big Data)</p>');
INSERT INTO `wp_postmeta` VALUES (1823, 194, '_doanhnghiep_desc', 'field_5c7b89d08d9dd');
INSERT INTO `wp_postmeta` VALUES (1824, 194, 'giadinh_sub_title', 'Làm cho ngôi nhà của bạn thông minh');
INSERT INTO `wp_postmeta` VALUES (1825, 194, '_giadinh_sub_title', 'field_5c7b8ae55730c');
INSERT INTO `wp_postmeta` VALUES (1826, 194, 'giadinh_title', 'Giải pháp cho gia đình bạn');
INSERT INTO `wp_postmeta` VALUES (1827, 194, '_giadinh_title', 'field_5c7b8b025730d');
INSERT INTO `wp_postmeta` VALUES (1828, 194, 'giadinh_giaphap_0_title', 'Bảo vệ an toàn cho ngôi nhà của bạn');
INSERT INTO `wp_postmeta` VALUES (1829, 194, '_giadinh_giaphap_0_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1830, 194, 'giadinh_giaphap_0_tag', 'Bảo mật');
INSERT INTO `wp_postmeta` VALUES (1831, 194, '_giadinh_giaphap_0_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1832, 194, 'giadinh_giaphap_0_link', '#');
INSERT INTO `wp_postmeta` VALUES (1833, 194, '_giadinh_giaphap_0_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1834, 194, 'giadinh_giaphap_0_desc', 'Công nghệ bảo mật tiên tiến nhất đến từ các thương hiệu hàng đầu thế giới');
INSERT INTO `wp_postmeta` VALUES (1835, 194, '_giadinh_giaphap_0_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1836, 194, 'giadinh_giaphap', '3');
INSERT INTO `wp_postmeta` VALUES (1837, 194, '_giadinh_giaphap', 'field_5c7b8c835730e');
INSERT INTO `wp_postmeta` VALUES (1838, 194, 'giadinh_giaphap_0_background', '50');
INSERT INTO `wp_postmeta` VALUES (1839, 194, '_giadinh_giaphap_0_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1840, 194, 'giadinh_giaphap_1_title', '<br/>Giải pháp khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (1841, 194, '_giadinh_giaphap_1_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1842, 194, 'giadinh_giaphap_1_tag', 'Khóa khách sạn');
INSERT INTO `wp_postmeta` VALUES (1843, 194, '_giadinh_giaphap_1_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1844, 194, 'giadinh_giaphap_1_link', '#');
INSERT INTO `wp_postmeta` VALUES (1845, 194, '_giadinh_giaphap_1_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1846, 194, 'giadinh_giaphap_1_desc', 'Quản lý Khách sạn quy mô từ 2 sao với giải pháp quản lý 4.0, giúp tối ưu doanh thu và tiết kiệm chi phí');
INSERT INTO `wp_postmeta` VALUES (1847, 194, '_giadinh_giaphap_1_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1848, 194, 'giadinh_giaphap_1_background', '110');
INSERT INTO `wp_postmeta` VALUES (1849, 194, '_giadinh_giaphap_1_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1850, 194, 'giadinh_giaphap_2_title', '<br/>Smarthome');
INSERT INTO `wp_postmeta` VALUES (1851, 194, '_giadinh_giaphap_2_title', 'field_5c7b8c965730f');
INSERT INTO `wp_postmeta` VALUES (1852, 194, 'giadinh_giaphap_2_tag', 'Smarthome');
INSERT INTO `wp_postmeta` VALUES (1853, 194, '_giadinh_giaphap_2_tag', 'field_5c7b8cc357310');
INSERT INTO `wp_postmeta` VALUES (1854, 194, 'giadinh_giaphap_2_link', '#');
INSERT INTO `wp_postmeta` VALUES (1855, 194, '_giadinh_giaphap_2_link', 'field_5c7b8cd357312');
INSERT INTO `wp_postmeta` VALUES (1856, 194, 'giadinh_giaphap_2_desc', 'Kết nối các thiết bị thông minh với nhau, giúp nâng cao tiện nghi cho ngôi nhà của bạn, tự động hoá nhiệm vụ của các thiết bị trong nhà.');
INSERT INTO `wp_postmeta` VALUES (1857, 194, '_giadinh_giaphap_2_desc', 'field_5c7b8cca57311');
INSERT INTO `wp_postmeta` VALUES (1858, 194, 'giadinh_giaphap_2_background', '112');
INSERT INTO `wp_postmeta` VALUES (1859, 194, '_giadinh_giaphap_2_background', 'field_5c7b8e75f5531');
INSERT INTO `wp_postmeta` VALUES (1860, 194, 'thumb', '');
INSERT INTO `wp_postmeta` VALUES (1861, 194, '_thumb', 'field_5c7fe2cc5c2e1');
INSERT INTO `wp_postmeta` VALUES (1862, 194, 'background', '');
INSERT INTO `wp_postmeta` VALUES (1863, 194, '_background', 'field_5c7fe2e25c2e2');
INSERT INTO `wp_postmeta` VALUES (1864, 194, 'image_desc', '');
INSERT INTO `wp_postmeta` VALUES (1865, 194, '_image_desc', 'field_5c7fe3105c2e4');
INSERT INTO `wp_postmeta` VALUES (1866, 194, 'desc', '');
INSERT INTO `wp_postmeta` VALUES (1867, 194, '_desc', 'field_5c7fe2fe5c2e3');

-- ----------------------------
-- Table structure for wp_posts
-- ----------------------------
DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE `wp_posts`  (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime(0) NOT NULL,
  `post_date_gmt` datetime(0) NOT NULL,
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime(0) NOT NULL,
  `post_modified_gmt` datetime(0) NOT NULL,
  `post_content_filtered` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `post_name`(`post_name`(191)) USING BTREE,
  INDEX `type_status_date`(`post_type`, `post_status`, `post_date`, `ID`) USING BTREE,
  INDEX `post_parent`(`post_parent`) USING BTREE,
  INDEX `post_author`(`post_author`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 195 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_posts
-- ----------------------------
INSERT INTO `wp_posts` VALUES (1, 1, '2019-03-02 15:53:17', '2019-03-02 15:53:17', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2019-03-03 07:34:54', '2019-03-03 07:34:54', '', 0, 'http://localhost:8080/onycom/?p=1', 0, 'post', '', 1);
INSERT INTO `wp_posts` VALUES (2, 1, '2019-03-02 15:53:17', '2019-03-02 15:53:17', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8080/onycom/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-03-02 15:53:17', '2019-03-02 15:53:17', '', 0, 'http://localhost:8080/onycom/?page_id=2', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (3, 1, '2019-03-02 15:53:17', '2019-03-02 15:53:17', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost:8080/onycom.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-03-02 15:53:17', '2019-03-02 15:53:17', '', 0, 'http://localhost:8080/onycom/?page_id=3', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (5, 1, '2019-03-03 07:15:23', '2019-03-03 07:15:23', '', 'ICT Service', '', 'publish', 'closed', 'closed', '', 'giai-phap-cho-doanh-nghiep', '', '', '2019-03-12 17:31:23', '2019-03-12 17:31:23', '', 0, 'http://localhost:8080/onycom/?page_id=5', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (6, 1, '2019-03-03 07:15:23', '2019-03-03 07:15:23', '', 'Giải pháp cho doanh nghiệp', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-03-03 07:15:23', '2019-03-03 07:15:23', '', 5, 'http://localhost:8080/onycom/2019/03/03/5-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (7, 1, '2019-03-03 07:15:36', '2019-03-03 07:15:36', '', 'Giải pháp smarthome', '', 'publish', 'closed', 'closed', '', 'giai-phap-smarthome', '', '', '2019-03-06 15:56:11', '2019-03-06 15:56:11', '', 0, 'http://localhost:8080/onycom/?page_id=7', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (8, 1, '2019-03-03 07:15:36', '2019-03-03 07:15:36', '', 'Giải pháp smarthome', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-03-03 07:15:36', '2019-03-03 07:15:36', '', 7, 'http://localhost:8080/onycom/2019/03/03/7-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (9, 1, '2019-03-03 07:15:47', '2019-03-03 07:15:47', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'publish', 'closed', 'closed', '', 'gioi-thieu', '', '', '2019-03-07 18:47:30', '2019-03-07 18:47:30', '', 0, 'http://localhost:8080/onycom/?page_id=9', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (10, 1, '2019-03-03 07:15:47', '2019-03-03 07:15:47', '', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-03 07:15:47', '2019-03-03 07:15:47', '', 9, 'http://localhost:8080/onycom/2019/03/03/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (11, 1, '2019-03-03 07:17:24', '2019-03-03 07:17:24', '', 'Tin tức', '', 'publish', 'closed', 'closed', '', 'tin-tuc', '', '', '2019-03-07 17:39:13', '2019-03-07 17:39:13', '', 0, 'http://localhost:8080/onycom/?page_id=11', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (12, 1, '2019-03-03 07:17:24', '2019-03-03 07:17:24', '', 'Tin tức', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-03-03 07:17:24', '2019-03-03 07:17:24', '', 11, 'http://localhost:8080/onycom/2019/03/03/11-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (13, 1, '2019-03-03 07:20:53', '2019-03-03 07:20:53', ' ', '', '', 'publish', 'closed', 'closed', '', '13', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=13', 14, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (14, 1, '2019-03-03 07:20:52', '2019-03-03 07:20:52', ' ', '', '', 'publish', 'closed', 'closed', '', '14', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=14', 13, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (15, 1, '2019-03-03 07:20:52', '2019-03-03 07:20:52', ' ', '', '', 'publish', 'closed', 'closed', '', '15', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=15', 11, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (16, 1, '2019-03-03 07:20:52', '2019-03-03 07:20:52', ' ', '', '', 'publish', 'closed', 'closed', '', '16', '', '', '2019-03-13 17:20:01', '2019-03-13 17:20:01', '', 0, 'http://localhost:8080/onycom/?p=16', 1, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (17, 1, '2019-03-03 07:32:13', '2019-03-03 07:32:13', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-03 07:32:13', '2019-03-03 07:32:13', '', 9, 'http://localhost:8080/onycom/2019/03/03/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (18, 1, '2019-03-03 07:34:48', '2019-03-03 07:34:48', '<!-- wp:paragraph -->\n<p><strong>Bước 1:&nbsp;</strong>Vào cài đặt &gt;&gt; Màn hình khoá và bảo mật.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-01.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p><strong>Bước 2:</strong>&nbsp;Cài đặt bảo mật khác.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn1.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-02.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p><strong>Bước 3:&nbsp;</strong>Tác nhân tin cậy.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><em>*Lưu ý: Bạn phải cài đặt mật khẩu cho điện thoại thì thiết bị mới hiển thị mục cài đặt này.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn2.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-03.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p><strong>Bước 4:</strong>&nbsp;Bật Smartlock.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn3.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-04.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Như vậy&nbsp;<a href=\"http://www.thegioididong.com/hoi-dap/khac-phuc-loi-khong-hien-cai-dat-smartlock-tren-sa-1033327\" target=\"_blank\" rel=\"noreferrer noopener\">Smartlock</a>&nbsp;sẽ hiển thị lại tại mục&nbsp;<em>Màn hình khoá và bảo mật</em>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn4.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-samsung-05.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>​</p>\n<!-- /wp:paragraph -->', 'Khắc phục lỗi không hiện cài dặt Smartlock trên Samsung', '', 'publish', 'open', 'open', '', 'khac-phuc-loi-khong-hien-cai-dat-smartlock-tren-samsung', '', '', '2019-03-12 17:04:58', '2019-03-12 17:04:58', '', 0, 'http://localhost:8080/onycom/?p=18', 0, 'post', '', 0);
INSERT INTO `wp_posts` VALUES (19, 1, '2019-03-03 07:34:48', '2019-03-03 07:34:48', '<!-- wp:paragraph -->\n<p><strong>Bước 1:&nbsp;</strong>Vào cài đặt &gt;&gt; Màn hình khoá và bảo mật.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-01.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p><strong>Bước 2:</strong>&nbsp;Cài đặt bảo mật khác.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn1.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-02.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p><strong>Bước 3:&nbsp;</strong>Tác nhân tin cậy.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><em>*Lưu ý: Bạn phải cài đặt mật khẩu cho điện thoại thì thiết bị mới hiển thị mục cài đặt này.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn2.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-03.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p><strong>Bước 4:</strong>&nbsp;Bật Smartlock.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn3.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-smassung-04.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Như vậy&nbsp;<a href=\"http://www.thegioididong.com/hoi-dap/khac-phuc-loi-khong-hien-cai-dat-smartlock-tren-sa-1033327\" target=\"_blank\" rel=\"noreferrer noopener\">Smartlock</a>&nbsp;sẽ hiển thị lại tại mục&nbsp;<em>Màn hình khoá và bảo mật</em>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"https://cdn4.tgdd.vn/hoi-dap/1033327/khong-hien-smartlock-samsung-05.jpg\" alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>​</p>\n<!-- /wp:paragraph -->', 'Khắc phục lỗi không hiện cài dặt Smartlock trên Samsung', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2019-03-03 07:34:48', '2019-03-03 07:34:48', '', 18, 'http://localhost:8080/onycom/2019/03/03/18-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (20, 1, '2019-03-03 07:34:54', '2019-03-03 07:34:54', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-03-03 07:34:54', '2019-03-03 07:34:54', '', 1, 'http://localhost:8080/onycom/2019/03/03/1-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (21, 1, '2019-03-03 07:46:16', '2019-03-03 07:46:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:21:\"template-homepage.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Trang chủ - Block chúng tôi là ai', 'trang-chu-block-chung-toi-la-ai', 'publish', 'closed', 'closed', '', 'group_5c7b852b6bdd3', '', '', '2019-03-03 07:55:24', '2019-03-03 07:55:24', '', 0, 'http://localhost:8080/onycom/?post_type=acf-field-group&#038;p=21', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (22, 1, '2019-03-03 07:46:16', '2019-03-03 07:46:16', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Hình ảnh', 'whoweaare_image', 'publish', 'closed', 'closed', '', 'field_5c7b8532b00c5', '', '', '2019-03-03 07:46:16', '2019-03-03 07:46:16', '', 21, 'http://localhost:8080/onycom/?post_type=acf-field&p=22', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (23, 1, '2019-03-03 07:46:16', '2019-03-03 07:46:16', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'whoweaare_title', 'publish', 'closed', 'closed', '', 'field_5c7b858ab00c6', '', '', '2019-03-03 07:46:16', '2019-03-03 07:46:16', '', 21, 'http://localhost:8080/onycom/?post_type=acf-field&p=23', 1, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (24, 1, '2019-03-03 07:46:16', '2019-03-03 07:46:16', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"34\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Link', 'whoweaare_link', 'publish', 'closed', 'closed', '', 'field_5c7b8623b00c8', '', '', '2019-03-03 07:46:16', '2019-03-03 07:46:16', '', 21, 'http://localhost:8080/onycom/?post_type=acf-field&p=24', 2, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (25, 1, '2019-03-03 07:46:16', '2019-03-03 07:46:16', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Mô tả', 'whoweaare_desc', 'publish', 'closed', 'closed', '', 'field_5c7b8605b00c7', '', '', '2019-03-03 07:46:16', '2019-03-03 07:46:16', '', 21, 'http://localhost:8080/onycom/?post_type=acf-field&p=25', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (26, 1, '2019-03-03 07:55:01', '2019-03-03 07:55:01', '', 'Trang chủ', '', 'publish', 'closed', 'closed', '', 'trang-chu', '', '', '2019-03-13 18:33:03', '2019-03-13 18:33:03', '', 0, 'http://localhost:8080/onycom/?page_id=26', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (27, 1, '2019-03-03 07:55:01', '2019-03-03 07:55:01', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 07:55:01', '2019-03-03 07:55:01', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (28, 1, '2019-03-03 07:57:17', '2019-03-03 07:57:17', '', 'chungtoilaai-img', '', 'inherit', 'open', 'closed', '', 'chungtoilaai-img', '', '', '2019-03-03 07:57:17', '2019-03-03 07:57:17', '', 26, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/chungtoilaai-img.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (29, 1, '2019-03-03 07:57:39', '2019-03-03 07:57:39', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 07:57:39', '2019-03-03 07:57:39', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (30, 1, '2019-03-03 07:58:29', '2019-03-03 07:58:29', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 07:58:29', '2019-03-03 07:58:29', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (31, 1, '2019-03-03 07:59:47', '2019-03-03 07:59:47', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 07:59:47', '2019-03-03 07:59:47', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (32, 1, '2019-03-03 08:02:05', '2019-03-03 08:02:05', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:21:\"template-homepage.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Trang chủ - Block Giải pháp cho doanh nghiệp', 'trang-chu-block-giai-phap-cho-doanh-nghiep', 'publish', 'closed', 'closed', '', 'group_5c7b897e5b7ed', '', '', '2019-03-03 08:02:05', '2019-03-03 08:02:05', '', 0, 'http://localhost:8080/onycom/?post_type=acf-field-group&#038;p=32', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (33, 1, '2019-03-03 08:02:05', '2019-03-03 08:02:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'doanhnghiep_title', 'publish', 'closed', 'closed', '', 'field_5c7b898f8d9db', '', '', '2019-03-03 08:02:05', '2019-03-03 08:02:05', '', 32, 'http://localhost:8080/onycom/?post_type=acf-field&p=33', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (34, 1, '2019-03-03 08:02:05', '2019-03-03 08:02:05', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Hình ảnh', 'doanhnghiep_image', 'publish', 'closed', 'closed', '', 'field_5c7b89b48d9dc', '', '', '2019-03-03 08:02:05', '2019-03-03 08:02:05', '', 32, 'http://localhost:8080/onycom/?post_type=acf-field&p=34', 1, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (35, 1, '2019-03-03 08:02:05', '2019-03-03 08:02:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"34\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Link', 'doanhnghiep_link', 'publish', 'closed', 'closed', '', 'field_5c7b89ec8d9de', '', '', '2019-03-03 08:02:05', '2019-03-03 08:02:05', '', 32, 'http://localhost:8080/onycom/?post_type=acf-field&p=35', 2, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (36, 1, '2019-03-03 08:02:05', '2019-03-03 08:02:05', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:6:\"visual\";s:7:\"toolbar\";s:5:\"basic\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Mô tả', 'doanhnghiep_desc', 'publish', 'closed', 'closed', '', 'field_5c7b89d08d9dd', '', '', '2019-03-03 08:02:05', '2019-03-03 08:02:05', '', 32, 'http://localhost:8080/onycom/?post_type=acf-field&p=36', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (37, 1, '2019-03-03 08:03:01', '2019-03-03 08:03:01', '', 'gpdn-home', '', 'inherit', 'open', 'closed', '', 'gpdn-home', '', '', '2019-03-03 08:03:01', '2019-03-03 08:03:01', '', 26, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/gpdn-home.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` VALUES (38, 1, '2019-03-03 08:03:09', '2019-03-03 08:03:09', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:03:09', '2019-03-03 08:03:09', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (39, 1, '2019-03-03 08:14:43', '2019-03-03 08:14:43', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:21:\"template-homepage.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Trang chủ - Block giải pháp gia đình', 'trang-chu-block-giai-phap-gia-dinh', 'publish', 'closed', 'closed', '', 'group_5c7b8acc4df3a', '', '', '2019-03-07 17:16:51', '2019-03-07 17:16:51', '', 0, 'http://localhost:8080/onycom/?post_type=acf-field-group&#038;p=39', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (40, 1, '2019-03-03 08:14:43', '2019-03-03 08:14:43', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề phụ', 'giadinh_sub_title', 'publish', 'closed', 'closed', '', 'field_5c7b8ae55730c', '', '', '2019-03-03 08:14:43', '2019-03-03 08:14:43', '', 39, 'http://localhost:8080/onycom/?post_type=acf-field&p=40', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (41, 1, '2019-03-03 08:14:43', '2019-03-03 08:14:43', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'giadinh_title', 'publish', 'closed', 'closed', '', 'field_5c7b8b025730d', '', '', '2019-03-03 08:14:43', '2019-03-03 08:14:43', '', 39, 'http://localhost:8080/onycom/?post_type=acf-field&p=41', 1, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (42, 1, '2019-03-03 08:14:44', '2019-03-03 08:14:44', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Giải pháp', 'giadinh_giaphap', 'publish', 'closed', 'closed', '', 'field_5c7b8c835730e', '', '', '2019-03-03 08:19:29', '2019-03-03 08:19:29', '', 39, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=42', 2, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (43, 1, '2019-03-03 08:14:44', '2019-03-03 08:14:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"34\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_5c7b8c965730f', '', '', '2019-03-03 08:20:25', '2019-03-03 08:20:25', '', 42, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=43', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (44, 1, '2019-03-03 08:14:44', '2019-03-03 08:14:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tag', 'tag', 'publish', 'closed', 'closed', '', 'field_5c7b8cc357310', '', '', '2019-03-03 08:20:25', '2019-03-03 08:20:25', '', 42, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=44', 1, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (45, 1, '2019-03-03 08:14:44', '2019-03-03 08:14:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"67\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Mô tả', 'desc', 'publish', 'closed', 'closed', '', 'field_5c7b8cca57311', '', '', '2019-03-03 08:27:43', '2019-03-03 08:27:43', '', 42, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=45', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (46, 1, '2019-03-03 08:14:44', '2019-03-03 08:14:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_5c7b8cd357312', '', '', '2019-03-03 08:20:26', '2019-03-03 08:20:26', '', 42, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=46', 2, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (47, 1, '2019-03-03 08:21:30', '2019-03-03 08:21:30', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Background', 'background', 'publish', 'closed', 'closed', '', 'field_5c7b8e75f5531', '', '', '2019-03-07 17:16:51', '2019-03-07 17:16:51', '', 42, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=47', 4, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (48, 1, '2019-03-03 08:21:33', '2019-03-03 08:21:33', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:21:33', '2019-03-03 08:21:33', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (49, 1, '2019-03-03 08:24:35', '2019-03-03 08:24:35', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:24:35', '2019-03-03 08:24:35', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (50, 1, '2019-03-03 08:28:13', '2019-03-03 08:28:13', '', 'image-2', '', 'inherit', 'open', 'closed', '', 'image-2', '', '', '2019-03-03 08:28:13', '2019-03-03 08:28:13', '', 26, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/image-2.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (51, 1, '2019-03-03 08:28:18', '2019-03-03 08:28:18', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:28:18', '2019-03-03 08:28:18', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (52, 1, '2019-03-03 08:34:37', '2019-03-03 08:34:37', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:34:37', '2019-03-03 08:34:37', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (53, 1, '2019-03-03 08:35:49', '2019-03-03 08:35:49', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:35:49', '2019-03-03 08:35:49', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (54, 1, '2019-03-03 08:37:12', '2019-03-03 08:37:12', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:37:12', '2019-03-03 08:37:12', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (55, 1, '2019-03-03 08:40:05', '2019-03-03 08:40:05', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:40:05', '2019-03-03 08:40:05', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (56, 1, '2019-03-03 08:42:08', '2019-03-03 08:42:08', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-03 08:42:08', '2019-03-03 08:42:08', '', 26, 'http://localhost:8080/onycom/2019/03/03/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (57, 1, '2019-03-03 08:46:38', '2019-03-03 08:46:38', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2019-03-07 18:54:28', '2019-03-07 18:54:28', '', 0, 'http://localhost:8080/onycom/shop/', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (58, 1, '2019-03-03 08:46:38', '2019-03-03 08:46:38', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2019-03-03 08:46:38', '2019-03-03 08:46:38', '', 0, 'http://localhost:8080/onycom/cart/', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (59, 1, '2019-03-03 08:46:39', '2019-03-03 08:46:39', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2019-03-03 08:46:39', '2019-03-03 08:46:39', '', 0, 'http://localhost:8080/onycom/checkout/', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (60, 1, '2019-03-03 08:46:39', '2019-03-03 08:46:39', '[woocommerce_my_account]', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2019-03-03 08:46:39', '2019-03-03 08:46:39', '', 0, 'http://localhost:8080/onycom/my-account/', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (63, 1, '2019-03-03 08:50:08', '2019-03-03 08:50:08', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:15:\"acf_after_title\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Sản phẩm', 'san-pham', 'publish', 'closed', 'closed', '', 'group_5c7b950382a2d', '', '', '2019-03-03 08:50:33', '2019-03-03 08:50:33', '', 0, 'http://localhost:8080/onycom/?post_type=acf-field-group&#038;p=63', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (64, 1, '2019-03-03 08:50:08', '2019-03-03 08:50:08', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Mã sản phẩm', 'product_code', 'publish', 'closed', 'closed', '', 'field_5c7b950afd4a3', '', '', '2019-03-03 08:50:33', '2019-03-03 08:50:33', '', 63, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=64', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (66, 1, '2019-03-03 08:51:19', '2019-03-03 08:51:19', '', 'khóa thông minh samsung', '<div class=\"desc\">Mở khóa bằng vân tay, mã số, thẻ từ, chìa khóa và bằng Smartphone</div>', 'publish', 'open', 'closed', '', 'khoa-thong-minh-samsung', '', '', '2019-03-07 18:58:44', '2019-03-07 18:58:44', '', 0, 'http://localhost:8080/onycom/?post_type=product&#038;p=66', 0, 'product', '', 0);
INSERT INTO `wp_posts` VALUES (67, 1, '2019-03-03 08:51:37', '2019-03-03 08:51:37', '', 'smart-doorlock', '', 'inherit', 'open', 'closed', '', 'smart-doorlock', '', '', '2019-03-03 08:51:37', '2019-03-03 08:51:37', '', 66, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/smart-doorlock.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (68, 1, '2019-03-03 09:02:51', '2019-03-03 09:02:51', '', 'khóa thông minh samsung', '<div class=\"desc\">Mở khóa bằng vân tay, mã số, thẻ từ, chìa khóa và bằng Smartphone</div>', 'publish', 'open', 'closed', '', 'khoa-thong-minh-samsung-copy', '', '', '2019-03-07 18:59:17', '2019-03-07 18:59:17', '', 0, 'http://localhost:8080/onycom/?post_type=product&#038;p=68', 0, 'product', '', 0);
INSERT INTO `wp_posts` VALUES (69, 1, '2019-03-03 09:03:12', '2019-03-03 09:03:12', '', 'khóa thông minh samsung', '<div class=\"desc\">Mở khóa bằng vân tay, mã số, thẻ từ, chìa khóa và bằng Smartphone</div>', 'publish', 'open', 'closed', '', 'khoa-thong-minh-samsung-copy-2', '', '', '2019-03-13 07:48:02', '2019-03-13 07:48:02', '', 0, 'http://localhost:8080/onycom/?post_type=product&#038;p=69', 0, 'product', '', 0);
INSERT INTO `wp_posts` VALUES (71, 1, '2019-03-06 15:11:51', '2019-03-06 15:11:51', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Page - thông tin', 'page-thong-tin', 'publish', 'closed', 'closed', '', 'group_5c7fe2bc4cb88', '', '', '2019-03-06 15:15:46', '2019-03-06 15:15:46', '', 0, 'http://localhost:8080/onycom/?post_type=acf-field-group&#038;p=71', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (72, 1, '2019-03-06 15:11:51', '2019-03-06 15:11:51', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'hình ảnh đại diện', 'thumb', 'publish', 'closed', 'closed', '', 'field_5c7fe2cc5c2e1', '', '', '2019-03-06 15:15:46', '2019-03-06 15:15:46', '', 71, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=72', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (73, 1, '2019-03-06 15:11:51', '2019-03-06 15:11:51', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Hình nền tiêu đề', 'background', 'publish', 'closed', 'closed', '', 'field_5c7fe2e25c2e2', '', '', '2019-03-06 15:15:46', '2019-03-06 15:15:46', '', 71, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=73', 1, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (74, 1, '2019-03-06 15:11:51', '2019-03-06 15:11:51', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'hình ảnh mô tả', 'image_desc', 'publish', 'closed', 'closed', '', 'field_5c7fe3105c2e4', '', '', '2019-03-06 15:15:46', '2019-03-06 15:15:46', '', 71, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=74', 2, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (75, 1, '2019-03-06 15:11:51', '2019-03-06 15:11:51', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Mô tả ngắn', 'desc', 'publish', 'closed', 'closed', '', 'field_5c7fe2fe5c2e3', '', '', '2019-03-06 15:13:16', '2019-03-06 15:13:16', '', 71, 'http://localhost:8080/onycom/?post_type=acf-field&#038;p=75', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (76, 1, '2019-03-06 15:13:51', '2019-03-06 15:13:51', '', 'Giải pháp cho doanh nghiệp', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-03-06 15:13:51', '2019-03-06 15:13:51', '', 5, 'http://localhost:8080/onycom/2019/03/06/5-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (77, 1, '2019-03-06 15:14:52', '2019-03-06 15:14:52', '', 'Giải pháp cho doanh nghiệp', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-03-06 15:14:52', '2019-03-06 15:14:52', '', 5, 'http://localhost:8080/onycom/2019/03/06/5-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (78, 1, '2019-03-06 15:17:17', '2019-03-06 15:17:17', '', 'slider', '', 'inherit', 'open', 'closed', '', 'slider', '', '', '2019-03-06 15:17:17', '2019-03-06 15:17:17', '', 5, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/slider.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (79, 1, '2019-03-06 15:17:22', '2019-03-06 15:17:22', '', 'Giải pháp cho doanh nghiệp', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-03-06 15:17:22', '2019-03-06 15:17:22', '', 5, 'http://localhost:8080/onycom/2019/03/06/5-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (80, 1, '2019-03-06 15:24:39', '2019-03-06 15:24:39', '', 'Xây dựng hệ thống', '', 'publish', 'closed', 'closed', '', 'xay-dung-he-thong', '', '', '2019-03-07 17:24:27', '2019-03-07 17:24:27', '', 5, 'http://localhost:8080/onycom/?page_id=80', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (81, 1, '2019-03-06 15:24:39', '2019-03-06 15:24:39', '', 'Xây dựng hệ thống', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2019-03-06 15:24:39', '2019-03-06 15:24:39', '', 80, 'http://localhost:8080/onycom/2019/03/06/80-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (82, 1, '2019-03-06 15:26:27', '2019-03-06 15:26:27', '', 'gpdn-1', '', 'inherit', 'open', 'closed', '', 'gpdn-1', '', '', '2019-03-06 15:26:27', '2019-03-06 15:26:27', '', 80, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/gpdn-1.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (83, 1, '2019-03-06 15:26:28', '2019-03-06 15:26:28', '', 'gpdn-hethong', '', 'inherit', 'open', 'closed', '', 'gpdn-hethong', '', '', '2019-03-06 15:26:28', '2019-03-06 15:26:28', '', 80, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/gpdn-hethong.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` VALUES (84, 1, '2019-03-06 15:26:28', '2019-03-06 15:26:28', '', 'gpdn-hieusuat', '', 'inherit', 'open', 'closed', '', 'gpdn-hieusuat', '', '', '2019-03-06 15:26:28', '2019-03-06 15:26:28', '', 80, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/gpdn-hieusuat.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` VALUES (85, 1, '2019-03-06 15:26:29', '2019-03-06 15:26:29', '', 'qgpd-chatluong', '', 'inherit', 'open', 'closed', '', 'qgpd-chatluong', '', '', '2019-03-06 15:26:29', '2019-03-06 15:26:29', '', 80, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/qgpd-chatluong.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` VALUES (86, 1, '2019-03-06 15:26:57', '2019-03-06 15:26:57', '', 'Xây dựng hệ thống', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2019-03-06 15:26:57', '2019-03-06 15:26:57', '', 80, 'http://localhost:8080/onycom/2019/03/06/80-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (87, 1, '2019-03-06 15:27:37', '2019-03-06 15:27:37', '', 'Dữ liệu lớn', '', 'publish', 'closed', 'closed', '', 'du-lieu-lon', '', '', '2019-03-06 15:27:38', '2019-03-06 15:27:38', '', 5, 'http://localhost:8080/onycom/?page_id=87', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (88, 1, '2019-03-06 15:27:37', '2019-03-06 15:27:37', '', 'Dữ liệu lớn', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2019-03-06 15:27:37', '2019-03-06 15:27:37', '', 87, 'http://localhost:8080/onycom/2019/03/06/87-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (89, 1, '2019-03-06 15:27:38', '2019-03-06 15:27:38', '', 'Dữ liệu lớn', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2019-03-06 15:27:38', '2019-03-06 15:27:38', '', 87, 'http://localhost:8080/onycom/2019/03/06/87-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (90, 1, '2019-03-06 15:28:12', '2019-03-06 15:28:12', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Đảm bảo chất lượng', '', 'publish', 'closed', 'closed', '', 'dam-bao-chat-luong', '', '', '2019-03-06 15:28:26', '2019-03-06 15:28:26', '', 5, 'http://localhost:8080/onycom/?page_id=90', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (91, 1, '2019-03-06 15:28:12', '2019-03-06 15:28:12', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Đảm bảo chất lượng', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2019-03-06 15:28:12', '2019-03-06 15:28:12', '', 90, 'http://localhost:8080/onycom/2019/03/06/90-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (92, 1, '2019-03-06 15:28:26', '2019-03-06 15:28:26', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Đảm bảo chất lượng', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2019-03-06 15:28:26', '2019-03-06 15:28:26', '', 90, 'http://localhost:8080/onycom/2019/03/06/90-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (93, 1, '2019-03-06 15:28:49', '2019-03-06 15:28:49', '', 'Giám sát hiệu suất di động', '', 'publish', 'closed', 'closed', '', 'giam-sat-hieu-suat-di-dong', '', '', '2019-03-06 15:29:08', '2019-03-06 15:29:08', '', 5, 'http://localhost:8080/onycom/?page_id=93', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (94, 1, '2019-03-06 15:28:49', '2019-03-06 15:28:49', '', 'Giám sát hiệu suất di động', '', 'inherit', 'closed', 'closed', '', '93-revision-v1', '', '', '2019-03-06 15:28:49', '2019-03-06 15:28:49', '', 93, 'http://localhost:8080/onycom/2019/03/06/93-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (95, 1, '2019-03-06 15:29:02', '2019-03-06 15:29:02', '', 'Giám sát hiệu suất di động', '', 'inherit', 'closed', 'closed', '', '93-revision-v1', '', '', '2019-03-06 15:29:02', '2019-03-06 15:29:02', '', 93, 'http://localhost:8080/onycom/2019/03/06/93-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (96, 1, '2019-03-06 15:29:08', '2019-03-06 15:29:08', '', 'Giám sát hiệu suất di động', '', 'inherit', 'closed', 'closed', '', '93-revision-v1', '', '', '2019-03-06 15:29:08', '2019-03-06 15:29:08', '', 93, 'http://localhost:8080/onycom/2019/03/06/93-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (97, 1, '2019-03-06 15:53:44', '2019-03-06 15:53:44', '', 'Giải pháp smarthome', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-03-06 15:53:44', '2019-03-06 15:53:44', '', 7, 'http://localhost:8080/onycom/2019/03/06/7-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (98, 1, '2019-03-06 15:54:20', '2019-03-06 15:54:20', '', 'Giải pháp smarthome', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-03-06 15:54:20', '2019-03-06 15:54:20', '', 7, 'http://localhost:8080/onycom/2019/03/06/7-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (99, 1, '2019-03-06 15:56:08', '2019-03-06 15:56:08', '', 'smarthome-tit', '', 'inherit', 'open', 'closed', '', 'smarthome-tit', '', '', '2019-03-06 15:56:08', '2019-03-06 15:56:08', '', 7, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/smarthome-tit.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (100, 1, '2019-03-06 15:56:11', '2019-03-06 15:56:11', '', 'Giải pháp smarthome', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-03-06 15:56:11', '2019-03-06 15:56:11', '', 7, 'http://localhost:8080/onycom/2019/03/06/7-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (101, 1, '2019-03-06 16:58:58', '2019-03-06 16:58:58', '', 'Gallery sản phẩm', '', 'trash', 'open', 'closed', '', '__trashed', '', '', '2019-03-06 16:58:58', '2019-03-06 16:58:58', '', 0, 'http://localhost:8080/onycom/?post_type=product&#038;p=101', 0, 'product', '', 0);
INSERT INTO `wp_posts` VALUES (102, 1, '2019-03-06 16:58:47', '2019-03-06 16:58:47', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Gallery sản phẩm', 'gallery-san-pham', 'publish', 'closed', 'closed', '', 'group_5c7ffc10f2f42', '', '', '2019-03-06 16:58:47', '2019-03-06 16:58:47', '', 0, 'http://localhost:8080/onycom/?post_type=acf-field-group&#038;p=102', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (103, 1, '2019-03-06 16:58:47', '2019-03-06 16:58:47', 'a:16:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:10:\"uploadedTo\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Gallery hình ảnh', 'image_gallery', 'publish', 'closed', 'closed', '', 'field_5c7ffc18024c7', '', '', '2019-03-06 16:58:47', '2019-03-06 16:58:47', '', 102, 'http://localhost:8080/onycom/?post_type=acf-field&p=103', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (104, 1, '2019-03-06 16:59:17', '2019-03-06 16:59:17', '', 'chungtoilaai-img', '', 'inherit', 'open', 'closed', '', 'chungtoilaai-img-2', '', '', '2019-03-06 16:59:20', '2019-03-06 16:59:20', '', 69, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/chungtoilaai-img-1.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (105, 1, '2019-03-06 16:59:18', '2019-03-06 16:59:18', '', 'smart-doorlock', '', 'inherit', 'open', 'closed', '', 'smart-doorlock-2', '', '', '2019-03-06 16:59:18', '2019-03-06 16:59:18', '', 69, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/smart-doorlock-1.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (106, 1, '2019-03-06 17:53:36', '2019-03-06 17:53:36', ' ', '', '', 'publish', 'closed', 'closed', '', '106', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 5, 'http://localhost:8080/onycom/?p=106', 2, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (107, 1, '2019-03-06 17:53:37', '2019-03-06 17:53:37', ' ', '', '', 'publish', 'closed', 'closed', '', '107', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 5, 'http://localhost:8080/onycom/?p=107', 3, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (108, 1, '2019-03-06 17:53:37', '2019-03-06 17:53:37', ' ', '', '', 'publish', 'closed', 'closed', '', '108', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 5, 'http://localhost:8080/onycom/?p=108', 4, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (109, 1, '2019-03-06 17:53:37', '2019-03-06 17:53:37', ' ', '', '', 'publish', 'closed', 'closed', '', '109', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 5, 'http://localhost:8080/onycom/?p=109', 5, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (110, 1, '2019-03-07 17:14:54', '2019-03-07 17:14:54', '', 'kks_slide_image', '', 'inherit', 'open', 'closed', '', 'kks_slide_image', '', '', '2019-03-07 17:14:54', '2019-03-07 17:14:54', '', 26, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/kks_slide_image.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (111, 1, '2019-03-07 17:14:59', '2019-03-07 17:14:59', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-07 17:14:59', '2019-03-07 17:14:59', '', 26, 'http://localhost:8080/onycom/2019/03/07/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (112, 1, '2019-03-07 17:19:40', '2019-03-07 17:19:40', '', 'smarthome_slide', '', 'inherit', 'open', 'closed', '', 'smarthome_slide', '', '', '2019-03-07 17:19:40', '2019-03-07 17:19:40', '', 26, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/smarthome_slide.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (113, 1, '2019-03-07 17:20:13', '2019-03-07 17:20:13', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-07 17:20:13', '2019-03-07 17:20:13', '', 26, 'http://localhost:8080/onycom/2019/03/07/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (114, 1, '2019-03-07 17:21:30', '2019-03-07 17:21:30', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-07 17:21:30', '2019-03-07 17:21:30', '', 26, 'http://localhost:8080/onycom/2019/03/07/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (115, 1, '2019-03-07 17:21:37', '2019-03-07 17:21:37', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-07 17:21:37', '2019-03-07 17:21:37', '', 26, 'http://localhost:8080/onycom/2019/03/07/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (116, 1, '2019-03-07 17:21:46', '2019-03-07 17:21:46', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-07 17:21:46', '2019-03-07 17:21:46', '', 26, 'http://localhost:8080/onycom/2019/03/07/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (117, 1, '2019-03-07 17:24:07', '2019-03-07 17:24:07', '', 'Xây dựng hệ thống', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2019-03-07 17:24:07', '2019-03-07 17:24:07', '', 80, 'http://localhost:8080/onycom/2019/03/07/80-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (118, 1, '2019-03-07 17:24:12', '2019-03-07 17:24:12', '', 'Xây dựng hệ thống', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2019-03-07 17:24:12', '2019-03-07 17:24:12', '', 80, 'http://localhost:8080/onycom/2019/03/07/80-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (119, 1, '2019-03-07 17:24:27', '2019-03-07 17:24:27', '', 'Xây dựng hệ thống', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2019-03-07 17:24:27', '2019-03-07 17:24:27', '', 80, 'http://localhost:8080/onycom/2019/03/07/80-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (121, 1, '2019-03-07 17:39:09', '2019-03-07 17:39:09', '', 'tintuc_head_bg', '', 'inherit', 'open', 'closed', '', 'tintuc_head_bg', '', '', '2019-03-07 17:39:09', '2019-03-07 17:39:09', '', 11, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/tintuc_head_bg.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` VALUES (122, 1, '2019-03-07 17:39:13', '2019-03-07 17:39:13', '', 'Tin tức', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-03-07 17:39:13', '2019-03-07 17:39:13', '', 11, 'http://localhost:8080/onycom/2019/03/07/11-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (124, 1, '2019-03-07 18:04:18', '2019-03-07 18:04:18', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Tin tức', 'tin-tuc', 'publish', 'closed', 'closed', '', 'group_5c815d02e762f', '', '', '2019-03-07 18:22:23', '2019-03-07 18:22:23', '', 0, 'http://localhost:8080/onycom/?post_type=acf-field-group&#038;p=124', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (125, 1, '2019-03-07 18:04:18', '2019-03-07 18:04:18', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Mô tả ngắn', 'desc', 'publish', 'closed', 'closed', '', 'field_5c815d073ae7d', '', '', '2019-03-07 18:04:18', '2019-03-07 18:04:18', '', 124, 'http://localhost:8080/onycom/?post_type=acf-field&p=125', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (126, 1, '2019-03-07 18:05:59', '2019-03-07 18:05:59', '<!-- wp:paragraph -->\n<p>Khi nhắc đến các thương hiệu danh tiếng Hàn Quốc, chúng ta không thể bỏ qua nhà sản xuất khóa điện tử Epic. Trong xu thế của thời đại, những bộ khóa thông minh đã dần dần thay thế khóa cơ. Thực tiễn này diễn ra mạnh nhất ở các căn hộ cao cấp, khu nghỉ dưỡng, khách sạn và văn phòng. Tính đến thời điểm hiện tại, model Epic EF-8000L đang làm mưa làm gió trên thị trường, nhận được sự chú ý của rất nhiều người. Bài viết dưới đây của&nbsp;<strong><em>dhome.vn</em></strong>&nbsp;sẽ giúp các bạn hiểu rõ hơn về đặc điểm của thiết bị!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Thế mạnh làm nên uy tín của khóa điện tử Epic EF-8000L</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Thứ nhất, sản phẩm được tích hợp 3 chế độ đóng – mở cửa tiện dụng, đó là Vân tay - Mã số -Thẻ từ. Với cơ chế bảo mật 3 lớp, độ an toàn của sản phẩm được nâng cao. Không những thế, nó còn giúp người dùng chủ động hơn trong các tình huống. Giả sử ngón tay của bạn bị trầy xước, đầu đọc vân tay không nhận dạng được, bạn có thể chuyển sang sử dụng thẻ từ hoặc nhập mã số (mật khẩu). Đặc biệt, tính bảo mật còn được thể hiện qua công nghệ mã số ảo với hai hệ mã: Master (mã chủ) và User (mã thành viên). Và tất nhiên, thiết kế ổ khóa cơ thông minh đa điểm (dạng khế tròn) cũng có tác dụng chống phá khóa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nhắc đến khóa điện tử chính hãng Epic, người ta luôn nhấn mạnh đến công nghệ vân tay. Model EF-8000L của hãng ứng dụng công nghệ vân tay lưới quang học. Vậy tính chất của lưới quang học là gì? Nó có thể quét 360° và đọc mọi góc cạnh (ngang, dọc, chéo, ngược). Từ đó mang lại số liệu đầy đủ và toàn diện nhất về vân tay hợp lệ. &nbsp;Bên cạnh tốc độ đọc cực nhạy, nó có thể đọc cả vân tay bị mờ - đây là lợi ích cực kì thiết thực đối với những người mắc bệnh viêm da cơ địa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-3.jpg\" alt=\"khoa-dien-tu-epic-1913\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Không những thế, người dùng có thể kiểm soát cửa với chiếc điện thoại thông minh (smartphone). Cụ thể là công nghệ NFC – viết tắt của cụm từ tiếng Anh Near-Field Communications. Bạn có thể hiểu đây là công nghệ kết nối không dây trong phạm vi ngắn.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-1.jpg\" alt=\"khoa-dien-tu-epic-1914\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Trong những trường hợp như có kẻ phá khóa, đột nhập bất hợp pháp, chuông báo động của khóa điện tử Epic sẽ kêu lên. Gia chủ sẽ có biện pháp đối phó kịp thời, bảo vệ tối đa người và tài sản trong nhà.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Đôi nét giới thiệu về lịch sử công nghệ vân tay</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ở trên,&nbsp;<strong><em>dhome.vn</em></strong>&nbsp;đã giúp các bạn nắm được những ưu điểm hàng đầu của khóa điện tử vân tay Epic. Đây là một trong những minh chứng rõ nét về sự sáng tạo của con người, mang lại tính năng bảo vệ tinh vi hơn, ưu việt hơn. Mặc dù gương mặt, dáng đi, chữ ký của con người có thể thay đổi theo thời gian và bị làm giả nhưng với vân tay điều đó là không thể. Với nền tảng công nghệ thông tin phát triển mạnh mẽ, các thiết bị thông minh như khóa điện tử có thể thu nhận và lưu trữ được hàng triệu ghi chép dưới dạng số hóa. Nhờ đó, đặc điểm vân tay được ghi nhớ trong hệ thống cảm biến của khóa. Vậy từ khi nào con người bắt đầu sử dụng vân tay cho các hoạt động sống?</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Theo khảo cứu, vân tay xuất hiện trên những tấm thẻ bằng đất sét, phục vụ giao dịch buôn bán hàng hóa ở Babylon cổ đại. &nbsp;Tại Trung Quốc, các nhà khảo cổ tìm thấy dấu vân tay in trên những con dấu làm từ đất sét.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Tuy nhiên, phải đến thế kỷ XIX công nghệ vân tay mới xuất hiện những thành tựu đáng nói và được ứng dụng trong giai đoạn đầu thế kỷ XX. Năm 1924, Tổ chức FBI đã lưu trữ 250 tỉ vân tay của công dân làm dữ liệu truy lùng tội phạm cũng như tìm danh tính, quê quán của những người bị chết mà không biết rõ họ tên. Khi công nghệ “live-scan” ra đời, nó được biết đến với tác dụng lưu trữ hình ảnh vân tay không cần tới mực in.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ngoài mục đích pháp lý, công nghệ vân tay này nhanh chóng được ứng dụng cho mục đích kinh doanh, y học và không loại trừ việc điều khiển truy nhập và khóa cửa. Sự cải tiến của công nghệ vân tay gắn bó chặt chẽ với sự phát triển của mắt đọc vân tay dạng nén. Đây là mối quan hệ tương tác, thúc đẩy nhanh cùng đi lên.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-2.jpg\" alt=\"khoa-dien-tu-epic-1915\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Như vậy, các bạn có thể thấy, để sáng chế nên những sản phẩm ưu việt như khóa vân tay Epic, khóa cửa mã số Samsung giá rẻ… các nhà khoa học đã dày công nghiên cứu, nâng cấp tính năng sản phẩm lên tầm cao mới.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Khi đến với&nbsp;<strong><em>dhome.vn</em></strong>, các bạn sẽ được tiếp cận với những mẫu khóa cửa điện tử cao cấp. Chúng là đứa con tinh thần, là tâm huyết của nhiều nhà sản xuất lớn trên thế giới. Ngoài ra, các bạn có thể chọn mua phụ kiện khóa điện tử Samsung!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Khóa điện tử Epic EF-8000L và đẳng cấp vượt trội', '', 'publish', 'open', 'open', '', 'khoa-dien-tu-epic-ef-8000l-va-dang-cap-vuot-troi', '', '', '2019-03-07 18:22:56', '2019-03-07 18:22:56', '', 0, 'http://localhost:8080/onycom/?p=126', 0, 'post', '', 0);
INSERT INTO `wp_posts` VALUES (127, 1, '2019-03-07 18:05:55', '2019-03-07 18:05:55', '', '564_chuong_cua_co_hinh_samsung_sht_7017_cn610e', '', 'inherit', 'open', 'closed', '', '564_chuong_cua_co_hinh_samsung_sht_7017_cn610e', '', '', '2019-03-07 18:05:55', '2019-03-07 18:05:55', '', 126, 'http://localhost:8080/onycom/wp-content/uploads/2019/03/564_chuong_cua_co_hinh_samsung_sht_7017_cn610e.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` VALUES (128, 1, '2019-03-07 18:05:59', '2019-03-07 18:05:59', '<!-- wp:paragraph -->\n<p>Khi nhắc đến các thương hiệu danh tiếng Hàn Quốc, chúng ta không thể bỏ qua nhà sản xuất khóa điện tử Epic. Trong xu thế của thời đại, những bộ khóa thông minh đã dần dần thay thế khóa cơ. Thực tiễn này diễn ra mạnh nhất ở các căn hộ cao cấp, khu nghỉ dưỡng, khách sạn và văn phòng. Tính đến thời điểm hiện tại, model Epic EF-8000L đang làm mưa làm gió trên thị trường, nhận được sự chú ý của rất nhiều người. Bài viết dưới đây của <strong><em>dhome.vn</em></strong> sẽ giúp các bạn hiểu rõ hơn về đặc điểm của thiết bị!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Thế mạnh làm nên uy tín của khóa điện tử Epic EF-8000L</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Thứ nhất, sản phẩm được tích hợp 3 chế độ đóng – mở cửa tiện dụng, đó là Vân tay - Mã số -Thẻ từ. Với cơ chế bảo mật 3 lớp, độ an toàn của sản phẩm được nâng cao. Không những thế, nó còn giúp người dùng chủ động hơn trong các tình huống. Giả sử ngón tay của bạn bị trầy xước, đầu đọc vân tay không nhận dạng được, bạn có thể chuyển sang sử dụng thẻ từ hoặc nhập mã số (mật khẩu). Đặc biệt, tính bảo mật còn được thể hiện qua công nghệ mã số ảo với hai hệ mã: Master (mã chủ) và User (mã thành viên). Và tất nhiên, thiết kế ổ khóa cơ thông minh đa điểm (dạng khế tròn) cũng có tác dụng chống phá khóa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nhắc đến khóa điện tử chính hãng Epic, người ta luôn nhấn mạnh đến công nghệ vân tay. Model EF-8000L của hãng ứng dụng công nghệ vân tay lưới quang học. Vậy tính chất của lưới quang học là gì? Nó có thể quét 360° và đọc mọi góc cạnh (ngang, dọc, chéo, ngược). Từ đó mang lại số liệu đầy đủ và toàn diện nhất về vân tay hợp lệ. &nbsp;Bên cạnh tốc độ đọc cực nhạy, nó có thể đọc cả vân tay bị mờ - đây là lợi ích cực kì thiết thực đối với những người mắc bệnh viêm da cơ địa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-3.jpg\" alt=\"khoa-dien-tu-epic-1913\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Không những thế, người dùng có thể kiểm soát cửa với chiếc điện thoại thông minh (smartphone). Cụ thể là công nghệ NFC – viết tắt của cụm từ tiếng Anh Near-Field Communications. Bạn có thể hiểu đây là công nghệ kết nối không dây trong phạm vi ngắn.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-1.jpg\" alt=\"khoa-dien-tu-epic-1914\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Trong những trường hợp như có kẻ phá khóa, đột nhập bất hợp pháp, chuông báo động của khóa điện tử Epic sẽ kêu lên. Gia chủ sẽ có biện pháp đối phó kịp thời, bảo vệ tối đa người và tài sản trong nhà.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Đôi nét giới thiệu về lịch sử công nghệ vân tay</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ở trên,&nbsp;<strong><em>dhome.vn</em></strong>&nbsp;đã giúp các bạn nắm được những ưu điểm hàng đầu của khóa điện tử vân tay Epic. Đây là một trong những minh chứng rõ nét về sự sáng tạo của con người, mang lại tính năng bảo vệ tinh vi hơn, ưu việt hơn. Mặc dù gương mặt, dáng đi, chữ ký của con người có thể thay đổi theo thời gian và bị làm giả nhưng với vân tay điều đó là không thể. Với nền tảng công nghệ thông tin phát triển mạnh mẽ, các thiết bị thông minh như khóa điện tử có thể thu nhận và lưu trữ được hàng triệu ghi chép dưới dạng số hóa. Nhờ đó, đặc điểm vân tay được ghi nhớ trong hệ thống cảm biến của khóa. Vậy từ khi nào con người bắt đầu sử dụng vân tay cho các hoạt động sống?</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Theo khảo cứu, vân tay xuất hiện trên những tấm thẻ bằng đất sét, phục vụ giao dịch buôn bán hàng hóa ở Babylon cổ đại. &nbsp;Tại Trung Quốc, các nhà khảo cổ tìm thấy dấu vân tay in trên những con dấu làm từ đất sét.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Tuy nhiên, phải đến thế kỷ XIX công nghệ vân tay mới xuất hiện những thành tựu đáng nói và được ứng dụng trong giai đoạn đầu thế kỷ XX. Năm 1924, Tổ chức FBI đã lưu trữ 250 tỉ vân tay của công dân làm dữ liệu truy lùng tội phạm cũng như tìm danh tính, quê quán của những người bị chết mà không biết rõ họ tên. Khi công nghệ “live-scan” ra đời, nó được biết đến với tác dụng lưu trữ hình ảnh vân tay không cần tới mực in.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ngoài mục đích pháp lý, công nghệ vân tay này nhanh chóng được ứng dụng cho mục đích kinh doanh, y học và không loại trừ việc điều khiển truy nhập và khóa cửa. Sự cải tiến của công nghệ vân tay gắn bó chặt chẽ với sự phát triển của mắt đọc vân tay dạng nén. Đây là mối quan hệ tương tác, thúc đẩy nhanh cùng đi lên.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-2.jpg\" alt=\"khoa-dien-tu-epic-1915\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Như vậy, các bạn có thể thấy, để sáng chế nên những sản phẩm ưu việt như khóa vân tay Epic, khóa cửa mã số Samsung giá rẻ… các nhà khoa học đã dày công nghiên cứu, nâng cấp tính năng sản phẩm lên tầm cao mới.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Khi đến với&nbsp;<strong><em>dhome.vn</em></strong>, các bạn sẽ được tiếp cận với những mẫu khóa cửa điện tử cao cấp. Chúng là đứa con tinh thần, là tâm huyết của nhiều nhà sản xuất lớn trên thế giới. Ngoài ra, các bạn có thể chọn mua phụ kiện khóa điện tử Samsung!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Khóa điện tử Epic EF-8000L và đẳng cấp vượt trội', '', 'inherit', 'closed', 'closed', '', '126-revision-v1', '', '', '2019-03-07 18:05:59', '2019-03-07 18:05:59', '', 126, 'http://localhost:8080/onycom/126-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (129, 1, '2019-03-07 18:22:54', '2019-03-07 18:22:54', '<!-- wp:paragraph -->\n<p>Khi nhắc đến các thương hiệu danh tiếng Hàn Quốc, chúng ta không thể bỏ qua nhà sản xuất khóa điện tử Epic. Trong xu thế của thời đại, những bộ khóa thông minh đã dần dần thay thế khóa cơ. Thực tiễn này diễn ra mạnh nhất ở các căn hộ cao cấp, khu nghỉ dưỡng, khách sạn và văn phòng. Tính đến thời điểm hiện tại, model Epic EF-8000L đang làm mưa làm gió trên thị trường, nhận được sự chú ý của rất nhiều người. Bài viết dưới đây của&nbsp;<strong><em>dhome.vn</em></strong>&nbsp;sẽ giúp các bạn hiểu rõ hơn về đặc điểm của thiết bị!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Thế mạnh làm nên uy tín của khóa điện tử Epic EF-8000L</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Thứ nhất, sản phẩm được tích hợp 3 chế độ đóng – mở cửa tiện dụng, đó là Vân tay - Mã số -Thẻ từ. Với cơ chế bảo mật 3 lớp, độ an toàn của sản phẩm được nâng cao. Không những thế, nó còn giúp người dùng chủ động hơn trong các tình huống. Giả sử ngón tay của bạn bị trầy xước, đầu đọc vân tay không nhận dạng được, bạn có thể chuyển sang sử dụng thẻ từ hoặc nhập mã số (mật khẩu). Đặc biệt, tính bảo mật còn được thể hiện qua công nghệ mã số ảo với hai hệ mã: Master (mã chủ) và User (mã thành viên). Và tất nhiên, thiết kế ổ khóa cơ thông minh đa điểm (dạng khế tròn) cũng có tác dụng chống phá khóa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nhắc đến khóa điện tử chính hãng Epic, người ta luôn nhấn mạnh đến công nghệ vân tay. Model EF-8000L của hãng ứng dụng công nghệ vân tay lưới quang học. Vậy tính chất của lưới quang học là gì? Nó có thể quét 360° và đọc mọi góc cạnh (ngang, dọc, chéo, ngược). Từ đó mang lại số liệu đầy đủ và toàn diện nhất về vân tay hợp lệ. &nbsp;Bên cạnh tốc độ đọc cực nhạy, nó có thể đọc cả vân tay bị mờ - đây là lợi ích cực kì thiết thực đối với những người mắc bệnh viêm da cơ địa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-3.jpg\" alt=\"khoa-dien-tu-epic-1913\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Không những thế, người dùng có thể kiểm soát cửa với chiếc điện thoại thông minh (smartphone). Cụ thể là công nghệ NFC – viết tắt của cụm từ tiếng Anh Near-Field Communications. Bạn có thể hiểu đây là công nghệ kết nối không dây trong phạm vi ngắn.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-1.jpg\" alt=\"khoa-dien-tu-epic-1914\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Trong những trường hợp như có kẻ phá khóa, đột nhập bất hợp pháp, chuông báo động của khóa điện tử Epic sẽ kêu lên. Gia chủ sẽ có biện pháp đối phó kịp thời, bảo vệ tối đa người và tài sản trong nhà.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Đôi nét giới thiệu về lịch sử công nghệ vân tay</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ở trên,&nbsp;<strong><em>dhome.vn</em></strong>&nbsp;đã giúp các bạn nắm được những ưu điểm hàng đầu của khóa điện tử vân tay Epic. Đây là một trong những minh chứng rõ nét về sự sáng tạo của con người, mang lại tính năng bảo vệ tinh vi hơn, ưu việt hơn. Mặc dù gương mặt, dáng đi, chữ ký của con người có thể thay đổi theo thời gian và bị làm giả nhưng với vân tay điều đó là không thể. Với nền tảng công nghệ thông tin phát triển mạnh mẽ, các thiết bị thông minh như khóa điện tử có thể thu nhận và lưu trữ được hàng triệu ghi chép dưới dạng số hóa. Nhờ đó, đặc điểm vân tay được ghi nhớ trong hệ thống cảm biến của khóa. Vậy từ khi nào con người bắt đầu sử dụng vân tay cho các hoạt động sống?</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Theo khảo cứu, vân tay xuất hiện trên những tấm thẻ bằng đất sét, phục vụ giao dịch buôn bán hàng hóa ở Babylon cổ đại. &nbsp;Tại Trung Quốc, các nhà khảo cổ tìm thấy dấu vân tay in trên những con dấu làm từ đất sét.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Tuy nhiên, phải đến thế kỷ XIX công nghệ vân tay mới xuất hiện những thành tựu đáng nói và được ứng dụng trong giai đoạn đầu thế kỷ XX. Năm 1924, Tổ chức FBI đã lưu trữ 250 tỉ vân tay của công dân làm dữ liệu truy lùng tội phạm cũng như tìm danh tính, quê quán của những người bị chết mà không biết rõ họ tên. Khi công nghệ “live-scan” ra đời, nó được biết đến với tác dụng lưu trữ hình ảnh vân tay không cần tới mực in.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ngoài mục đích pháp lý, công nghệ vân tay này nhanh chóng được ứng dụng cho mục đích kinh doanh, y học và không loại trừ việc điều khiển truy nhập và khóa cửa. Sự cải tiến của công nghệ vân tay gắn bó chặt chẽ với sự phát triển của mắt đọc vân tay dạng nén. Đây là mối quan hệ tương tác, thúc đẩy nhanh cùng đi lên.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-2.jpg\" alt=\"khoa-dien-tu-epic-1915\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Như vậy, các bạn có thể thấy, để sáng chế nên những sản phẩm ưu việt như khóa vân tay Epic, khóa cửa mã số Samsung giá rẻ… các nhà khoa học đã dày công nghiên cứu, nâng cấp tính năng sản phẩm lên tầm cao mới.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Khi đến với&nbsp;<strong><em>dhome.vn</em></strong>, các bạn sẽ được tiếp cận với những mẫu khóa cửa điện tử cao cấp. Chúng là đứa con tinh thần, là tâm huyết của nhiều nhà sản xuất lớn trên thế giới. Ngoài ra, các bạn có thể chọn mua phụ kiện khóa điện tử Samsung!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Khóa điện tử Epic EF-8000L và đẳng cấp vượt trội', '', 'inherit', 'closed', 'closed', '', '126-revision-v1', '', '', '2019-03-07 18:22:54', '2019-03-07 18:22:54', '', 126, 'http://localhost:8080/onycom/126-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (130, 1, '2019-03-07 18:22:56', '2019-03-07 18:22:56', '<!-- wp:paragraph -->\n<p>Khi nhắc đến các thương hiệu danh tiếng Hàn Quốc, chúng ta không thể bỏ qua nhà sản xuất khóa điện tử Epic. Trong xu thế của thời đại, những bộ khóa thông minh đã dần dần thay thế khóa cơ. Thực tiễn này diễn ra mạnh nhất ở các căn hộ cao cấp, khu nghỉ dưỡng, khách sạn và văn phòng. Tính đến thời điểm hiện tại, model Epic EF-8000L đang làm mưa làm gió trên thị trường, nhận được sự chú ý của rất nhiều người. Bài viết dưới đây của&nbsp;<strong><em>dhome.vn</em></strong>&nbsp;sẽ giúp các bạn hiểu rõ hơn về đặc điểm của thiết bị!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Thế mạnh làm nên uy tín của khóa điện tử Epic EF-8000L</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Thứ nhất, sản phẩm được tích hợp 3 chế độ đóng – mở cửa tiện dụng, đó là Vân tay - Mã số -Thẻ từ. Với cơ chế bảo mật 3 lớp, độ an toàn của sản phẩm được nâng cao. Không những thế, nó còn giúp người dùng chủ động hơn trong các tình huống. Giả sử ngón tay của bạn bị trầy xước, đầu đọc vân tay không nhận dạng được, bạn có thể chuyển sang sử dụng thẻ từ hoặc nhập mã số (mật khẩu). Đặc biệt, tính bảo mật còn được thể hiện qua công nghệ mã số ảo với hai hệ mã: Master (mã chủ) và User (mã thành viên). Và tất nhiên, thiết kế ổ khóa cơ thông minh đa điểm (dạng khế tròn) cũng có tác dụng chống phá khóa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nhắc đến khóa điện tử chính hãng Epic, người ta luôn nhấn mạnh đến công nghệ vân tay. Model EF-8000L của hãng ứng dụng công nghệ vân tay lưới quang học. Vậy tính chất của lưới quang học là gì? Nó có thể quét 360° và đọc mọi góc cạnh (ngang, dọc, chéo, ngược). Từ đó mang lại số liệu đầy đủ và toàn diện nhất về vân tay hợp lệ. &nbsp;Bên cạnh tốc độ đọc cực nhạy, nó có thể đọc cả vân tay bị mờ - đây là lợi ích cực kì thiết thực đối với những người mắc bệnh viêm da cơ địa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-3.jpg\" alt=\"khoa-dien-tu-epic-1913\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Không những thế, người dùng có thể kiểm soát cửa với chiếc điện thoại thông minh (smartphone). Cụ thể là công nghệ NFC – viết tắt của cụm từ tiếng Anh Near-Field Communications. Bạn có thể hiểu đây là công nghệ kết nối không dây trong phạm vi ngắn.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-1.jpg\" alt=\"khoa-dien-tu-epic-1914\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Trong những trường hợp như có kẻ phá khóa, đột nhập bất hợp pháp, chuông báo động của khóa điện tử Epic sẽ kêu lên. Gia chủ sẽ có biện pháp đối phó kịp thời, bảo vệ tối đa người và tài sản trong nhà.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Đôi nét giới thiệu về lịch sử công nghệ vân tay</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ở trên,&nbsp;<strong><em>dhome.vn</em></strong>&nbsp;đã giúp các bạn nắm được những ưu điểm hàng đầu của khóa điện tử vân tay Epic. Đây là một trong những minh chứng rõ nét về sự sáng tạo của con người, mang lại tính năng bảo vệ tinh vi hơn, ưu việt hơn. Mặc dù gương mặt, dáng đi, chữ ký của con người có thể thay đổi theo thời gian và bị làm giả nhưng với vân tay điều đó là không thể. Với nền tảng công nghệ thông tin phát triển mạnh mẽ, các thiết bị thông minh như khóa điện tử có thể thu nhận và lưu trữ được hàng triệu ghi chép dưới dạng số hóa. Nhờ đó, đặc điểm vân tay được ghi nhớ trong hệ thống cảm biến của khóa. Vậy từ khi nào con người bắt đầu sử dụng vân tay cho các hoạt động sống?</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Theo khảo cứu, vân tay xuất hiện trên những tấm thẻ bằng đất sét, phục vụ giao dịch buôn bán hàng hóa ở Babylon cổ đại. &nbsp;Tại Trung Quốc, các nhà khảo cổ tìm thấy dấu vân tay in trên những con dấu làm từ đất sét.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Tuy nhiên, phải đến thế kỷ XIX công nghệ vân tay mới xuất hiện những thành tựu đáng nói và được ứng dụng trong giai đoạn đầu thế kỷ XX. Năm 1924, Tổ chức FBI đã lưu trữ 250 tỉ vân tay của công dân làm dữ liệu truy lùng tội phạm cũng như tìm danh tính, quê quán của những người bị chết mà không biết rõ họ tên. Khi công nghệ “live-scan” ra đời, nó được biết đến với tác dụng lưu trữ hình ảnh vân tay không cần tới mực in.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ngoài mục đích pháp lý, công nghệ vân tay này nhanh chóng được ứng dụng cho mục đích kinh doanh, y học và không loại trừ việc điều khiển truy nhập và khóa cửa. Sự cải tiến của công nghệ vân tay gắn bó chặt chẽ với sự phát triển của mắt đọc vân tay dạng nén. Đây là mối quan hệ tương tác, thúc đẩy nhanh cùng đi lên.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img src=\"http://dhome.vn/media/lib/989_khoa-cua-dien-tu-van-tay-EPIC-EF-8000L-2.jpg\" alt=\"khoa-dien-tu-epic-1915\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Như vậy, các bạn có thể thấy, để sáng chế nên những sản phẩm ưu việt như khóa vân tay Epic, khóa cửa mã số Samsung giá rẻ… các nhà khoa học đã dày công nghiên cứu, nâng cấp tính năng sản phẩm lên tầm cao mới.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Khi đến với&nbsp;<strong><em>dhome.vn</em></strong>, các bạn sẽ được tiếp cận với những mẫu khóa cửa điện tử cao cấp. Chúng là đứa con tinh thần, là tâm huyết của nhiều nhà sản xuất lớn trên thế giới. Ngoài ra, các bạn có thể chọn mua phụ kiện khóa điện tử Samsung!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Khóa điện tử Epic EF-8000L và đẳng cấp vượt trội', '', 'inherit', 'closed', 'closed', '', '126-revision-v1', '', '', '2019-03-07 18:22:56', '2019-03-07 18:22:56', '', 126, 'http://localhost:8080/onycom/126-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (131, 1, '2019-03-07 18:46:18', '2019-03-07 18:46:18', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-07 18:46:18', '2019-03-07 18:46:18', '', 9, 'http://localhost:8080/onycom/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (132, 1, '2019-03-07 18:46:32', '2019-03-07 18:46:32', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-07 18:46:32', '2019-03-07 18:46:32', '', 9, 'http://localhost:8080/onycom/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (133, 1, '2019-03-07 18:46:43', '2019-03-07 18:46:43', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-07 18:46:43', '2019-03-07 18:46:43', '', 9, 'http://localhost:8080/onycom/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (134, 1, '2019-03-07 18:46:52', '2019-03-07 18:46:52', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-07 18:46:52', '2019-03-07 18:46:52', '', 9, 'http://localhost:8080/onycom/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (135, 1, '2019-03-07 18:47:06', '2019-03-07 18:47:06', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-07 18:47:06', '2019-03-07 18:47:06', '', 9, 'http://localhost:8080/onycom/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (136, 1, '2019-03-07 18:47:30', '2019-03-07 18:47:30', '<!-- wp:paragraph -->\n<p>Thông tin đang được cập nhật ...</p>\n<!-- /wp:paragraph -->', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-07 18:47:30', '2019-03-07 18:47:30', '', 9, 'http://localhost:8080/onycom/9-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (137, 1, '2019-03-07 18:54:28', '2019-03-07 18:54:28', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2019-03-07 18:54:28', '2019-03-07 18:54:28', '', 57, 'http://localhost:8080/onycom/57-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (138, 1, '2019-03-07 18:54:28', '2019-03-07 18:54:28', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2019-03-07 18:54:28', '2019-03-07 18:54:28', '', 57, 'http://localhost:8080/onycom/57-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (139, 1, '2019-03-12 16:28:09', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-12 16:28:09', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/onycom/?p=139', 0, 'post', '', 0);
INSERT INTO `wp_posts` VALUES (140, 1, '2019-03-12 17:01:23', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-12 17:01:23', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/onycom/?p=140', 0, 'post', '', 0);
INSERT INTO `wp_posts` VALUES (141, 1, '2019-03-12 17:02:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-12 17:02:54', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/onycom/?p=141', 0, 'post', '', 0);
INSERT INTO `wp_posts` VALUES (142, 1, '2019-03-12 17:31:22', '2019-03-12 17:31:22', '', 'ICT Service', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-03-12 17:31:22', '2019-03-12 17:31:22', '', 5, 'http://localhost:8080/onycom/5-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (143, 1, '2019-03-12 17:35:16', '2019-03-12 17:35:16', '', 'Sản phẩm', '', 'publish', 'closed', 'closed', '', 'san-pham', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=143', 6, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (144, 1, '2019-03-12 17:35:17', '2019-03-12 17:35:17', ' ', '', '', 'publish', 'closed', 'closed', '', '144', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=144', 9, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (146, 1, '2019-03-12 17:35:16', '2019-03-12 17:35:16', ' ', '', '', 'publish', 'closed', 'closed', '', '146', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=146', 8, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (148, 1, '2019-03-12 17:35:18', '2019-03-12 17:35:18', ' ', '', '', 'publish', 'closed', 'closed', '', '148', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=148', 12, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (149, 1, '2019-03-12 17:48:00', '2019-03-12 17:48:00', ' ', '', '', 'publish', 'closed', 'closed', '', '149', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=149', 7, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (150, 1, '2019-03-13 03:19:14', '2019-03-13 03:19:14', '', 'Khóa khách sạn', '', 'publish', 'closed', 'closed', '', 'khoa-khach-san', '', '', '2019-03-13 06:30:43', '2019-03-13 06:30:43', '', 0, 'http://localhost/luonghx/onycom/?page_id=150', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (151, 1, '2019-03-13 03:19:14', '2019-03-13 03:19:14', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 03:19:14', '2019-03-13 03:19:14', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (152, 1, '2019-03-13 03:19:42', '2019-03-13 03:19:42', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 03:19:42', '2019-03-13 03:19:42', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (153, 1, '2019-03-13 03:20:33', '2019-03-13 03:20:33', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 03:20:33', '2019-03-13 03:20:33', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (154, 1, '2019-03-13 03:22:00', '2019-03-13 03:22:00', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 03:22:00', '2019-03-13 03:22:00', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (155, 1, '2019-03-13 04:18:07', '2019-03-13 04:18:07', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"150\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Page - Khóa khách sạn', 'page-khoa-khach-san', 'publish', 'closed', 'closed', '', 'group_5c8883759f9ea', '', '', '2019-03-13 04:25:36', '2019-03-13 04:25:36', '', 0, 'http://localhost/luonghx/onycom/?post_type=acf-field-group&#038;p=155', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (161, 1, '2019-03-13 04:23:00', '2019-03-13 04:23:00', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Block - Khóa khách sạn', 'kks_block', 'publish', 'closed', 'closed', '', 'field_5c8884a6f295a', '', '', '2019-03-13 04:23:00', '2019-03-13 04:23:00', '', 155, 'http://localhost/luonghx/onycom/?post_type=acf-field&p=161', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (162, 1, '2019-03-13 04:23:00', '2019-03-13 04:23:00', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Background', 'background', 'publish', 'closed', 'closed', '', 'field_5c8884cbf295b', '', '', '2019-03-13 04:23:00', '2019-03-13 04:23:00', '', 161, 'http://localhost/luonghx/onycom/?post_type=acf-field&p=162', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (163, 1, '2019-03-13 04:23:00', '2019-03-13 04:23:00', 'a:6:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";}', 'Màu nền', 'background_color', 'publish', 'closed', 'closed', '', 'field_5c888500f295c', '', '', '2019-03-13 04:23:00', '2019-03-13 04:23:00', '', 161, 'http://localhost/luonghx/onycom/?post_type=acf-field&p=163', 1, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (164, 1, '2019-03-13 04:23:00', '2019-03-13 04:23:00', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_5c88854af295d', '', '', '2019-03-13 04:25:36', '2019-03-13 04:25:36', '', 161, 'http://localhost/luonghx/onycom/?post_type=acf-field&#038;p=164', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (165, 1, '2019-03-13 04:23:00', '2019-03-13 04:23:00', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Mô tả', 'desc', 'publish', 'closed', 'closed', '', 'field_5c88856df295e', '', '', '2019-03-13 04:25:36', '2019-03-13 04:25:36', '', 161, 'http://localhost/luonghx/onycom/?post_type=acf-field&#038;p=165', 5, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (166, 1, '2019-03-13 04:23:01', '2019-03-13 04:23:01', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";}', 'Link xem thêm', 'link_xem_them', 'publish', 'closed', 'closed', '', 'field_5c888589f295f', '', '', '2019-03-13 04:25:36', '2019-03-13 04:25:36', '', 161, 'http://localhost/luonghx/onycom/?post_type=acf-field&#038;p=166', 4, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (167, 1, '2019-03-13 04:24:27', '2019-03-13 04:24:27', '', 'kks-trainghiemkh-bg', '', 'inherit', 'open', 'closed', '', 'kks-trainghiemkh-bg', '', '', '2019-03-13 04:24:27', '2019-03-13 04:24:27', '', 150, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/kks-trainghiemkh-bg.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (168, 1, '2019-03-13 04:24:41', '2019-03-13 04:24:41', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 04:24:41', '2019-03-13 04:24:41', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (169, 1, '2019-03-13 04:25:36', '2019-03-13 04:25:36', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:2:{s:4:\"left\";s:5:\"Trái\";s:5:\"right\";s:6:\"Phải\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Canh lề', 'align_content', 'publish', 'closed', 'closed', '', 'field_5c88860c16b9d', '', '', '2019-03-13 04:25:36', '2019-03-13 04:25:36', '', 161, 'http://localhost/luonghx/onycom/?post_type=acf-field&p=169', 2, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (170, 1, '2019-03-13 04:27:11', '2019-03-13 04:27:11', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 04:27:11', '2019-03-13 04:27:11', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (171, 1, '2019-03-13 04:33:47', '2019-03-13 04:33:47', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 04:33:47', '2019-03-13 04:33:47', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (172, 1, '2019-03-13 04:35:32', '2019-03-13 04:35:32', '', 'kks-giaiphapquanly-bg', '', 'inherit', 'open', 'closed', '', 'kks-giaiphapquanly-bg', '', '', '2019-03-13 04:35:32', '2019-03-13 04:35:32', '', 150, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/kks-giaiphapquanly-bg.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (173, 1, '2019-03-13 04:36:08', '2019-03-13 04:36:08', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 04:36:08', '2019-03-13 04:36:08', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (174, 1, '2019-03-13 06:27:13', '2019-03-13 06:27:13', '', 'kks-tietkiemchiphi', '', 'inherit', 'open', 'closed', '', 'kks-tietkiemchiphi', '', '', '2019-03-13 06:27:13', '2019-03-13 06:27:13', '', 150, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/kks-tietkiemchiphi.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (175, 1, '2019-03-13 06:28:23', '2019-03-13 06:28:23', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 06:28:23', '2019-03-13 06:28:23', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (176, 1, '2019-03-13 06:28:41', '2019-03-13 06:28:41', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 06:28:41', '2019-03-13 06:28:41', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (177, 1, '2019-03-13 06:30:09', '2019-03-13 06:30:09', '', 'kss_kiemsoattaichinh-bg', '', 'inherit', 'open', 'closed', '', 'kss_kiemsoattaichinh-bg', '', '', '2019-03-13 06:30:09', '2019-03-13 06:30:09', '', 150, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/kss_kiemsoattaichinh-bg.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (178, 1, '2019-03-13 06:30:43', '2019-03-13 06:30:43', '', 'Khóa khách sạn', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2019-03-13 06:30:43', '2019-03-13 06:30:43', '', 150, 'http://localhost/luonghx/onycom/150-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (179, 1, '2019-03-13 07:44:20', '2019-03-13 07:44:20', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"brands\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Sản phẩm - Thương hiệu', 'san-pham-thuong-hieu', 'publish', 'closed', 'closed', '', 'group_5c88b4bab40c9', '', '', '2019-03-13 07:44:44', '2019-03-13 07:44:44', '', 0, 'http://localhost/luonghx/onycom/?post_type=acf-field-group&#038;p=179', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` VALUES (180, 1, '2019-03-13 07:44:20', '2019-03-13 07:44:20', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Hình ảnh', 'image', 'publish', 'closed', 'closed', '', 'field_5c88b4c4cdd57', '', '', '2019-03-13 07:44:20', '2019-03-13 07:44:20', '', 179, 'http://localhost/luonghx/onycom/?post_type=acf-field&p=180', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` VALUES (181, 1, '2019-03-13 07:45:00', '0000-00-00 00:00:00', '', 'AUTO-DRAFT', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2019-03-13 07:45:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/luonghx/onycom/?post_type=product&p=181', 0, 'product', '', 0);
INSERT INTO `wp_posts` VALUES (182, 1, '2019-03-13 07:46:42', '2019-03-13 07:46:42', '', 'logo-samsung', '', 'inherit', 'open', 'closed', '', 'logo-samsung', '', '', '2019-03-13 07:46:42', '2019-03-13 07:46:42', '', 0, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/logo-samsung.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (183, 1, '2019-03-13 08:00:46', '2019-03-13 08:00:46', '', 'logo-lg', '', 'inherit', 'open', 'closed', '', 'logo-lg', '', '', '2019-03-13 08:00:46', '2019-03-13 08:00:46', '', 0, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/logo-lg.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (184, 1, '2019-03-13 08:01:01', '2019-03-13 08:01:01', '', 'logo-adel', '', 'inherit', 'open', 'closed', '', 'logo-adel', '', '', '2019-03-13 08:01:01', '2019-03-13 08:01:01', '', 0, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/logo-adel.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (185, 1, '2019-03-13 08:01:17', '2019-03-13 08:01:17', '', 'log-ibc', '', 'inherit', 'open', 'closed', '', 'log-ibc', '', '', '2019-03-13 08:01:17', '2019-03-13 08:01:17', '', 0, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/log-ibc.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (186, 1, '2019-03-13 08:01:32', '2019-03-13 08:01:32', '', 'logo-dessman', '', 'inherit', 'open', 'closed', '', 'logo-dessman', '', '', '2019-03-13 08:01:32', '2019-03-13 08:01:32', '', 0, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/logo-dessman.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (187, 1, '2019-03-13 08:01:51', '2019-03-13 08:01:51', '', 'logo-nuki', '', 'inherit', 'open', 'closed', '', 'logo-nuki', '', '', '2019-03-13 08:01:51', '2019-03-13 08:01:51', '', 0, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/logo-nuki.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (188, 1, '2019-03-13 08:02:07', '2019-03-13 08:02:07', '', 'logo-yale', '', 'inherit', 'open', 'closed', '', 'logo-yale', '', '', '2019-03-13 08:02:07', '2019-03-13 08:02:07', '', 0, 'http://localhost/luonghx/onycom/wp-content/uploads/2019/03/logo-yale.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` VALUES (189, 1, '2019-03-13 17:20:02', '2019-03-13 17:20:02', ' ', '', '', 'publish', 'closed', 'closed', '', '189', '', '', '2019-03-13 17:20:02', '2019-03-13 17:20:02', '', 0, 'http://localhost:8080/onycom/?p=189', 10, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` VALUES (190, 1, '2019-03-13 18:30:52', '2019-03-13 18:30:52', '<!-- wp:paragraph -->\n<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>\n<!-- /wp:paragraph -->', 'Chúng tôi là ai?', '', 'publish', 'closed', 'closed', '', 'chung-toi-la-ai', '', '', '2019-03-13 18:31:46', '2019-03-13 18:31:46', '', 0, 'http://localhost:8080/onycom/?page_id=190', 0, 'page', '', 0);
INSERT INTO `wp_posts` VALUES (191, 1, '2019-03-13 18:30:52', '2019-03-13 18:30:52', '', 'Chúng tôi là ai?', '', 'inherit', 'closed', 'closed', '', '190-revision-v1', '', '', '2019-03-13 18:30:52', '2019-03-13 18:30:52', '', 190, 'http://localhost:8080/onycom/190-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (192, 1, '2019-03-13 18:31:24', '2019-03-13 18:31:24', '', 'Chúng tôi là ai?', '', 'inherit', 'closed', 'closed', '', '190-revision-v1', '', '', '2019-03-13 18:31:24', '2019-03-13 18:31:24', '', 190, 'http://localhost:8080/onycom/190-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (193, 1, '2019-03-13 18:31:45', '2019-03-13 18:31:45', '<!-- wp:paragraph -->\n<p>Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ thông minh, Big Data và Ứng dụng di động.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi, an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho các doanh nghiệp.</p>\n<!-- /wp:paragraph -->', 'Chúng tôi là ai?', '', 'inherit', 'closed', 'closed', '', '190-revision-v1', '', '', '2019-03-13 18:31:45', '2019-03-13 18:31:45', '', 190, 'http://localhost:8080/onycom/190-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` VALUES (194, 1, '2019-03-13 18:33:03', '2019-03-13 18:33:03', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-03-13 18:33:03', '2019-03-13 18:33:03', '', 26, 'http://localhost:8080/onycom/26-revision-v1/', 0, 'revision', '', 0);

-- ----------------------------
-- Table structure for wp_term_relationships
-- ----------------------------
DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE `wp_term_relationships`  (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`object_id`, `term_taxonomy_id`) USING BTREE,
  INDEX `term_taxonomy_id`(`term_taxonomy_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_term_relationships
-- ----------------------------
INSERT INTO `wp_term_relationships` VALUES (1, 1, 0);
INSERT INTO `wp_term_relationships` VALUES (13, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (14, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (15, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (16, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (18, 3, 0);
INSERT INTO `wp_term_relationships` VALUES (66, 5, 0);
INSERT INTO `wp_term_relationships` VALUES (66, 21, 0);
INSERT INTO `wp_term_relationships` VALUES (68, 5, 0);
INSERT INTO `wp_term_relationships` VALUES (68, 21, 0);
INSERT INTO `wp_term_relationships` VALUES (69, 5, 0);
INSERT INTO `wp_term_relationships` VALUES (69, 21, 0);
INSERT INTO `wp_term_relationships` VALUES (69, 26, 0);
INSERT INTO `wp_term_relationships` VALUES (106, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (107, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (108, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (109, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (126, 3, 0);
INSERT INTO `wp_term_relationships` VALUES (143, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (144, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (146, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (148, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (149, 2, 0);
INSERT INTO `wp_term_relationships` VALUES (189, 2, 0);

-- ----------------------------
-- Table structure for wp_term_taxonomy
-- ----------------------------
DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE `wp_term_taxonomy`  (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_taxonomy_id`) USING BTREE,
  UNIQUE INDEX `term_id_taxonomy`(`term_id`, `taxonomy`) USING BTREE,
  INDEX `taxonomy`(`taxonomy`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_term_taxonomy
-- ----------------------------
INSERT INTO `wp_term_taxonomy` VALUES (1, 1, 'category', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (2, 2, 'nav_menu', '', 0, 14);
INSERT INTO `wp_term_taxonomy` VALUES (3, 3, 'category', '', 0, 2);
INSERT INTO `wp_term_taxonomy` VALUES (4, 4, 'category', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (5, 5, 'product_type', '', 0, 3);
INSERT INTO `wp_term_taxonomy` VALUES (6, 6, 'product_type', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (7, 7, 'product_type', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (8, 8, 'product_type', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (9, 9, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (10, 10, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (11, 11, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (12, 12, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (13, 13, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (14, 14, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (15, 15, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (16, 16, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (17, 17, 'product_visibility', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (18, 18, 'product_cat', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (21, 21, 'product_cat', '', 0, 3);
INSERT INTO `wp_term_taxonomy` VALUES (22, 22, 'product_cat', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (23, 23, 'product_cat', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (24, 24, 'product_cat', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (25, 25, 'product_cat', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (26, 26, 'brands', '', 0, 1);
INSERT INTO `wp_term_taxonomy` VALUES (27, 27, 'brands', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (28, 28, 'brands', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (29, 29, 'brands', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (30, 30, 'brands', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (31, 31, 'brands', '', 0, 0);
INSERT INTO `wp_term_taxonomy` VALUES (32, 32, 'brands', '', 0, 0);

-- ----------------------------
-- Table structure for wp_termmeta
-- ----------------------------
DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE `wp_termmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `term_id`(`term_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_termmeta
-- ----------------------------
INSERT INTO `wp_termmeta` VALUES (8, 18, 'product_count_product_cat', '0');
INSERT INTO `wp_termmeta` VALUES (9, 21, 'order', '0');
INSERT INTO `wp_termmeta` VALUES (10, 21, 'display_type', '');
INSERT INTO `wp_termmeta` VALUES (11, 21, 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES (12, 22, 'order', '0');
INSERT INTO `wp_termmeta` VALUES (13, 22, 'display_type', '');
INSERT INTO `wp_termmeta` VALUES (14, 22, 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES (15, 21, 'product_count_product_cat', '3');
INSERT INTO `wp_termmeta` VALUES (16, 23, 'order', '0');
INSERT INTO `wp_termmeta` VALUES (17, 23, 'display_type', '');
INSERT INTO `wp_termmeta` VALUES (18, 23, 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES (19, 24, 'order', '0');
INSERT INTO `wp_termmeta` VALUES (20, 24, 'display_type', '');
INSERT INTO `wp_termmeta` VALUES (21, 24, 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES (22, 25, 'order', '0');
INSERT INTO `wp_termmeta` VALUES (23, 25, 'display_type', '');
INSERT INTO `wp_termmeta` VALUES (24, 25, 'thumbnail_id', '0');
INSERT INTO `wp_termmeta` VALUES (25, 26, 'image', '182');
INSERT INTO `wp_termmeta` VALUES (26, 26, '_image', 'field_5c88b4c4cdd57');
INSERT INTO `wp_termmeta` VALUES (27, 27, 'image', '183');
INSERT INTO `wp_termmeta` VALUES (28, 27, '_image', 'field_5c88b4c4cdd57');
INSERT INTO `wp_termmeta` VALUES (29, 28, 'image', '184');
INSERT INTO `wp_termmeta` VALUES (30, 28, '_image', 'field_5c88b4c4cdd57');
INSERT INTO `wp_termmeta` VALUES (31, 29, 'image', '185');
INSERT INTO `wp_termmeta` VALUES (32, 29, '_image', 'field_5c88b4c4cdd57');
INSERT INTO `wp_termmeta` VALUES (33, 30, 'image', '186');
INSERT INTO `wp_termmeta` VALUES (34, 30, '_image', 'field_5c88b4c4cdd57');
INSERT INTO `wp_termmeta` VALUES (35, 31, 'image', '187');
INSERT INTO `wp_termmeta` VALUES (36, 31, '_image', 'field_5c88b4c4cdd57');
INSERT INTO `wp_termmeta` VALUES (37, 32, 'image', '188');
INSERT INTO `wp_termmeta` VALUES (38, 32, '_image', 'field_5c88b4c4cdd57');

-- ----------------------------
-- Table structure for wp_terms
-- ----------------------------
DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE `wp_terms`  (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_id`) USING BTREE,
  INDEX `slug`(`slug`(191)) USING BTREE,
  INDEX `name`(`name`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_terms
-- ----------------------------
INSERT INTO `wp_terms` VALUES (1, 'Uncategorized', 'uncategorized', 0);
INSERT INTO `wp_terms` VALUES (2, 'main-menu', 'main-menu', 0);
INSERT INTO `wp_terms` VALUES (3, 'Tin tức', 'tin-tuc', 0);
INSERT INTO `wp_terms` VALUES (4, 'Dự án', 'du-an', 0);
INSERT INTO `wp_terms` VALUES (5, 'simple', 'simple', 0);
INSERT INTO `wp_terms` VALUES (6, 'grouped', 'grouped', 0);
INSERT INTO `wp_terms` VALUES (7, 'variable', 'variable', 0);
INSERT INTO `wp_terms` VALUES (8, 'external', 'external', 0);
INSERT INTO `wp_terms` VALUES (9, 'exclude-from-search', 'exclude-from-search', 0);
INSERT INTO `wp_terms` VALUES (10, 'exclude-from-catalog', 'exclude-from-catalog', 0);
INSERT INTO `wp_terms` VALUES (11, 'featured', 'featured', 0);
INSERT INTO `wp_terms` VALUES (12, 'outofstock', 'outofstock', 0);
INSERT INTO `wp_terms` VALUES (13, 'rated-1', 'rated-1', 0);
INSERT INTO `wp_terms` VALUES (14, 'rated-2', 'rated-2', 0);
INSERT INTO `wp_terms` VALUES (15, 'rated-3', 'rated-3', 0);
INSERT INTO `wp_terms` VALUES (16, 'rated-4', 'rated-4', 0);
INSERT INTO `wp_terms` VALUES (17, 'rated-5', 'rated-5', 0);
INSERT INTO `wp_terms` VALUES (18, 'Uncategorized', 'uncategorized', 0);
INSERT INTO `wp_terms` VALUES (21, 'Khóa của điện tử', 'khoa-dien-tu', 0);
INSERT INTO `wp_terms` VALUES (22, 'Khóa khách sạn', 'khoa-khach-san', 0);
INSERT INTO `wp_terms` VALUES (23, 'Khóa cửa điện tử cao cấp', 'khoa-cua-dien-tu-cao-cap', 0);
INSERT INTO `wp_terms` VALUES (24, 'Chuông hình', 'chuong-hinh', 0);
INSERT INTO `wp_terms` VALUES (25, 'Kiểm soát cửa chấm công', 'kiem-soat-cua-cham-cong', 0);
INSERT INTO `wp_terms` VALUES (26, 'Samsung', 'samsung', 0);
INSERT INTO `wp_terms` VALUES (27, 'LG', 'lg', 0);
INSERT INTO `wp_terms` VALUES (28, 'ADEL', 'adel', 0);
INSERT INTO `wp_terms` VALUES (29, 'IBC', 'ibc', 0);
INSERT INTO `wp_terms` VALUES (30, 'Dessmann', 'dessmann', 0);
INSERT INTO `wp_terms` VALUES (31, 'Nuki', 'nuki', 0);
INSERT INTO `wp_terms` VALUES (32, 'Yale', 'yale', 0);

-- ----------------------------
-- Table structure for wp_usermeta
-- ----------------------------
DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE `wp_usermeta`  (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`umeta_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_usermeta
-- ----------------------------
INSERT INTO `wp_usermeta` VALUES (1, 1, 'nickname', 'root');
INSERT INTO `wp_usermeta` VALUES (2, 1, 'first_name', '');
INSERT INTO `wp_usermeta` VALUES (3, 1, 'last_name', '');
INSERT INTO `wp_usermeta` VALUES (4, 1, 'description', '');
INSERT INTO `wp_usermeta` VALUES (5, 1, 'rich_editing', 'true');
INSERT INTO `wp_usermeta` VALUES (6, 1, 'syntax_highlighting', 'true');
INSERT INTO `wp_usermeta` VALUES (7, 1, 'comment_shortcuts', 'false');
INSERT INTO `wp_usermeta` VALUES (8, 1, 'admin_color', 'fresh');
INSERT INTO `wp_usermeta` VALUES (9, 1, 'use_ssl', '0');
INSERT INTO `wp_usermeta` VALUES (10, 1, 'show_admin_bar_front', 'true');
INSERT INTO `wp_usermeta` VALUES (11, 1, 'locale', '');
INSERT INTO `wp_usermeta` VALUES (12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (13, 1, 'wp_user_level', '10');
INSERT INTO `wp_usermeta` VALUES (14, 1, 'dismissed_wp_pointers', 'wp496_privacy');
INSERT INTO `wp_usermeta` VALUES (15, 1, 'show_welcome_panel', '1');
INSERT INTO `wp_usermeta` VALUES (16, 1, 'session_tokens', 'a:3:{s:64:\"7a6e8cad5b9992828aa445544d67c331045b192cee87685e7e00795fa50f5cbc\";a:4:{s:10:\"expiration\";i:1552580885;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1552408085;}s:64:\"291ab57a06d0188d115aac2d6d8d24a2d9096993341e2548a8f2c60ae254209b\";a:4:{s:10:\"expiration\";i:1552619852;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36\";s:5:\"login\";i:1552447052;}s:64:\"6a24ab50210761a6dca4566655799fe87b0eb5c9cb7d40828a6379bddd1b39a3\";a:4:{s:10:\"expiration\";i:1552670221;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1552497421;}}');
INSERT INTO `wp_usermeta` VALUES (17, 1, 'wp_dashboard_quick_press_last_post_id', '139');
INSERT INTO `wp_usermeta` VALUES (18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}');
INSERT INTO `wp_usermeta` VALUES (19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}');
INSERT INTO `wp_usermeta` VALUES (20, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce');
INSERT INTO `wp_usermeta` VALUES (21, 1, 'wp_user-settings-time', '1551602940');
INSERT INTO `wp_usermeta` VALUES (22, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}');
INSERT INTO `wp_usermeta` VALUES (23, 1, 'wc_last_active', '1552435200');
INSERT INTO `wp_usermeta` VALUES (25, 1, 'nav_menu_recently_edited', '2');
INSERT INTO `wp_usermeta` VALUES (28, 1, 'closedpostboxes_product', 'a:0:{}');
INSERT INTO `wp_usermeta` VALUES (29, 1, 'metaboxhidden_product', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- ----------------------------
-- Table structure for wp_users
-- ----------------------------
DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE `wp_users`  (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime(0) NOT NULL,
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `user_login_key`(`user_login`) USING BTREE,
  INDEX `user_nicename`(`user_nicename`) USING BTREE,
  INDEX `user_email`(`user_email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_users
-- ----------------------------
INSERT INTO `wp_users` VALUES (1, 'root', '$P$BxXtJ0Y5Fjw80iXO/dLgZnZlkY/Jce1', 'root', 'luonghx@gmail.com', '', '2019-03-02 15:53:16', '', 0, 'root');

-- ----------------------------
-- Table structure for wp_wc_download_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_wc_download_log`;
CREATE TABLE `wp_wc_download_log`  (
  `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime(0) NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `user_ip_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`download_log_id`) USING BTREE,
  INDEX `permission_id`(`permission_id`) USING BTREE,
  INDEX `timestamp`(`timestamp`) USING BTREE,
  CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_wc_webhooks
-- ----------------------------
DROP TABLE IF EXISTS `wp_wc_webhooks`;
CREATE TABLE `wp_wc_webhooks`  (
  `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime(0) NOT NULL,
  `date_created_gmt` datetime(0) NOT NULL,
  `date_modified` datetime(0) NOT NULL,
  `date_modified_gmt` datetime(0) NOT NULL,
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT 0,
  `pending_delivery` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`webhook_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_api_keys
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_api_keys`;
CREATE TABLE `wp_woocommerce_api_keys`  (
  `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `permissions` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `truncated_key` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`key_id`) USING BTREE,
  INDEX `consumer_key`(`consumer_key`) USING BTREE,
  INDEX `consumer_secret`(`consumer_secret`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_attribute_taxonomies
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_attribute_taxonomies`;
CREATE TABLE `wp_woocommerce_attribute_taxonomies`  (
  `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `attribute_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`attribute_id`) USING BTREE,
  INDEX `attribute_name`(`attribute_name`(20)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_downloadable_product_permissions
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_downloadable_product_permissions`;
CREATE TABLE `wp_woocommerce_downloadable_product_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `download_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `order_key` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `downloads_remaining` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `access_granted` datetime(0) NOT NULL,
  `access_expires` datetime(0) NULL DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`permission_id`) USING BTREE,
  INDEX `download_order_key_product`(`product_id`, `order_id`, `order_key`(16), `download_id`) USING BTREE,
  INDEX `download_order_product`(`download_id`, `order_id`, `product_id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_log`;
CREATE TABLE `wp_woocommerce_log`  (
  `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime(0) NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `level`(`level`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_order_itemmeta
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_order_itemmeta`;
CREATE TABLE `wp_woocommerce_order_itemmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `order_item_id`(`order_item_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(32)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_order_items
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_order_items`;
CREATE TABLE `wp_woocommerce_order_items`  (
  `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_item_id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_payment_tokenmeta
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_payment_tokenmeta`;
CREATE TABLE `wp_woocommerce_payment_tokenmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `payment_token_id`(`payment_token_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(32)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_payment_tokens
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_payment_tokens`;
CREATE TABLE `wp_woocommerce_payment_tokens`  (
  `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`token_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_sessions
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_sessions`;
CREATE TABLE `wp_woocommerce_sessions`  (
  `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`session_id`) USING BTREE,
  UNIQUE INDEX `session_key`(`session_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_woocommerce_sessions
-- ----------------------------
INSERT INTO `wp_woocommerce_sessions` VALUES (4, 'ce7d5937a89e300cf3849da02499395e', 'a:12:{s:4:\"cart\";s:420:\"a:1:{s:32:\"a3f390d88e4c41f2747bfa2f1b5f87db\";a:11:{s:3:\"key\";s:32:\"a3f390d88e4c41f2747bfa2f1b5f87db\";s:10:\"product_id\";i:68;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:5000000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:5000000;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:423:\"a:15:{s:8:\"subtotal\";s:10:\"5000000.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:4:\"0.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:10:\"5000000.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:10:\"5000000.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:22:\"shipping_for_package_0\";s:409:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_247a2c53437a96f0e8c25898951f8e27\";s:5:\"rates\";a:1:{s:15:\"free_shipping:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:15:\"free_shipping:1\";s:9:\"method_id\";s:13:\"free_shipping\";s:11:\"instance_id\";i:1;s:5:\"label\";s:13:\"Free shipping\";s:4:\"cost\";s:4:\"0.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Items\";s:35:\"khóa thông minh samsung &times; 1\";}}}}\";s:25:\"previous_shipping_methods\";s:43:\"a:1:{i:0;a:1:{i:0;s:15:\"free_shipping:1\";}}\";s:23:\"chosen_shipping_methods\";s:33:\"a:1:{i:0;s:15:\"free_shipping:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:10:\"wc_notices\";N;s:8:\"customer\";s:687:\"a:26:{s:2:\"id\";s:1:\"0\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"VN\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"VN\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:0:\"\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1552940109);

-- ----------------------------
-- Table structure for wp_woocommerce_shipping_zone_locations
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_shipping_zone_locations`;
CREATE TABLE `wp_woocommerce_shipping_zone_locations`  (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`) USING BTREE,
  INDEX `location_id`(`location_id`) USING BTREE,
  INDEX `location_type_code`(`location_type`(10), `location_code`(20)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_woocommerce_shipping_zone_locations
-- ----------------------------
INSERT INTO `wp_woocommerce_shipping_zone_locations` VALUES (1, 1, 'VN', 'country');

-- ----------------------------
-- Table structure for wp_woocommerce_shipping_zone_methods
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_shipping_zone_methods`;
CREATE TABLE `wp_woocommerce_shipping_zone_methods`  (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `method_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`instance_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_woocommerce_shipping_zone_methods
-- ----------------------------
INSERT INTO `wp_woocommerce_shipping_zone_methods` VALUES (1, 1, 'free_shipping', 1, 1);
INSERT INTO `wp_woocommerce_shipping_zone_methods` VALUES (0, 2, 'free_shipping', 1, 1);

-- ----------------------------
-- Table structure for wp_woocommerce_shipping_zones
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_shipping_zones`;
CREATE TABLE `wp_woocommerce_shipping_zones`  (
  `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`zone_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_woocommerce_shipping_zones
-- ----------------------------
INSERT INTO `wp_woocommerce_shipping_zones` VALUES (1, 'Vietnam', 0);

-- ----------------------------
-- Table structure for wp_woocommerce_tax_rate_locations
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_tax_rate_locations`;
CREATE TABLE `wp_woocommerce_tax_rate_locations`  (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `location_code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`) USING BTREE,
  INDEX `tax_rate_id`(`tax_rate_id`) USING BTREE,
  INDEX `location_type_code`(`location_type`(10), `location_code`(20)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_woocommerce_tax_rates
-- ----------------------------
DROP TABLE IF EXISTS `wp_woocommerce_tax_rates`;
CREATE TABLE `wp_woocommerce_tax_rates`  (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT 0,
  `tax_rate_shipping` int(1) NOT NULL DEFAULT 1,
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`) USING BTREE,
  INDEX `tax_rate_country`(`tax_rate_country`) USING BTREE,
  INDEX `tax_rate_state`(`tax_rate_state`(2)) USING BTREE,
  INDEX `tax_rate_class`(`tax_rate_class`(10)) USING BTREE,
  INDEX `tax_rate_priority`(`tax_rate_priority`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
