<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo wp_title(); ?></title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/plugin/dist/css/lightslider.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />



    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/onycom.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="application/javascript"
            src="<?php echo get_template_directory_uri() ?>/plugin/dist/js/lightslider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


</head>
<body>
<div class="wrapper">
    <div class="header">
        <div class="col-md-2">
            <div class="head-1" style="height: 55px;">&nbsp;</div>
            <div class="head-2">
                <div class="logo"><a href="<?php echo site_url();?>"><img src="<?php echo get_template_directory_uri() ?>/images/logo.png"
                                                  alt=""></a></div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="top-header" style="height: 55px;">&nbsp;</div>
            <div class="menu">
                <?php wp_nav_menu(array(
                    'menu' => 'main-menu',
                    'menu_class' => 'main-menu',
                    'menu_id' => 'main-menu',
                    'container' => 'ul',
                    'depth' => 2));
                ?>
                <!--<ul class="main-menu">
                    <li><a href="">Giải pháp cho doanh nghiệp</a></li>
                    <li><a href="">Giải pháp smarthome</a>
                        <ul class="sub-menu">
                            <li><a href="">Khóa vân tay</a></li>
                            <li><a href="">Khóa thẻ từ</a></li>
                            <li><a href="">Khóa khách sạn</a></li>
                            <li><a href="">Khóa tủ đồ</a></li>
                        </ul>
                    </li>
                    <li><a href="">Giới thiệu</a></li>
                    <li><a href="">Tin tức</a></li>
                </ul>-->
            </div>
        </div>
        <div class="col-md-3">
            <div class="row">
                <ul class="head-function">
                    <li><a href=""><i class="fa fa-search"></i></a></li>
                    <li><a href=""><i class="fa fa-user"></i></a></li>
                    <li><a href="<?php echo wc_get_cart_url();?>"><i class="fa fa-opencart"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php
include ('aq_resizer.php');
?>