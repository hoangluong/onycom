<?php
/*
 * Template Name: Tin tức
 * */
get_header();
if (have_posts()):
    the_post();
    ?>
    <div class="banner-page-list"
         style="background-image: url('<?php echo get_field("background") ?>');">
        <div class="title-page">
            <div class="ac">
            </div>

        </div>
    </div>
    <div class="container">
        <?php
        $newsCat = get_terms('category',
            array('hide_empty' => false)
        );
        foreach ($newsCat as $newsC) {
            if (in_array($newsC->term_id, array(3, 34))) {
                ?>
                <div class="block clearfix">


                    <div class="block-detail">
                        <h2 class="module-title center"><?php echo $newsC->name; ?></h2>

                        <hr>
                        <div class="block-content">
                            <div class="list-news">
                                <?php
                                $args = array(
                                    'post_status' => 'publish'


                                );
                                wp_reset_query();
                                wp_reset_postdata();

                                $the_query = new WP_Query($args);
                                if ($the_query->have_posts()):
                                    while ($the_query->have_posts()):$the_query->the_post();
                                        ?>
                                        <div class="news-item col-md-4">
                                            <div class="thumb"><a
                                                        href="<?php echo get_permalink() ?>"><?php $url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'thumbnail'); ?>
                                                    <img src="<?php echo aq_resize($url, 350, 200, false, true, true) ?>"/></a>
                                            </div>
                                            <div class="title"><a
                                                        href="<?php echo get_permalink() ?>"><?php the_title(); ?></a>
                                            </div>
                                            <div class="desc"><?php echo get_field('desc') ?></div>
                                        </div>
                                        <?php
                                    endwhile;
                                endif;
                                ?>


                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        } ?>
        <div class="col-md-4">
            <div class="block-sub clearfix">


                <div class="block-detail">
                    <h2 class="module-title-2">Hình ảnh</h2>

                    <hr>
                    <div class="block-content">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block-sub clearfix">


                <div class="block-detail">
                    <h2 class="module-title-2">Video</h2>

                    <hr>
                    <div class="block-content">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block-sub clearfix">


                <div class="block-detail">
                    <h2 class="module-title-2">Tin khuyến mại</h2>

                    <hr>
                    <div class="block-content">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <?php
endif;
get_footer();
?>

