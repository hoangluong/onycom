<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_onycom' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'E=P^Iyi9 yx*F,G9bw|9TH36W7]7X~{Zu:0^(V-$gZ>o-pokPnNGKt,my^cKM/Eb' );
define( 'SECURE_AUTH_KEY',  '9!*cv>A]-D@,:toGOD2Zi}Z,G _Ebke_}-*x#$&?]a__`-A[CTbpM1F[9t1ri=!l' );
define( 'LOGGED_IN_KEY',    'fAbkFw}_wq+C#>Hdj[qs&tX|)&{&Q_(peY3x1gIZ$VcxNKTBlf~h.A(/&%1$Tt{i' );
define( 'NONCE_KEY',        '-aOpcb%:v#8%MCJkj`rNu_ZUVK|ktqK*[@Rh+zm64=.r<hDR}JOS|>!) OPeFEqV' );
define( 'AUTH_SALT',        'g_<jw*VRiB4jTmqu.$sObCJ#(D&UwOVc^b0.P5eclyify<Zfg&{clu.Z^X?S;/LF' );
define( 'SECURE_AUTH_SALT', 'I6EK-l:bKpNz<A%YL[:N%v6oN(x&gkgk0+z15Vl+ Z^7tYQ)-w[9)4gA~s|*+K!T' );
define( 'LOGGED_IN_SALT',   '8s:oH$B)-cy#tG9O)c>E^2>f^:<Lqq4s4j;$+17Rg:{%T>]:%KZ+a{iP[8WpeNgq' );
define( 'NONCE_SALT',       '-obO7iTH+n9RH3 B?oL]@MUfk>5FIGP4|2Pw=ty]*_>STMK0+GuAB-TxLn}7(Ha9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
