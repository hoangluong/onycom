

<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="f-sub-title">Để tiện lợi</div>
            <div class="f-title">Liên hệ với chúng tôi</div>

            <div class="f-services">
                <div class="col-md-4">
                    <img src="<?php echo get_template_directory_uri() ?>/images/icon-24h.png" alt="">
                    <div class="f-s">0246.287.1606 <br>
                        8:00 - 18:00
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo get_template_directory_uri() ?>/images/icon-place.png" alt="">
                    <div class="f-s">Số 8 - Lô C11<br> Ngõ 44/3/2 Nguyễn Cơ Thạch <br>
                        Mỹ Đình 1, Nam Từ Liêm, Hà Nội
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo get_template_directory_uri() ?>/images/icon-acong.png" alt="">
                    <div class="f-s">dhomevn.hanoi@gmail.com <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="div footer-bot clearfix">
        <div class="container">
            <div class="col-md-3">
                <ul class="f-menu">
                    <li>Giới thiệu</li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="f-menu">
                    <li>Giới thiệu</li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="f-menu">
                    <li>Giới thiệu</li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="f-menu">
                    <li>Giới thiệu</li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                    <li><a href="">Về chúng tôi</a></li>
                </ul>
            </div>
            <div class="col-md-12">
                <hr>
                <div class="right">
                    <a href="" class="f-social"><i class="fa fa-facebook-square"></i></a>
                    <a href="" class="f-social"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $('.slider2').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false
    });
</script>
</body>
</html>