<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

get_header('shop');
global $wp;

?>
<div class="onycom-page">
    <div class="onycom-page-header" style="background: url('<?php echo get_field("background", 57) ?>')">
        <div class="container">
            <div class="col-md-8">
                <div class="text">
                    <h1><?php echo the_title(); ?></h1>

                    <div class="page-desc">
                        <?php echo get_field('desc') ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="img right">
                    <img src="<?php echo get_field('image_desc', 57); ?>" alt="">
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="block">


            <div class="block-detail" style="padding-top: 0">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="">Trang chủ</a> <i class="fa fa-chevron-right"></i></li>
                        <li><a href=""><?php echo single_term_title(); ?></a></li>
                    </ul>
                </div>
                <?php
                get_template_part('parts/slider', 'brands');
                ?>
                <br>

                <div class="archive-content">
                    <h1 class="module-title"><?php echo single_term_title(); ?></h1>
                    <div class="archive-body">
                        <div class="col-md-3">
                            <div class="leftside">
                                <div class="title">Danh mục</div>
                                <div class="list-categories">
                                    <ul>


                                        <?php
                                        $categories = get_terms('product_cat', array(
                                            'hide_empty' => false,
                                        ));
                                        foreach ($categories as $item) {
                                            if($item->term_id != 18){
                                            ?>
                                            <li>
                                                <a href="<?php echo get_term_link($item->term_id) ?>"><?php echo $item->name; ?></a>
                                            </li>
                                        <?php }} ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">

                            <div class="archive-product-list">
                                <div class="filter">
                                    <ul>
                                        <li><a href="<?php echo home_url($wp->request) ?>?orderby=price"
                                               class="<?php echo (isset($_GET['orderby']) && $_GET['orderby'] == 'price') ? 'active' : '' ?>">Giá
                                                tăng dần</a></li>
                                        <li><a href="<?php echo home_url($wp->request) ?>?orderby=price-desc"
                                               class="<?php echo (isset($_GET['orderby']) && $_GET['orderby'] == 'price-desc') ? 'active' : '' ?>">Giá
                                                giảm dần</a></li>
                                        <li><a href="<?php echo home_url($wp->request) ?>?orderby=popularity"
                                               class="<?php echo (isset($_GET['orderby']) && $_GET['orderby'] == 'popularity') ? 'active' : '' ?>">Bán
                                                chạy nhất</a></li>
                                    </ul>
                                </div>
                                <?php

                                $k = 0;
                                if (woocommerce_product_loop()) {

                                    /**
                                     * Hook: woocommerce_before_shop_loop.
                                     *
                                     * @hooked woocommerce_output_all_notices - 10
                                     * @hooked woocommerce_result_count - 20
                                     * @hooked woocommerce_catalog_ordering - 30
                                     */
                                    do_action('woocommerce_before_shop_loop');

                                    woocommerce_product_loop_start();

                                    if (wc_get_loop_prop('total')) {
                                        while (have_posts()) {
                                            $k++;
                                            the_post();
                                            $product = new WC_Product(get_the_ID());
                                            ?>

                                            <div class="product-item col-md-4">
                                                <div class="thumb"><a href="<?php echo get_permalink() ?>">
                                                        <?php
                                                        $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'single-post-thumbnail');
                                                        ?>
                                                        <img style="max-height: 200px;"
                                                             src="<?php echo $image[0] ?>"
                                                             alt="">
                                                    </a></div>

                                                <div class="title"><a
                                                            href="<?php echo get_permalink() ?>"><?php the_title(); ?> </a>
                                                </div>

                                                <div class="price"><?php echo number_format($product->get_price()); ?>
                                                    đ
                                                </div>
                                                <div class="center" style="padding: 30px;">
                                                    <a href="<?php echo get_permalink() ?>" class="block-more">Mua
                                                        ngay</a>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                woocommerce_product_loop_end();

                                /**
                                 * Hook: woocommerce_after_shop_loop.
                                 *
                                 * @hooked woocommerce_pagination - 10
                                 */
                                do_action('woocommerce_after_shop_loop');
                                if ($k == 0) echo 'Không tìm thấy sản phẩm nào!';
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer('shop');
?>
