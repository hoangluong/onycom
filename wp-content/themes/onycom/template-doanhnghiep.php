<?php
/*
 * Template Name: Giải pháp doanh nghiệp
 * */
get_header();
if (have_posts()):the_post();
    ?>
    <div class="onycom-page">
        <div class="onycom-page-header" style="background: url('<?php echo get_field("background") ?>')">
            <div class="container">
                <div class="col-md-8">
                    <div class="text">
                        <h1><?php echo the_title(); ?></h1>

                        <div class="page-desc">
                            <?php echo get_field('desc') ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img right">
                        <img src="<?php echo get_field('image_desc'); ?>" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="block">


            <div class="block-detail">
                <h2 class="module-title center"><?php echo the_title(); ?></h2>
                <hr>
                <div class="block-content">
                    <div class="list-gp-doanhnghiep clearfix">
                        <?php
                        $my_wp_query = new WP_Query();
                        $all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
                        $dn_children = get_page_children(5, $all_wp_pages);
                      //  print_r($dn_children);
                        foreach ($dn_children as $item) {
                            $thumbId = get_post_meta($item->ID, 'thumb', TRUE);

                            $thumb = (wp_get_attachment_image($thumbId));

                            ?>
                            <div class="col-md-3">
                                <div class="item">
                                    <div class="thumb"><a href=""><?php echo $thumb?></a></div>
                                    <div class="title"><a href=""><?php echo $item->post_title?></a></div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
endif;
get_footer();
?>

