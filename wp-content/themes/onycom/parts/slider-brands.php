<div class="list-brand-slider">

    <ul id="sliderPartner" class="simply-scroll-list">
        <?php
        $terms = get_terms(array(
            'taxonomy' => 'brands',
            'hide_empty' => false,
        ));
        foreach ($terms as $item) {
            ?>
            <li><a title="??i t�c Ocean bank" target="_blank"><img
                        src="<?php echo get_field('image','brands_'.$item->term_id)?>"
                        alt="??i t�c Ocean bank"
                        title="??i t�c Ocean bank"></a></li>
        <?php } ?>

    </ul>

</div>
<script>
    $('#sliderPartner').slick({
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        speed: 1000,
        arrows: false
    });
</script>