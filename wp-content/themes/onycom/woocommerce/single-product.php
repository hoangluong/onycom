<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop');
$_product = wc_get_product(get_the_ID());
$terms = get_the_terms ( get_the_ID(), 'product_cat' );

$cateID = $terms[0]->term_id;
?>
    <div class="product-detail">
        <?php woocommerce_breadcrumb(); ?>

        <div class="container">
            <div class="product-info clearfix">
                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                    <?php
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'single-post-thumbnail');

                    $gallery = get_field('image_gallery');

                    ?>
                    <ul id="imageGallery">
                        <li data-thumb="<?php echo aq_resize($image[0], 150, 150, true, true, true); ?>"
                            data-src="<?php echo $image[0]; ?>">
                            <img src="<?php echo $image[0]; ?>"/>
                        </li>
                        <?php
                        if (count($gallery) > 1) {
                            foreach ($gallery as $item) {
                                ?>
                                <li data-thumb="<?php echo aq_resize($item['url'], 150, 150, true, true, true); ?>"
                                    data-src="<?php echo $item['url']; ?>">
                                    <img src="<?php echo $item['url']; ?>"/>
                                </li>
                            <?php }
                        } ?>

                        </li>
                    </ul>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                    <h1><?php the_title(); ?></h1>
                    <?php echo $_product->get_short_description(); ?>
                    <div class="price"><?php echo number_format($_product->get_price()); ?> VNĐ</div>
                    <span class="regular-price"><?php echo number_format($_product->get_regular_price()); ?> VNĐ</span>
                    <span class="badge">-30%</span>
                    <a href="<?php echo do_shortcode('[add_to_cart_url  id="' . get_the_ID() . '"]') ?>"
                       class="btn btn-primary">Mua hàng</a>
                </div>
                <script>
                    $(document).ready(function () {
                        $('#imageGallery').lightSlider({
                            gallery: true,
                            item: 1,
                            loop: true,
                            vertical: true,
                            verticalHeight: 295,
                            vThumbWidth: 50,
                            thumbItem: 8,
                            thumbMargin: 4,
                            slideMargin: 0,
                            enableDrag: false,
                            currentPagerPosition: 'left',
                            onSliderLoad: function (el) {
                                el.lightGallery({
                                    selector: '#imageGallery .lslide'
                                });
                            }
                        });
                    });
                </script>
            </div>
        </div>

        <div class="product-content-list">
            <div class="product-nav">
                <div class="container">
                    <ul class="nav nav-tabs">

                        <li class="active"><a data-toggle="tab" href="#dacdiem">Đặc điểm nổi bật</a></li>
                        <li><a data-toggle="tab" href="#thongso">Thông số kỹ thuật</a></li>
                        <li><a data-toggle="tab" href="#chitiet">Chi tiết sản phẩm</a></li>
                        <li><a data-toggle="tab" href="#baohanh">Chế độ bảo hành</a></li>
                        <li><a data-toggle="tab" href="#khuyenmai">Khuyến mãi</a></li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <div class="tab-content">
                    <div id="dacdiem" class="tab-pane fade in active">
                        <h3>Đặc điểm nổi bật</h3>
                        <p><?php echo $_product->get_description(); ?></p>
                    </div>
                    <div id="thongso" class="tab-pane fade  ">
                        <h3>Thông số kỹ thuật</h3>
                        <p><?php echo get_field('thong_so') ?></p>
                    </div>

                    <div id="chitiet" class="tab-pane fade">
                        <h3>Chi tiết sản phẩm</h3>
                        <p><?php echo get_field('chi_tiet') ?></p>
                    </div>
                    <div id="baohanh" class="tab-pane fade">
                        <h3>Chế độ bảo hành</h3>
                        <p><?php echo get_field('bao_hanh') ?></p>
                    </div>
                    <div id="khuyenmai" class="tab-pane fade">
                        <h3>Khuyến mãi</h3>
                        <p><?php echo get_field('khuyen_mai') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <div class="block home-sanphamnoibat">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Sản phẩm tương tự</h2>
                <div class="list-products">

                    <?php

                    $args = array(
                        'post_status' => 'publish',
                        'post_type' => 'product',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_cat',
                                'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                                'terms' => $cateID,
                                'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                            )
                        )
                    );
                    wp_reset_query();
                    wp_reset_postdata();
                    $arrProductHome = array();
                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()):
                        while ($the_query->have_posts()):$the_query->the_post();
                            wc_get_template_part('content', 'product');
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
?>