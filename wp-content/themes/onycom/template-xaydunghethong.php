<?php
/*
 * Template Name: Xây dựng hệ thống
 * */
get_header();
if (have_posts()):the_post();
    ?>
    <div class="onycom-page">
        <div class="onycom-page-header" style="background: url('<?php echo get_field("background") ?>')">
            <div class="container">
                <div class="col-md-8">
                    <div class="text">
                        <h1><?php echo the_title(); ?></h1>

                        <div class="page-desc">
                            <?php echo get_field('desc') ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img right">
                        <img src="<?php echo get_field('image_desc'); ?>" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="block">


            <div class="block-detail">
                <h2 class="module-title center"><?php echo the_title(); ?></h2>
                <hr>
                <div class="block-content">
                </div>
            </div>
        </div>
    </div>
    <?php
endif;
get_footer();
?>

