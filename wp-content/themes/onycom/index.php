<?php
get_header();
?>
    <div class="home-content">
        <div class="hero-slider">
            <div class="slider-1">
                <div class="slider-1-text"><img src="<?php echo get_template_directory_uri() ?>/images/slider-text.png"
                                                alt=""></div>

            </div>
        </div>
        <div class="block block-chungtoilaai">
            <div class="container">
                <div class="col-md-6">
                    <br>
                    <br>
                    <br>
                    <img src="<?php echo get_template_directory_uri() ?>/images/chungtoilaai-img.png" alt="">
                </div>
                <div class="col-md-6 bg-gray">
                    <div class="row">
                        <div class="block-detail">
                            <h2 class="module-title">Chúng tôi là ai</h2>
                            <div class="module-desc">
                                <p>
                                    Công ty IT hàng đầu từ Hàn Quốc với thế mạnh trong các lĩnh vực Giải pháp và dịch vụ
                                    thông minh, Big Data và Ứng dụng di động.
                                </p>
                                <p>
                                    Tại Việt Nam, Onycom Vina tập trung vào cải thiện cuộc sống, nâng cao sự tiện nghi,
                                    an toàn cho người Việt Nam và giải quyết các vấn đề bảo mật, tối ưu tài nguyên cho
                                    các doanh nghiệp.
                                </p>
                                <a href="" class="block-more">Tìm hiểu thêm</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block-giaiphapchodoanhnghiep">
            <div class="container">

                <div class="col-md-12">
                    <div class="block-detail">
                        <h2 class="module-title center">Giải pháp cho doanh nghiệp</h2>
                        <div class="module-desc w80 center">
                            <p style="font-family: Roboto">Với chuyên môn kỹ thuật và sự hiểu biết của chúng tôi về
                                khách hàng, ONYCOM cung cấp toàn
                                bộ
                                dịch
                                vụ CNTT trong các ngành khác nhau, bao gồm Xây dựng hệ thống, IMQA, Kiểm soát chất lượng
                                &
                                dữ
                                liệu lớn (Big Data)</p>
                            <div class="center clearfix"><img
                                        src="<?php echo get_template_directory_uri() ?>/images/gpdn-home.jpg" alt="">
                            </div>
                            <br>
                            <br>

                            <div class="clearfix">
                                <a href="" class="block-more">Xem tất cả</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <hr>
        <div class="block">
            <div class="container">
                <div class="block-detail pd-bot-zero">
                    <div class="sub-module-title">Làm cho ngôi nhà của bạn thông minh</div>
                    <h2 class="module-title center">Giải pháp cho gia đình bạn</h2>
                    <div class="module-tabs">
                        <ul class="family-solution">
                            <li class="active">
                                <a href="#tab3" data-toggle="tab"
                                   style="    background: #f6f6f6;border-color: transparent;">
                                    Bảo vệ an toàn cho ngôi nhà của bạn
                                </a>
                            </li>
                            <li class="">
                                <a href="#tab2" data-toggle="tab"
                                   style="background: #f6f6f6;border-color: transparent;">
                                    Bảo vệ an toàn cho ngôi nhà của bạn
                                </a>
                            </li>
                            <li class="">
                                <a href="#tab4" data-toggle="tab"
                                   style="background: #f6f6f6;border-color: transparent;">
                                    Bảo vệ an toàn cho ngôi nhà của bạn
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div id="tab3"
                     class="tab-pane fade in active ">
                    <div class="f-slider-1" style="height: 500px;">
                        <div class="f-slider-content">
                            <div class="col-md-2 center bd-white-right"><span>Bảo mật</span></div>
                            <div class="col-md-8 center bd-white-right"><p>Công nghệ bảo mật tiên tiến nhất đến từ các
                                    thương hiệu hàng đầu thế giới</p></div>
                            <div class="col-md-2 center"><a href="" class="block-more white">Xem thêm</a></div>
                        </div>
                    </div>
                </div>
                <div id="tab2"
                     class="tab-pane fade in">
                    <div class="f-slider-1" style="height: 500px;">s</div>
                </div>
                <div id="tab4"
                     class="tab-pane fade in">
                    <div class="f-slider-1" style="height: 500px;">d</div>
                </div>
            </div>
        </div>
        <div class="block home-sanphamnoibat">
            <div class="container">
                <div class="block-detail">

                    <h2 class="module-title center">Sản phẩm nổi bật</h2>
                    <div class="list-products">

                        <?php
                        for ($i = 0; $i < 6; $i++) {
                            ?>
                            <div class="product-item col-md-4">
                                <div class="thumb"><a href=""><img
                                                src="<?php echo get_template_directory_uri() ?>/images/smart-doorlock.png"
                                                alt=""></a></div>

                                <div class="title"><a href="">SHP DP 728 <br> KHÓA THÔNG MINH SAMSUNG </a></div>
                                <div class="desc">
                                    Mở khóa bằng vân tay, mã số, thẻ từ, chìa khóa và bằng Smartphone
                                </div>

                                <div class="center" style="padding: 30px;">
                                    <a href="" class="block-more">Mua ngay</a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block-duan bg-gray">
            <div class="container">
                <div class="block-detail">

                    <h2 class="module-title center">Dự án đã thực hiện</h2>
                    <div class="list-projects">
                        <?php
                        for ($i = 0; $i < 6; $i++) {
                            ?>
                            <div class="project-item col-md-6">
                                <div class="thumb"><a href=""><img
                                                src="<?php echo get_template_directory_uri() ?>/images/gpdn-home.jpg"
                                                alt=""></a></div>
                                <div class="title"><a href="">Dự án A bla bla bla</a></div>
                                <div class="desc">LẮP ĐĂT KHÓA CHO KHÁCH SẠN LIBERTY QUÊ HƯƠNG KHÓA THẺ KHÁCH SẠN</div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-tintuc">
            <div class="container">
                <div class="block-detail">

                    <h2 class="module-title center">Tin tức</h2>
                    <div class="list-projects">
                        <?php
                        for ($i = 0; $i < 6; $i++) {
                            ?>
                            <div class="project-item col-md-6">
                                <div class="thumb"><a href=""><img
                                                src="<?php echo get_template_directory_uri() ?>/images/gpdn-home.jpg"
                                                alt=""></a></div>
                                <div class="title"><a href="">Các dòng khóa khách sạn tốt nhất hiện nay</a></div>
                                <div class="desc">Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu
                                    "Nội dung, nội dung, nội dung" là nó khiến văn bản giống thật hơn, bình thường hơn
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="block block-video">
            <div class="container">
                <div class="block-detail">

                    <h2 class="module-title center">Video</h2>
                </div>
            </div>
        </div>
        <hr>
        <div class="block block-video">
            <div class="container">
                <div class="block-detail">

                    <h2 class="module-title center">Đối tác</h2>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>