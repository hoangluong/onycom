<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

global $product, $post;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}


?>
<div class="product-item col-md-4">
    <div class="">
        <div class="thumb"><a href="<?php echo get_permalink() ?>">
                <?php
                $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'single-post-thumbnail');
                ?>
                <img
                        src="<?php echo $image[0] ?>"
                        alt="">
            </a></div>

        <div class="title"><a href="<?php echo get_permalink() ?>"><?php echo get_field('product_code'); ?>
                <br><?php the_title(); ?> </a></div>
        <div class="desc">
            <?php echo wp_trim_words($post->post_excerpt,15,'...'); ?>
        </div>

        <div class="center" style="padding: 30px;">

           <a href="<?php  echo get_permalink() ?>" class="block-more">Mua ngay</a>
        </div>
    </div>
</div>
