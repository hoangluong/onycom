<?php
/*
 * Template Name: Giải pháp smarthome
 * */
get_header();
if (have_posts()):the_post();
    ?>
    <div class="onycom-page">
        <div class="onycom-page-header" style="background: url('<?php echo get_field("background") ?>')">
            <div class="container">
                <div class="col-md-8">
                    <div class="text">
                        <h1><?php echo the_title(); ?></h1>

                        <div class="page-desc">
                            <?php echo get_field('desc') ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img right">
                        <img src="<?php echo get_field('image_desc'); ?>" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="smarthome-page">
        <div class="container">
            <div class="block">


                <div class="block-detail">
                    <h2 class="module-title center"><?php echo the_title(); ?></h2>
                    <hr>
                    <div class="block-content">
                        <?php
                        $thumbId = get_post_meta(get_the_ID(), 'thumb', TRUE);
                        echo $thumb = (wp_get_attachment_image($thumbId, array(1140, 300)));
                        ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="block home-sanphamnoibat">
        <div class="container">
            <div class="block-detail">

                <h2 class="module-title center">Sản phẩm nổi bật</h2>
                <div class="list-products">

                    <?php
                    $args = array(
                        'post_status' => 'publish',
                        'post_type' => 'product'

                    );
                    wp_reset_query();
                    wp_reset_postdata();
                    $arrProductHome = array();
                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()):
                        while ($the_query->have_posts()):$the_query->the_post();
                            wc_get_template_part('content', 'product');
                             endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
endif;
get_footer();
?>

